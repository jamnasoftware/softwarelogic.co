import { WP_REST_API_Post } from "wp-types";

export type Post = WP_REST_API_Post & {
    image: string;
    minutesToRead: number;
}