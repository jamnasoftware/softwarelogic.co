import { textToPath } from "utils/helpers/main";

export const getPath = (slug?: string) => `/services/${slug}${slug ? '/' : ''}`;

export default [
  {
    "title": "Dedicated Solutions",
    "category": "Software",
    "imgSrc": "/icon/iconServices1.svg",
    "content":
      "We develop and implement comprehensive solutions tailored from start to finish. Our approach ensures that the architecture is carefully chosen to fit your project’s needs and budget, so we can achieve your goals together efficiently and effectively.",
    "about":
      "At the heart of SoftwareLogic.co lies a commitment to innovation, using Python as a powerful tool to create scalable, high-performance digital solutions. We bring every project to life with precision and care, turning visions into reality in today’s fast-paced digital world.",
    "about2":
      "In a rapidly evolving digital landscape, development has moved beyond traditional boundaries, demanding versatile and adaptive approaches. SoftwareLogic.co expertly navigates this complexity by utilizing powerful frameworks like Django, DjangoCMS, Flask, Qt and Next.js. These technologies form the backbone of our work, allowing us to create engaging web applications, mobile apps, and software solutions for Linux and Windows environments that captivate users and drive success.",
    "takeaways": [
      "SoftwareLogic.co leads the way in development, offering customized solutions that meet the demands of the modern digital age.",
      "Python serves as the core language for crafting dynamic, scalable, secure, and high-performance applications.",
      "Frameworks such as Django, DjangoCMS, and Flask allow us to develop everything from user-friendly web applications to complex digital platforms.",
      "Next.js ensures state-of-the-art frontend development, providing users with engaging and responsive experiences across devices.",
      "Our expertise extends beyond web applications to mobile and desktop solutions, delivering versatile and high-quality apps for Linux and Windows."
    ],
    "why": [
      {
        "title": "Tailored Solutions, Top Quality",
        "desc": "We know that every business is unique. Our development services are fully customized to meet your specific needs, providing a high-performance solution that aligns perfectly with your business goals."
      },
      {
        "title": "Innovative and Adaptive",
        "desc": "We stay ahead of the curve by adopting the latest technologies and frameworks, ensuring that your digital presence is dynamic, secure, and built for future growth across various platforms."
      },
      {
        "title": "Client-Centered Approach",
        "desc": "Your vision is our mission. We work closely with you to understand your business objectives, creating tailored solutions that not only meet but exceed your expectations, whether it’s a web app, mobile app, or a desktop application."
      }
    ]
  },
  {
    "title": "Web Development",
    "category": "Software",
    "imgSrc": "/icon/iconServices2.svg",
    "content":
      "In the bustling realm of digital evolution, SoftwareLogic.co stands as a leader in innovation, delivering professional and impactful web applications tailored to the needs of every client.",
    "about":
      "With Python at the core of our solutions, SoftwareLogic.co builds scalable and high-performance web applications that bring every project to life. Each line of code reflects our dedication to creating robust, reliable, and engaging digital solutions that help our clients thrive in the connected world.",
    "about2":
      "In today’s dynamic digital environment, web development goes beyond traditional designs, requiring more flexible and versatile solutions. SoftwareLogic.co excels in this area, using advanced Python frameworks like Django, DjangoCMS, Flask, and Next.js. These technologies are the foundation for everything we build, from dynamic and user-friendly web applications to sophisticated platforms that engage users and drive success.",
    "takeaways": [
      "SoftwareLogic.co leads the way in web development, offering tailored solutions that meet the needs of the evolving digital landscape.",
      "Python serves as the backbone for developing dynamic applications that are secure, scalable, and high-performance.",
      "Frameworks such as Django, DjangoCMS, and Flask enable us to create diverse web solutions, ranging from intuitive applications to complex digital platforms.",
      "Next.js provides state-of-the-art frontend development, ensuring interactive and responsive user experiences.",
      "Our team adopts a custom approach to each project, aligning with client goals to create impactful digital solutions across web and mobile platforms."
    ],
    "why": [
      {
        "title": "Tailored Solutions, Top Quality",
        "desc": "We recognize that every business is different. Our services are fully customized to provide high-performance applications that align with your business objectives and drive growth."
      },
      {
        "title": "Innovative and Adaptive",
        "desc": "Our team uses the latest technologies and frameworks to create dynamic, secure, and future-proof solutions for web, mobile, and desktop platforms."
      },
      {
        "title": "Client-Centered Approach",
        "desc": "Your vision is our priority. We collaborate closely with you to build solutions that exceed expectations, whether it’s a web application, mobile app, or desktop software for Linux or Windows."
      }
    ]
  },
  {
    "title": "Mobile/PWA App",
    "category": "Software, Apps",
    "imgSrc": "/icon/iconServices4.svg",
    "content":
      "Developing a mobile app allows you to provide your users with an intuitive and efficient digital experience that aligns with your business objectives and enhances user engagement.",
    "about":
      "SoftwareLogic.co specializes in creating scalable and high-performance mobile applications using cutting-edge technologies. From iOS and Android apps to Progressive Web Apps (PWAs), we ensure that every product we build is optimized for usability and performance, reflecting our commitment to quality and innovation.",
    "about2":
      "In a world where mobile and web platforms converge, SoftwareLogic.co skillfully integrates leading frameworks like React Native, Flutter, and Python-based backends. These tools enable us to build powerful and interactive mobile and PWA solutions that offer seamless user experiences and business growth.",
    "takeaways": [
      "SoftwareLogic.co leads in developing mobile and PWA apps, providing tailored solutions that enhance user engagement and business impact.",
      "Python, alongside frameworks like React Native and Flutter, forms the basis of our mobile and PWA development, ensuring high performance and scalability.",
      "Our expertise spans various platforms, allowing us to build responsive and feature-rich applications for iOS, Android, and PWAs.",
      "We create apps that are not only visually appealing but also optimized for functionality, ensuring smooth and engaging user interactions.",
      "Our solutions are designed with flexibility in mind, allowing for easy scaling and updates as your business and user base grow."
    ],
    "why": [
      {
        "title": "Custom Mobile Solutions",
        "desc": "We design mobile and PWA apps tailored to your specific needs, ensuring a personalized, high-performance product that enhances user engagement and achieves business goals."
      },
      {
        "title": "Cross-Platform Expertise",
        "desc": "Our expertise in React Native, Flutter, and Python-based technologies allows us to create versatile and reliable mobile solutions for various platforms, including iOS, Android, Linux, and Windows."
      },
      {
        "title": "Future-Proof Development",
        "desc": "We build apps that are ready for future growth, ensuring they remain relevant and scalable as your business evolves and your user base expands."
      }
    ]
  },
  {
    "title": "Data Integration",
    "category": "Software",
    "imgSrc": "/icon/iconServices5.svg",
    "content":
      "Optimize your company's data flow with our advanced data integration solutions. Efficient data management translates into reduced business costs and streamlined operations. Our extensive experience ensures reliable and scalable data architecture tailored to your needs.",
    "about":
      "SoftwareLogic.co specializes in data integration solutions, leveraging Python and other technologies to create efficient and seamless data flows. Our team ensures that your data systems are integrated to maximize efficiency, reduce redundancy, and enhance data accuracy.",
    "about2":
      "In the era of data-driven decision-making, SoftwareLogic.co provides robust integration services using industry-leading frameworks and tools. We connect diverse systems, ensuring they work together smoothly, whether it’s connecting databases, APIs, or cloud services, enabling you to access and use your data effectively.",
    "takeaways": [
      "SoftwareLogic.co leads in data integration, creating custom solutions that streamline data flow and reduce operational costs.",
      "Python serves as the backbone for building dynamic, scalable data solutions that integrate seamlessly across platforms.",
      "Our expertise covers a range of technologies to ensure diverse systems—APIs, databases, and cloud services—are efficiently connected.",
      "We deliver solutions that ensure data integrity and security, supporting your business's data-driven initiatives.",
      "Our team tailors each integration to align with your specific business requirements, ensuring a smooth transition and optimal performance."
    ],
    "why": [
      {
        "title": "Custom Integration Solutions",
        "desc": "We understand that every business has unique data needs. Our services are fully tailored to integrate your systems seamlessly, enhancing performance and efficiency."
      },
      {
        "title": "Scalable and Secure",
        "desc": "Our solutions are built to scale with your business, ensuring data security and efficiency as your data needs grow."
      },
      {
        "title": "Proven Expertise",
        "desc": "Our team brings years of experience in data integration, ensuring your systems communicate effectively, whether on-premise or in the cloud."
      }
    ]
  },
  {
    "title": "Infrastructure Architecture & Design",
    "category": "Software, Apps",
    "imgSrc": "/icon/iconServices3.svg",
    "content":
      "We deliver custom high-availability systems for physical, virtual, or cloud environments. Our expertise ensures optimized resource usage, reduced downtime, and improved resilience. Our solutions are scalable, stable, and designed to provide continuous access with rapid recovery options.",
    "about":
      "SoftwareLogic.co designs robust infrastructure solutions using open-source technologies and industry standards to minimize costs and maximize efficiency. We build systems that provide the stability, scalability, and security required for your business's growth.",
    "about2":
      "In today’s fast-moving digital world, infrastructure demands are evolving. SoftwareLogic.co creates versatile setups—physical, virtual, or cloud-based—tailored to meet your business’s specific needs, ensuring that your IT operations are streamlined and resilient.",
    "takeaways": [
      "Our solutions are designed for high availability, ensuring minimal downtime and continuous service.",
      "We use open-source technologies to reduce costs while maintaining flexibility and scalability.",
      "Our architecture supports growth, ensuring your systems can adapt to changing demands.",
      "We provide cloud solutions that are secure, efficient, and designed for quick recovery.",
      "Our team works closely with clients to design bespoke infrastructure solutions that align with business goals."
    ],
    "why": [
      {
        "title": "High-Availability Solutions",
        "desc": "We design systems that minimize downtime and provide consistent access, enhancing business continuity and performance."
      },
      {
        "title": "Cost-Effective Architecture",
        "desc": "Our use of open-source technologies and industry standards allows us to build efficient systems that reduce costs and complexity."
      },
      {
        "title": "Scalable and Flexible",
        "desc": "We create solutions that can grow with your business, ensuring stability and adaptability in both virtual and cloud environments."
      }
    ]
  },
  {
    "title": "Servers & Architecture Management",
    "category": "Software, Apps",
    "imgSrc": "/icon/iconServices4.svg",
    "content":
      "We provide comprehensive server management and 24/7 monitoring services to address both day-to-day issues and evolving challenges. Our expert sysadmins handle updates, backups, security patches, and configurations to ensure optimal performance and security.",
    "about":
      "SoftwareLogic.co offers expert server management services, ensuring your systems are maintained efficiently. We provide proactive monitoring, regular updates, and tailored configurations to keep your infrastructure running smoothly and securely.",
    "about2":
      "In an ever-changing digital landscape, server management requires a proactive and meticulous approach. Our team uses advanced tools and best practices to manage and optimize your server environment, whether on-premise, virtual, or cloud-based.",
    "takeaways": [
      "Our server management services ensure continuous operation and security.",
      "We provide 24/7 monitoring to quickly identify and resolve issues, minimizing downtime.",
      "Our sysadmins apply best practices to keep systems updated, secure, and efficient.",
      "We tailor server configurations to your business needs, optimizing performance across layers.",
      "We support a range of environments—physical, virtual, or cloud—to meet diverse business requirements."
    ],
    "why": [
      {
        "title": "Proactive Server Management",
        "desc": "Our team offers 24/7 monitoring and management services to prevent issues before they impact your business."
      },
      {
        "title": "Customized Solutions",
        "desc": "We tailor server configurations to match your specific operational needs, ensuring efficiency and security."
      },
      {
        "title": "Experienced Sysadmins",
        "desc": "Our experts bring years of experience managing diverse environments, ensuring your systems remain stable and optimized."
      }
    ]
  },
  {
    "title": "UI/UX",
    "category": "Software, Apps",
    "imgSrc": "/icon/iconServices2.svg",
    "content":
      "The design of your digital product is a key element for success. With our extensive experience in UI/UX design, we ensure that your applications are not only visually appealing but also provide an intuitive and engaging user experience.",
    "about":
      "SoftwareLogic.co creates user-centric designs that elevate digital products, ensuring they look great and function seamlessly. We focus on delivering experiences that are both engaging and easy to use, enhancing user satisfaction and business results.",
    "about2":
      "In the digital age, design is more than just aesthetics—it’s about creating experiences. Our UI/UX experts craft designs that are both beautiful and functional, using industry best practices and modern tools to build interfaces that users love.",
    "takeaways": [
      "We focus on creating intuitive designs that enhance user engagement and satisfaction.",
      "Our designs are tailored to fit each client’s branding and objectives, ensuring consistency and impact.",
      "We prioritize functionality, ensuring that our UI/UX solutions are not just visually appealing but also user-friendly.",
      "We design for a range of platforms, including mobile and web, ensuring a seamless experience across devices.",
      "Our team works closely with clients to ensure that each design aligns with business goals and user expectations."
    ],
    "why": [
      {
        "title": "User-Centric Design",
        "desc": "Our UI/UX solutions prioritize the user, ensuring that designs are intuitive, engaging, and effective."
      },
      {
        "title": "Versatile Experience",
        "desc": "We create designs for multiple platforms, ensuring consistency and usability across mobile, web, and desktop environments."
      },
      {
        "title": "Brand Alignment",
        "desc": "Our approach ensures that your digital products align with your brand identity, creating cohesive and impactful experiences."
      }
    ]
  }
].map(item => ({
  ...item,
  url: getPath(textToPath(item.title))
}));