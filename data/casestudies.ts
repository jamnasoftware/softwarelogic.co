import { textToPath } from "utils/helpers/main";
import cateromarket from "./mockups/cateromarket.webp";
import dropui from "./mockups/dropui.webp";
import easyprotect from "./mockups/easyprotect.webp";
import fffrree from "./mockups/fffrree.webp";
import imker from "./mockups/imker.webp";
import isoTrade from "./mockups/iso-trade.webp";
import porownywarkabudowlana from "./mockups/porownywarkabudowlana.webp";
import simba from "./mockups/simba.webp";
import skinwallet from "./mockups/skinwallet.webp";
import timecamp from "./mockups/timecamp.webp";
import reviewDropui from "./casestudies/dropui.png";

const casestudies = [
  {
    title: "dropui.com",
    desc: "With dropui.com, anyone can design and implement sophisticated interactive elements on websites and online stores using intuitive, no-code tools to boost visitor engagement, enhance conversion rates, and optimize user experience.",
    imgSrc: dropui,
    imgAlt: "dropui.com",
    statistics: [
      { value: "2", name: "Years in development" },
      { value: "7", name: "Team members" },
      { value: "5+", name: "Integrations supported" },
      { value: "100%", name: "Client satisfaction" },
    ],
    content1: "<h2>Problem to solve</h2><p><strong>Customizability and Flexibility</strong>: The platform needed to offer high customization capabilities to suit different business needs, while remaining user-friendly for non-technical users.</p><p><strong>Scalability</strong>: Ensuring the platform could scale efficiently for various sizes and types of businesses was critical. This was achieved by leveraging Kubernetes and OpenShift for server management and deployment.</p><p><strong>AI-Driven Features</strong>: Integrating AI to enhance the user experience and provide intelligent recommendations for UI components was a priority. By incorporating OpenAI, the platform offers personalized and automated suggestions, simplifying the customization process.</p>",
    scopeOfWork: [
      { title: "Research", list: ["Conducted UX/UI analysis, planned AI implementation, and ensured cross-platform compatibility to provide a versatile and intuitive user interface."] },
      { title: "Design", list: ["Developed interactive prototypes and responsive designs to adapt to different devices and screen sizes."] },
      { title: "Development", list: ["Built the frontend using Next.js for performance optimization and a dynamic user experience, and developed the backend with Python (Django) for scalable and reliable operations. Integrated AI and third-party tools like OpenAI, Zapier, and Make.com to enhance the platform's capabilities."] }
    ],
    content2: "<h2>Outcomes</h2><p>dropui.com solution provided businesses with a robust and scalable platform capable of automating UI component creation, significantly reducing development time and enhancing operational efficiency. By integrating AI, the platform also offers users personalized customization options, making it easier to deploy and adjust elements to fit specific requirements.</p>",
    review: {
      stars: 5,
      image: reviewDropui,
      content: "Softwarelogic.co is helping us develop a low-code system integrated with AI for generating modal windows and notifications. Their innovative approach and technical expertise are enhancing the platform's efficiency and user-friendliness.",
      author: { name: "Konrad Kur", company: "CTO, dropui.com" }
    },
    content3: "<h2>Conclusion</h2><p>dropui.com success exemplifies the power of low-code solutions in optimizing and automating UI generation. The platform not only enhances operational efficiency but also reduces development time, making it a valuable asset for businesses seeking agility in their digital solutions.</p>",
    technologies: ["Python3", "Django", "PostgreSQL", "Redis", "RabbitMQ", "ReactJS", "NextJS", "Tailwind CSS", "Docker", "Kubernetes", "OpenShift", "Proxmox VE"]
  },
  {
    title: "skinwallet.com",
    desc: "We provided comprehensive outsourcing services for Skinwallet, focusing on Node.js, React.js, and Next.js technologies, which supported their growing platform for virtual asset trading.",
    imgSrc: skinwallet,
    imgAlt: "skinwallet.com",
    statistics: [
      { value: "1", name: "Years in development" },
      { value: "2", name: "Team members" },
      { value: "1.5M+", name: "User transactions" },
      { value: "100%", name: "Client satisfaction" }
    ],
    content1: "<h2>Problem to solve</h2><p><strong>Scalability and Performance</strong>: The platform needed to efficiently scale to support a rapidly increasing user base and transaction volume, while maintaining high performance.</p><p><strong>Security and Payment Integration</strong>: Ensuring secure and seamless transactions in a virtual asset marketplace was critical for user trust and platform growth.</p>",
    scopeOfWork: [
      {
        title: "Research",
        list: ["We analyzed the system architecture to identify bottlenecks, planned API optimizations, and ensured compliance with security standards in payment systems."]
      },
      {
        title: "Design",
        list: ["We developed a responsive frontend using React.js and Next.js, and designed a backend infrastructure with Node.js to handle increased transaction volumes."]
      },
      {
        title: "Development",
        list: ["We conducted full-stack development with Node.js for the backend and React.js for the frontend, optimized API performance, and scaled the database for stability under high loads."]
      }
    ],
    content2: "<h2>Outcomes</h2><p>Our work enabled Skinwallet to support over 1.5 million transactions across six countries, providing users with a seamless and secure trading experience. By optimizing the backend and integrating key third-party services, we significantly improved the platform’s scalability and performance, allowing for future growth without compromising on speed or reliability.</p>",
    review: {
      stars: 5,
      image: reviewDropui,
      content: "We partnered with softwarelogic.co for Node.js, React.js, and Next.js outsourcing services. Their technical proficiency and professionalism ensured smooth project execution and great results.",
      author: { name: "Kornel Szwaja", company: "CEO, Skinwallet" }
    },
    content3: "<h2>Conclusion</h2><p>Skinwallet's platform was transformed by our collaboration, allowing for rapid scaling, increased transaction capacity, and enhanced operational efficiency. By optimizing both frontend and backend systems, we delivered a seamless user experience and ensured secure, high-performance transactions, positioning the platform for continued success in the virtual asset trading market.</p>",
    technologies: ["NodeJS", "ReactJS", "NextJS", "Bootstrap 5", "Docker", "Kubernetes", "AWS"]
  },
  {
    title: "timecamp.com",
    desc: "We're developing a cross-platform C++ app with wxWidgets for Windows and Linux, an AI-powered Electron.js app for three OS, and a monday.com plugin, while conducting company-wide tests to ensure quality and efficiency.",
    imgSrc: timecamp,
    imgAlt: "Timecamp",
    statistics: [
      { value: "1", name: "Years in development" },
      { value: "6", name: "Team members" },
      { value: "25k+", name: "Daily users" },
      { value: "100%", name: "Client satisfaction" }
    ],
    content1: "<h2>Problem to solve</h2><p><strong>Cross-Platform Compatibility</strong>: TimeCamp required a robust time-tracking solution that could function efficiently across multiple operating systems, including Windows and Linux, to serve its growing user base.</p><p><strong>Performance Optimization</strong>: With an expanding number of daily users, the system needed enhanced performance and reliability, ensuring seamless user experiences across platforms.</p>",
    scopeOfWork: [
      {
        title: "Development",
        list: ["We developed cross-platform features using C++ and wxWidgets to ensure smooth operation on both Windows and Linux.", "We optimized C++ code to improve the application's performance and responsiveness.", "We ensured full compatibility and stability across Windows and Linux platforms."]
      },
      {
        title: "Integration",
        list: ["We integrated third-party tools to enhance functionality and extend the platform's capabilities.", "We implemented API connections to ensure seamless data synchronization across devices and platforms."]
      },
      {
        title: "Testing",
        list: ["We performed cross-platform compatibility tests to ensure the application works seamlessly on multiple systems.", "We conducted stress testing to guarantee the system's reliability under high user loads."]
      }
    ],
    content2: "<h2>Outcomes</h2><p>We delivered a highly efficient and reliable time-tracking system that operates smoothly on both Windows and Linux. The platform supports offline tracking and ensures data synchronization across multiple devices, providing users with a seamless experience regardless of their operating system.</p>",
    review: {
      stars: 4,
      content: "We have been collaborating with softwarelogic.co for several months on developing our Windows and Linux applications. Their technical knowledge and commitment have noticeably improved the performance and stability of our software.",
      author: { name: "Kamil Rudnicki", company: "CEO, timecamp.com" }
    },
    content3: "<h2>Conclusion</h2>Our collaboration with TimeCamp has greatly enhanced their ability to deliver a consistent, high-performance experience across platforms. These improvements have resulted in higher user engagement and satisfaction.",
    technologies: ["C++", "wxWidgets", "Linux", "Windows"]
  },
  {
    title: "fffrree.com",
    desc: "We developed a scalable B2B platform for fffrree.com, enabling seamless operations across Europe, with multi-currency support and the ability to handle hundreds of wholesale orders monthly.",
    imgSrc: fffrree,
    imgAlt: "fffrree.com",
    statistics: [
      { value: "3", name: "Years in development" },
      { value: "5", name: "Team members" },
      { value: "2000+", name: "B2B partners" },
      { value: "100%", name: "Client satisfaction" }
    ],
    content1: "<h2>Problem to solve</h2><p><strong>European Market Expansion</strong>: fffrree.com needed a platform capable of supporting cross-border operations, including multi-currency transactions for PLN, EUR, and CZK, to streamline sales across Europe.</p><p><strong>Efficient Wholesale Management</strong>: The platform required robust functionality to manage hundreds of wholesale orders every month, ensuring smooth and reliable order processing.</p><p><strong>Transaction Security</strong>: Secure handling of large-scale B2B transactions was crucial to support a growing network of partners.</p>",
    scopeOfWork: [
      {
        title: "Development",
        list: ["We built a full-stack B2B platform with e-commerce functionality, supporting multi-currency transactions (PLN, EUR, CZK) for European markets."]
      },
      {
        title: "Optimization",
        list: ["We optimized server performance to handle high-volume wholesale orders and ensure smooth transactions across different currencies."]
      },
      {
        title: "Integration",
        list: ["We integrated secure payment systems and third-party APIs to facilitate cross-border transactions and external services."]
      }
    ],
    content2: "<h2>Outcomes</h2><p>We delivered a scalable B2B platform capable of supporting sales across the European market, with full multi-currency support (PLN, EUR, CZK) and efficient handling of wholesale orders. The platform’s performance improvements allow fffrree.com to manage hundreds of orders monthly with secure and seamless transactions.</p>",
    review: {
      stars: 5,
      content: "Their team's expertise allowed us to scale our B2B operations across Europe, efficiently handling large volumes of orders and transactions in multiple currencies.",
      author: { name: "Mateusz Lasota", company: "CEO, fffrree.com" }
    },
    content3: "<h2>Conclusion</h2><p>Our collaboration with fffrree.com resulted in a robust B2B platform optimized for the European market. With multi-currency support and the ability to handle hundreds of wholesale orders each month, the platform has significantly expanded fffrree.com's operational capabilities, ensuring smooth transactions and sustained growth across Europe.</p>",
    technologies: ["Python3", "Django", "Flask", "PostgreSQL", "MongoDB", "JavaScript", "Bootstrap 5", "Docker", "Kubernetes", "OpenShift", "Proxmox VE"]
  },
  {
    title: "oms imker.pl",
    desc: "We developed a robust OMS (Order Management System) for imker.pl, integrating over 20 platforms, connecting more than 300 online stores, and ensuring high system availability for efficient order processing.",
    imgSrc: imker,
    imgAlt: "OMS imker.pl",
    statistics: [
      { value: "2", name: "Years in development" },
      { value: "3", name: "Team members" },
      { value: "10,000+", name: "Orders a day" },
      { value: "100%", name: "Client satisfaction" }
    ],
    content1: "<h2>Problem to solve</h2><p><strong>Scalability and Integration</strong>: Imker.pl needed a scalable OMS system that could integrate with over 300 stores, synchronize orders every 10 minutes, and maintain high availability to ensure seamless e-commerce operations.</p><p><strong>Efficient Order Processing</strong>: The platform required an optimized solution to handle growing volumes of orders efficiently while minimizing delays and errors.</p>",
    scopeOfWork: [
      {
        title: "Development",
        list: ["We developed a scalable OMS platform with over 20 integrations, connecting 300+ online stores to streamline order processing."]
      },
      {
        title: "Optimization",
        list: ["We optimized the system to synchronize orders every 10 minutes, ensuring real-time updates and reducing processing delays."]
      },
      {
        title: "Availability",
        list: ["We ensured high system availability (99.9%) to handle large volumes of orders and provide reliable, uninterrupted service."]
      }
    ],
    content2: "<h2>Outcomes</h2><p>The OMS system we developed for imker.pl enables seamless order management from over 300 connected stores, with orders synchronized every 10 minutes. The system's high availability and integration with over 20 platforms have drastically improved operational efficiency and order processing accuracy.</p>",
    review: {
      stars: 5,
      content: "We have been working with softwarelogic.co for over two years, enhancing our fulfillment operations with a robust Order Management System. Their expertise in both technology and business has made the collaboration very effective.",
      author: { name: "Krzysztof Bartnik", company: "CEO, imker.pl" }
    },
    content3: "<h2>Conclusion</h2><p>The OMS system we built for imker.pl has revolutionized their order management process, integrating over 300 stores, handling frequent order synchronizations, and providing near-perfect system availability. This solution has significantly enhanced their e-commerce operations, enabling them to scale efficiently and meet growing customer demand.</p>",
    technologies: ["Python3", "Django", "Flask", "PostgreSQL", "Redis", "RabbitMQ", "MongoDB", "JavaScript", "Bootstrap 5", "Docker", "Kubernetes", "OpenShift", "Proxmox VE"]
  },
  {
    title: "simba erp",
    desc: "We developed ERP from the ground up for the Simba ERP company, utilizing 12 microservices to ensure scalability and flexibility, tailored to meet the needs of diverse industries.",
    imgSrc: simba,
    imgAlt: "simba erp",
    statistics: [
      { value: "5", name: "Years in development" },
      { value: "4", name: "Team members" },
      { value: "10+", name: "Industries supported" },
      { value: "100%", name: "Client satisfaction" }
    ],
    content1: "<h2>Problem to solve</h2><p><strong>Building a Scalable ERP System from Scratch</strong>: The Simba ERP company needed a fully scalable ERP system that could handle the complex requirements of diverse industries. The system had to be highly flexible, scalable, and able to integrate with third-party tools to support business growth.</p>",
    scopeOfWork: [
      {
        title: "Development",
        list: ["We built the Simba ERP system from scratch using 12 microservices to ensure scalability, modularity, and flexibility for future growth."]
      },
      {
        title: "Optimization",
        list: ["We optimized the system for high-performance data processing, ensuring it could handle increasing workloads and scale efficiently as the client base grew."]
      },
      {
        title: "Integration",
        list: ["We integrated third-party payment and logistics systems to enhance business operations and streamline key processes."]
      }
    ],
    content2: "<h2>Outcomes</h2><p>The Simba ERP system, built from the ground up, utilizes 12 microservices to provide a highly scalable and flexible solution. It now serves over 300 clients across multiple industries, offering seamless integration with external systems and adaptable workflows tailored to the specific needs of each business.</p>",
    review: {
      stars: 5,
      content: "Their team built our ERP system from scratch, designing it to be scalable and flexible, perfectly suited for the diverse needs of our clients across various sectors.",
      author: { name: "Tomasz Szyszkowski", company: "CEO, Simba ERP" }
    },
    content3: "<h2>Conclusion</h2><p>Simba ERP was developed from the ground up for the Simba ERP company, using a microservice architecture to ensure high scalability and flexibility. The system's modular design and seamless integration capabilities have allowed it to grow with client needs, serving businesses across multiple industries with reliability and efficiency.</p>",
    technologies: ["Python3", "Django", "Flask", "PostgreSQL", "Redis", "RabbitMQ", "MongoDB", "ElasticSearch", "OCR", "JavaScript", "Bootstrap 5", "Docker", "Kubernetes", "OpenShift", "Proxmox VE"]
  },
  {
    title: "porownywarkabudowlana.pl",
    desc: "We developed a marketplace platform for comparing construction service offers, allowing customers to find the best prices and place orders directly, while supporting both sellers and buyers.",
    imgSrc: porownywarkabudowlana,
    imgAlt: "porownywarkabudowlana.pl",
    statistics: [
      { value: "1", name: "Years in development" },
      { value: "5", name: "Team members" },
      { value: "100+", name: "Transactions per month" },
      { value: "100%", name: "Client satisfaction" }
    ],
    content1: "<h2>Problem to solve</h2><p><strong>Offer Comparison and Ordering</strong>: Porownywarkabudowlana.pl needed a marketplace solution that would allow users to compare offers from various companies, find the best prices, and place orders directly through the platform.</p><p><strong>User Management</strong>: The platform required support for two types of users: businesses selling their services and customers looking to compare and purchase those services.</p>",
    scopeOfWork: [
      {
        title: "Development",
        list: ["We built a comprehensive marketplace that allows users to compare offers from various companies, check pricing, and place orders directly through the platform."]
      },
      {
        title: "User Management",
        list: ["We developed a system to manage two user levels, providing businesses with the tools to list their services and customers with the ability to compare and order."]
      },
      {
        title: "Optimization",
        list: ["We optimized the platform for real-time offer comparison, ensuring that users can easily find the best deals and place orders with minimal effort."]
      }
    ],
    content2: "<h2>Outcomes</h2><p>The marketplace platform now allows customers to compare 6 different services and offers from more than 100 companies, providing a seamless experience for both sellers and buyers. Customers can easily find the best prices and place orders directly, while businesses benefit from increased visibility and conversions.</p>",
    review: {
      stars: 5,
      content: "The comparison and ordering system has streamlined the decision-making process for our users, helping them find the best offers quickly and increasing our platform's engagement.",
      author: { name: "Adam Bieńkowski", company: "CTO, porownywarkabudowlana.pl" }
    },
    content3: "<h2>Conclusion</h2><p>Our work on Porownywarkabudowlana.pl created a dynamic marketplace that supports both businesses and customers. With a robust offer comparison engine and seamless ordering functionality, the platform has significantly improved user satisfaction and increased conversions, becoming a go-to destination for construction service comparisons in Poland.</p>",
    technologies: ["Python3", "Django", "PostgreSQL", "JavaScript", "Bootstrap 5", "Docker", "Kubernetes", "OpenShift", "Proxmox VE"]
  },
  {
    title: "iso-trade.eu",
    desc: "We provide comprehensive IT support for Iso-Trade, focusing on expanding system integrations and optimizing their e-commerce platform, which handles over 2 million B2B and B2C transactions annually.",
    imgSrc: isoTrade,
    imgAlt: "iso-trade.eu",
    statistics: [
      { value: "1", name: "Years in development" },
      { value: "5", name: "Team members" },
      { value: "2M+", name: "Annual transactions" },
      { value: "100%", name: "Client satisfaction" }
    ],
    content1: "<h2>Problem to solve</h2><p><strong>System Integration and Scalability</strong>: Iso-Trade required ongoing IT development to support its growing ecosystem of system integrations, ensuring scalability to handle over 2 million B2B and B2C transactions annually.</p><p><strong>E-commerce Optimization</strong>: The company needed an optimized e-commerce platform with improved user experience, seamless checkout, and integrated marketing strategies to increase sales and operational efficiency.</p>",
    scopeOfWork: [
      {
        title: "Development",
        list: ["We expanded system integrations to support the growing B2B and B2C ecosystem, ensuring smooth data flow and transaction management across multiple platforms."]
      },
      {
        title: "Optimization",
        list: ["We optimized the e-commerce platform to improve user experience, streamline the checkout process, and enhance the overall sales funnel."]
      },
      {
        title: "Design and Marketing",
        list: ["We provided branding updates, graphic design, and integrated marketing tools to increase site traffic and boost engagement."]
      }
    ],
    content2: "<h2>Outcomes</h2><p>The optimized e-commerce platform and expanded system integrations now support over 2 million transactions annually, significantly improving operational efficiency and boosting sales. The user experience and checkout flow enhancements have contributed to a 10% increase in sales.</p>",
    review: {
      stars: 5,
      content: "We have partnered with softwarelogic.co for over four years to develop custom eCommerce solutions with a focus on integrations. Their technical and business support in marketing, SEO, and UX/UI has made our collaboration highly effective.",
      author: { name: "Mateusz Lasota", company: "CEO, iso-trade.eu" }
    },
    content3: "<h2>Conclusion</h2><p>Our collaboration with Iso-Trade has resulted in a highly scalable e-commerce platform capable of handling millions of transactions annually. By expanding system integrations and optimizing the user experience, we have driven significant growth in both traffic and sales for the company.</p>",
    technologies: ["Python3", "Django", "Flask", "PostgreSQL", "Redis", "RabbitMQ", "MongoDB", "JavaScript", "Bootstrap 5", "Docker", "Kubernetes", "OpenShift", "Proxmox VE"]
  },
  {
    title: "cateromarket.pl",
    desc: "We redeveloped the Cateromarket platform from PHP to Python, enhancing its diet box comparison engine to help users across Poland find the best options based on their dietary preferences, while optimizing the database for better performance.",
    imgSrc: cateromarket,
    imgAlt: "cateromarket.pl",
    statistics: [
      { value: "1", name: "Years in development" },
      { value: "5", name: "Team members" },
      { value: "1000+", name: "Transactions per month" },
      { value: "100%", name: "Client satisfaction" }
    ],
    content1: "<h2>Problem to solve</h2><p><strong>Platform Modernization and Service Comparison</strong>: Cateromarket.pl required a complete overhaul from PHP to Python, while maintaining existing data. Additionally, the platform needed to improve its diet box comparison features, allowing users to easily compare services based on their dietary needs.</p><p><strong>Scalability and Data Optimization</strong>: The database had to be optimized to handle a growing number of providers and users, ensuring smooth operation and real-time updates.</p>",
    scopeOfWork: [
      {
        title: "Development",
        list: ["We redeveloped the platform from PHP to Python, improving performance and scalability while preserving existing data."]
      },
      {
        title: "Comparison Engine",
        list: ["We enhanced the diet box comparison engine, enabling users to filter options based on dietary preferences and easily compare services."]
      },
      {
        title: "Optimization",
        list: ["We optimized the database for faster search performance, real-time updates, and improved user experience across the platform."]
      }
    ],
    content2: "<h2>Outcomes</h2><p>The transition from PHP to Python significantly improved the platform’s scalability and performance, while the optimized comparison engine allowed users to easily compare over 100 diet box providers. The platform now serves over 50,000 users and continues to attract new traffic, increasing overall engagement by 15%.</p>",
    review: {
      stars: 5,
      content: "softwarelogic.co successfully delivered the project on time, improving the platform's scalability and performance. The team managed the project effectively and communicated clearly and thoroughly throughout. Overall, their flexibility and hands-on approach were the hallmarks of their work.",
      author: { name: "Łukasz Sot", company: "CEO, Cateromarket.pl" }
    },
    content3: "<h2>Conclusion</h2><p>By redeveloping Cateromarket.pl from PHP to Python, we significantly improved the platform’s performance, scalability, and user experience. The enhanced comparison engine and optimized database have transformed Cateromarket.pl into a key resource for users searching for diet box services across Poland, resulting in increased user satisfaction and traffic.</p>",
    technologies: ["Python3", "Django", "Flask", "PostgreSQL", "Redis", "RabbitMQ", "JavaScript", "Bootstrap 5", "Docker", "Kubernetes", "OpenShift", "Proxmox VE"]
  },
  {
    title: "easyprotect.pl",
    desc: "We developed a dedicated application for the e-commerce platform idosell.com, allowing online stores to seamlessly offer Easyprotect services, including extended warranties and product protection.",
    imgSrc: easyprotect,
    imgAlt: "Easyprotect",
    statistics: [
      { value: "3", name: "Month in development" },
      { value: "2", name: "Team members" },
      { value: "100+", name: "Installations" },
      { value: "100%", name: "Client satisfaction" }
    ],
    content1: "<h2>Problem to solve</h2><p><strong>Seamless Integration with E-commerce Platforms</strong>: Easyprotect needed an application that would integrate with the e-commerce platform idosell.com, enabling online stores to easily offer extended warranty and product protection services directly to their customers.</p>",
    scopeOfWork: [
      {
        title: "Development",
        list: ["We developed an application for idosell.com that allows online stores to easily integrate Easyprotect services, including extended warranties and protection options."]
      },
      {
        title: "Optimization",
        list: ["We optimized the user experience for both store owners and customers, simplifying the warranty management and claim processing workflow."]
      },
      {
        title: "Integration",
        list: ["We ensured seamless integration with e-commerce systems, enabling real-time synchronization and offering third-party protection services within online stores."]
      }
    ],
    content2: "<h2>Outcomes</h2><p>The Easyprotect application allows idosell.com-based stores to effortlessly offer product protection and extended warranties. The integration has improved customer satisfaction by simplifying warranty management and reducing product returns by 15%.</p>",
    review: {
      stars: 5,
      content: "The integration with idosell.com was smooth and the new warranty system is easy to use for both store owners and customers.",
      author: { name: "Wiktor Madej", company: "Business Partner Service Specialist, easyprotect.pl" }
    },
    content3: "<h2>Conclusion</h2><p>Our work with Easyprotect has resulted in a powerful e-commerce application that integrates seamlessly with idosell.com, enabling online stores to offer protection services and extended warranties with ease. This solution has enhanced the shopping experience for customers and streamlined operations for store owners.</p>",
    technologies: ["PHP", "MySQL", "JavaScript", "Bootstrap 5", "Docker", "Kubernetes", "OpenShift", "Proxmox VE"]
  }
];

const getPath = (slug?: string) => `/case-studies/${slug}${slug ? '/' : ''}`;

export default casestudies.map(item => ({
  ...item,
  url: getPath(textToPath(item.title))
}));
