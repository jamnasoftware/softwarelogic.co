import imker from "./reviews/imker.jpeg";
import timecamp from "./reviews/timecamp.jpeg";
import skinwallet from "./reviews/skinwallet.jpeg";
import dropui from "./reviews/dropui.jpeg";

export default [
    {
        content: "We have been collaborating with softwarelogic.co for several months on developing our Windows and Linux applications. Their technical knowledge and commitment have noticeably improved the performance and stability of our software.",
        author: {
            img: timecamp,
            name: "Kamil Rudnicki",
            company: "CEO timecamp.com"
        },
        recommendation: "Highly recommended!"
    },
    {
        content: "We partnered with softwarelogic.co for Node.js, React.js, and Next.js outsourcing services. Their technical proficiency and professionalism ensured smooth project execution and great results.",
        author: {
            img: skinwallet,
            name: "Kornel Szwaja",
            company: "CEO skinwallet.com"
        },
        recommendation: "Highly recommended!"
    },
    {
        content: "Softwarelogic.co is helping us develop a low-code system integrated with AI for generating modal windows and notifications. Their innovative approach and technical expertise are enhancing the platform's efficiency and user-friendliness.",
        author: {
            img: dropui,
            name: "Konrad Kur",
            company: "CTO dropui.com"
        },
        recommendation: "Highly recommended!"
    },
    {
        content: "We have been working with softwarelogic.co for over two years, enhancing our fulfillment operations with a robust Order Management System. Their expertise in both technology and business has made the collaboration very effective.",
        author: {
            img: imker,
            name: "Krzysztof Bartnik",
            company: "CEO imker.pl"
        },
        recommendation: "Highly recommended!"
    },
    {
        content: "For over four years, we have been closely involved with softwarelogic.co in developing custom eCommerce solutions with a focus on integrations. They have provided essential support in areas like marketing, SEO, and UX/UI, helping us achieve our shared goals.",
        author: {
            img: "default",
            name: "Mateusz Lasota",
            company: "CEO iso-trade.eu"
        },
        recommendation: "Highly recommended!"
    }
]





