/** @type {import('next').NextConfig} */

const nextConfig = {
  trailingSlash: true,
  reactStrictMode: true,
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'serwer2272706.home.pl',
        port: '',
        pathname: '/autoinstalator/wordpressplugins2/wp-content/uploads/**',
      },
    ],
  }
};

module.exports = nextConfig;
