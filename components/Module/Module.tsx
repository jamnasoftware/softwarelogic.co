import React from 'react';
import styles from './Module.module.scss';

interface IModuleProps {
  children: React.ReactNode;
  className?: string,
  id?: string,
}

const Module = (props: IModuleProps) => {
  return (
    <div id={props.id} className={ `${ styles["module"] } ${ props.className || "" }` }>
      { props.children }
    </div>
  );
};

export default Module;