import React from 'react';
import styles from './Card.module.scss';

interface ICardProps {
  children: React.ReactNode;
  className?: string;
}

const Card = (props: ICardProps) => {
    return (
      <div className={ `${ styles["card"] } ${ props.className || "" }` }>
        { props.children }
      </div>
    );
};

export default Card;