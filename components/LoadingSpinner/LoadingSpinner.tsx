import React from "react";
import styles from "./LoadingSpinner.module.scss";
import Image from "next/image";
import icon from "./loader.svg";
import classNames from "classnames";

interface ILoadingSpinnerProps {
  className?: string;
}

const LoadingSpinner = (props: ILoadingSpinnerProps) => {
  return (
    <Image
      alt="loading"
      className={classNames(styles.spinner, props.className)}
      src={icon}
    />
  );
};

export default LoadingSpinner;
