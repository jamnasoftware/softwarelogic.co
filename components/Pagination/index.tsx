import React from "react";
import styles from "./style.module.scss";
import leftArrow from "./arrows/left.svg";
import rightArrow from "./arrows/right.svg";
import Image from "next/image";

interface Props {
  active?: string;
  hidePages?: boolean;
  page: number;
  pages: number;
  setPage: (page: number) => void;
}

const Pagination = ({ active, hidePages, page, pages, setPage }: Props) => {
  return (
    <div className={styles.pagination}>
      <button
        disabled={page === 0}
        onClick={() => {
          setPage(page - 1);
        }}
        className={styles.arrow}
      >
        <Image
          alt="left arrow"
          src={leftArrow}
        />
      </button>
      {!hidePages && (
        <>
          {new Array(pages).fill(true).map((_, index) => (
            <button
              key={index}
              aria-label={`page ${index + 1} ${index === page ? 'active' : ''}`}
              className={index === page ? styles.active : ''}
              style={{ background: index === page ? active : undefined }}
              onClick={() => setPage(index)}
            />
          ))}
        </>
      )}
      <button
        className={styles.arrow}
        disabled={page === pages - 1}
        onClick={() => {
          setPage(page + 1);
        }}
      >
        <Image
          alt="right arrow"
          src={rightArrow}
        />
      </button>
    </div>
  );
};

export default Pagination;
