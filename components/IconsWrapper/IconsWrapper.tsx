import styles from "./IconsWrapper.module.scss";

interface IconsWrapperProps {
    children: React.ReactNode;
    color: string;
}

const IconsWrapper = ({ children, color }: IconsWrapperProps) => (
    <div className={styles.iconsWrapper}>
        <svg width="73"
            height="80"
            viewBox="0 0 73 80"
            fill="none"
            xmlns="http://www.ws3.org/2000/svg">
            <path d="M42.063 1.56852L66.563 15.7145C68.3428 16.742 69.8209 18.2199 70.8484 19.9997C71.876 21.7795 72.417 23.7984 72.417 25.8535V54.1465C72.417 56.2016 71.876 58.2206 70.8484 60.0004C69.8209 61.7801 68.3428 63.258 66.563 64.2855L42.063 78.4315C40.2833 79.459 38.2645 80 36.2095 80C34.1545 80 32.1357 79.459 30.356 78.4315L5.854 64.2855C4.07415 63.258 2.59614 61.7801 1.56855 60.0004C0.540949 58.2206 -2.66714e-05 56.2016 9.8625e-10 54.1465L9.86251e-10 25.8545C-2.66714e-05 23.7994 0.540949 21.7805 1.56855 20.0007C2.59614 18.2209 4.07415 16.743 5.854 15.7155L30.354 1.56952C32.1338 0.541491 34.153 0.000175583 36.2084 4.27073e-08C38.2638 -0.000175497 40.283 0.540795 42.063 1.56852Z"
                fill={color}
            />
            {children}
        </svg>
    </div>
);

export default IconsWrapper;
