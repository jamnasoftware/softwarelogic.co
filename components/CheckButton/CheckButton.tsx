import React from "react";
import Link from "next/link";
import styles from "./CheckButton.module.scss";
import icon from "./icon.svg";
import Image from "next/image";

interface ICheckButtonProps {
  text: string;
  className?: string;
  to: string;
  link?: string;
  target?: string;
}

const CheckButton = (props: ICheckButtonProps) => {
  return (
    <Link href={props.to} className={`${styles["check-button"]} ${props.className || ""}`}>
      {props.text}
      <Image
        alt="arrow right"
        src={icon}
      />
    </Link>
  );
};

export default CheckButton;
