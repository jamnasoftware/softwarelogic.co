import Link from "next/link";
import React, { useContext } from "react";
import { NavItem as NavItemRS, NavLink } from "reactstrap";
import { AppContext } from "../../../store/app-context";
import styles from "./NavItem.module.scss";

interface INavItemProps {
  name: string;
  url?: string;
}

const NavItem = (props: INavItemProps) => {
  const appContext = useContext(AppContext);

  const NavLinkOnClickHandler = () => {
    appContext.scrollToSection(props.name);
    appContext.setShowMobileMenu(false);
  };

  return (
    <NavItemRS
      className={[
        styles["nav-item"],
        appContext.showMobileMenu ? styles["active"] : "",
      ].join(" ")}
    >
      {!!props.url && (
        <Link href={props.url} legacyBehavior passHref>
          <NavLink
            className={styles["nav-link"]}
          >
            {props.name}
          </NavLink>
        </Link>
      )}
      {!props.url && (
        <Link legacyBehavior href="/" passHref>
          <NavLink onClick={NavLinkOnClickHandler} className={styles["nav-link"]}>
            {props.name}
          </NavLink>
        </Link>
      )}
    </NavItemRS>
  );
};

export default NavItem;
