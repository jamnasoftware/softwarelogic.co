import React, { useContext } from "react";
import { Nav, Navbar, NavbarToggler } from "reactstrap";
import { AppContext } from "../../store/app-context";
import styles from "./MobileMenu.module.scss";
import NavItem from "./NavItem/NavItem";
import useWindowSize from "../../utils/hooks/use-window-size";

const MobileMenu = () => {
  const appContext = useContext(AppContext);
  const [windowWidth] = useWindowSize();

  return windowWidth < 992 ? (
    <div
      className={[
        styles["menu"],
        appContext.showMobileMenu ? styles["active"] : "",
      ].join(" ")}
      onClick={() => appContext.setShowMobileMenu(false)}
    >
      <Navbar className={styles["navbar"]}>
        <NavbarToggler className={styles["navbar-toggler"]}>
          <div className={styles["bar"]}></div>
          <div className={styles["bar"]}></div>
        </NavbarToggler>
        <Nav className={styles["nav"]} vertical={false}>
          <NavItem name="SERVICES" />
          <NavItem name="TESTIMONIALS" />
          <NavItem name="CASE STUDIES" />
          <NavItem name="BLOG" url="/blog/" />
          <NavItem name="CONTACT" />
        </Nav>
      </Navbar>
    </div>
  ) : null;
};

export default MobileMenu;
