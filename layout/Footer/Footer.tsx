import React from "react";
import { Col, Container, Row } from "reactstrap";
import styles from "./Footer.module.scss";
import { useContext } from "react";
import { AppContext } from "../../store/app-context";
import Link from "next/link";
import { textToPath } from "utils/helpers/main";
import technologies from "data/technologies";
import services from "data/services";
import logo from "public/icon/software-logic-logo_white.svg";
import Image from "next/image";
import facebook from "public/icon/facebook_white.svg";
import linkedin from "public/icon/linkedin_white.svg";
import gitlab from "public/icon/gitlab_white.svg";

const Footer = ({ copyrigth }: { copyrigth: string }) => {
  const appContext = useContext(AppContext);
  return (
    <footer className={styles.footer}>
      <Container>
        <Row className="mb-lg-5">
          <Col md={6} lg={2}>
            <Image alt="SoftwareLogic" className={styles.logo} src={logo} />
          </Col>
          <Col md={6} lg={4}>
            <p className={styles["description"]}>
              Innovating since 2018 - transforming businesses with cutting-edge technology!
            </p>
          </Col>
          <Col md={6} lg={{ size: 3, offset: 1 }}>
            <ul className={styles["footer-list"]}>
              <li>
                JAMNA SOFTWARE Sp. z o.o.
                <br />
                Stargard, Poland
                <br />
                <strong>200 km from Berlin</strong>
                <br />
                VAT Number PL8542432361
              </li>
            </ul>
          </Col>
          <Col md={6} lg={2} className="text-lg-end">
            <div className="mb-4">
              <a
                href="https://www.facebook.com/softwarelogic.co"
                target="_blank"
                rel="noreferrer"
              >
                <Image
                  alt="Facebook"
                  className={styles["social-icon"]}
                  src={facebook}
                />
              </a>
              <a
                href="https://www.linkedin.com/company/softwarelogic-co/"
                target="_blank"
                rel="noreferrer"
              >
                <Image
                  alt="Linkedin"
                  className={styles["social-icon"]}
                  src={linkedin}
                />
              </a>
              <a
                href="https://gitlab.com/users/jamnasoftware/projects"
                target="_blank"
                rel="noreferrer"
              >
                <Image
                  alt="Gitlab"
                  className={styles["social-icon"]}
                  src={gitlab}
                />
              </a>
            </div>
            <div
              className="clutch-widget mb-4"
              data-nofollow="true"
              data-url="https://widget.clutch.co"
              data-widget-type="2"
              data-height="45"
              data-clutchcompany-id="1317804"
              data-primary-color="#58089F"
            ></div>
          </Col>
        </Row>
        <Row>
          <Col lg={2}>
            <div className={`${styles.list} mt-lg-0`}>
              <strong>MENU</strong>
              <Row>
                {[
                  "SERVICES",
                  "CASE STUDIES",
                  "BLOG",
                  "CONTACT",
                  "TESTIMONIALS",
                ].map((section) => (
                  <Col key={section} xs={6} lg={12}>
                    <Link legacyBehavior href="/">
                      <a onClick={() => appContext.scrollToSection(section)}>
                        {section}
                      </a>
                    </Link>
                  </Col>
                ))}
                <Col xs={6} lg={12}>
                  <Link href="/sitemap">SITE MAP</Link>
                </Col>
              </Row>
            </div>
          </Col>

          <Col lg={5}>
            <div className={`${styles.list} mt-lg-0`}>
              <strong>SERVICES</strong>
              <Row>
                {services.map((item) => (
                  <Col key={item.title} xs={6}>
                    <Link href={`/services/${textToPath(item.title)}`}>
                      {item.title}
                    </Link>
                  </Col>
                ))}
              </Row>
            </div>
          </Col>

          <Col lg={5}>
            <div className={`${styles.list} mt-lg-0`}>
              <strong>TECHNOLGIES</strong>
              <Row>
                {Object.entries(technologies).map(([key, item]) => (
                  <Col key={item.title} xs={6} md={3}>
                    <Link href={`/technology/${key}`}>{item.title}</Link>
                  </Col>
                ))}
              </Row>
            </div>
          </Col>
        </Row>
        <div className={styles["copyrights"]}>
          <hr />
          <p>Crafted in Stargard, Poland ❤️</p>
          <p>{copyrigth}</p>
        </div>
      </Container>
    </footer>
  );
};

export default Footer;
