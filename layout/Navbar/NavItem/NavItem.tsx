import Link from "next/link";
import React, { useContext } from "react";
import { NavLink, NavItem as NavItemRS } from "reactstrap";
import { AppContext } from "../../../store/app-context";

import styles from "./NavItem.module.scss";

interface INavItemProps {
  name: string;
  url?: string;
}

const NavItem = (props: INavItemProps) => {
  const appContext = useContext(AppContext);

  return (
    <NavItemRS className={styles["nav-item"]}>
      {!!props.url && (
        <Link href={props.url} legacyBehavior passHref>
          <NavLink
            className={styles["nav-link"]}
          >
            <span>{props.name}</span>
          </NavLink>
        </Link>
      )}
      {!props.url && (
        <Link legacyBehavior href="/" passHref>
          <NavLink
            onClick={() => appContext.scrollToSection(props.name)}
            className={styles["nav-link"]}
          >
            <span>{props.name}</span>
          </NavLink>
        </Link>
      )}
    </NavItemRS>
  );
};

export default NavItem;
