import React, { useContext } from "react";
import styles from "./Navbar.module.scss";
import {
  Nav,
  NavbarBrand,
  Navbar as NavbarRS,
  NavbarToggler,
} from "reactstrap";
import NavItem from "./NavItem/NavItem";
import useWindowSize from "../../utils/hooks/use-window-size";
import { AppContext } from "../../store/app-context";
import Image from "next/image";
import logo from "public/icon/software-logic-logo_purple.svg";

const Navbar = () => {
  const [windowWidth] = useWindowSize();
  const appContext = useContext(AppContext);
  const handleEmailButtonClick = () => {
    window.location.href =
      "mailto:office@softwarelogic.co?subject=Estimate%20Project";
  };
  return (
    <div className={styles.container}>
      <NavbarRS
        light
        expand={windowWidth > 991}
        className={styles["nav-bar"]}
        container
      >
        <NavbarBrand href="/" className={styles["logo"]}>
          <Image alt="SoftwareLogic" src={logo} />
        </NavbarBrand>
        {windowWidth > 991 && (
          <Nav className={styles["nav"]} vertical={false}>
            <NavItem name="SERVICES" />
            <NavItem name="TESTIMONIALS" />
            <NavItem name="CASE STUDIES" />
            <NavItem name="BLOG" url="/blog/" />
            <NavItem name="CONTACT" />
            <li>
              <button
                onClick={handleEmailButtonClick}
                style={{
                  padding: "12px 15px",
                  backgroundColor: "var(--green-light)",
                  border: "none",
                  outline: "none",
                  fontWeight: "500",
                  color: "var(--purple)",
                  textDecoration: "none",
                  fontSize: "16px",
                  display: "inline-block",
                }}
              >
                📝 Get an Estimate
              </button>
            </li>
          </Nav>
        )}
        {windowWidth <= 991 && (
          <NavbarToggler
            onClick={() => appContext.setShowMobileMenu(true)}
            className={styles["navbar-toggler"]}
          >
            <div className={styles["bar"]}></div>
            <div className={styles["bar"]}></div>
            <div className={styles["bar"]}></div>
          </NavbarToggler>
        )}
      </NavbarRS>
    </div>
  );
};

export default Navbar;
