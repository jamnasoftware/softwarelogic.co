import React, { ReactNode, useContext, useEffect } from "react";
import Navbar from "./Navbar/Navbar";
import { Container } from "reactstrap";
import styles from "./style.module.scss";
import Footer from "./Footer/Footer";
import { AppContext } from "../store/app-context";
import MobileMenu from "./MobileMenu/MobileMenu";

interface Props {
  children: ReactNode
}

const Layout = ({ children }: Props) => {
  const appContext = useContext(AppContext);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const copyright = `Copyright ${new Date().getFullYear()} @ softwarelogic.co`

  return (
    <div>
      <MobileMenu />
      <Navbar />
      {children}
      <div className={styles.contact}>
        <Container>
          <h2 ref={appContext.contactTitleRef}>
            Revolutionize your business with cutting-edge technology!
          </h2>
          <p>
          Let’s Bring Your Vision to Life—Contact Us Today!
          </p>
          <a className="contact-us-button" href="mailto:office@softwarelogic.co?subject=Estimate%20Project">
            📝 Get an Estimate
          </a>
        </Container>
      </div>
      <Footer copyrigth={copyright} />
    </div>
  );
};

export default Layout;
