import React, { useCallback, useRef } from "react";
import { useState } from "react";
import { scrollToTargetAdjusted } from "utils/helpers/main";

interface IAppContext {
  offerTitleRef: React.RefObject<HTMLHeadingElement> | null;
  workTitleRef: React.RefObject<HTMLHeadingElement> | null;
  realizationsTitleRef: React.RefObject<HTMLHeadingElement> | null;
  newsTitleRef: React.RefObject<HTMLHeadingElement> | null;
  testimonialsTitleRef: React.RefObject<HTMLHeadingElement> | null;
  contactTitleRef: React.RefObject<HTMLHeadingElement> | null;
  showMobileMenu: boolean;
  setShowMobileMenu: (value: boolean) => void;
  scrollToSection: (sectionName: string) => void;
}

interface IAppContextProviderProps {
  children: React.ReactNode;
}

const initialAppContext: IAppContext = {
  offerTitleRef: null,
  workTitleRef: null,
  realizationsTitleRef: null,
  newsTitleRef: null,
  testimonialsTitleRef: null,
  contactTitleRef: null,
  showMobileMenu: false,
  setShowMobileMenu: () => undefined,
  scrollToSection: () => undefined,
};

const AppContext = React.createContext(initialAppContext);

const AppContextProvider = (props: IAppContextProviderProps) => {
  const offerTitleRef = useRef<HTMLHeadingElement>(null);
  const workTitleRef = useRef<HTMLHeadingElement>(null);
  const realizationsTitleRef = useRef<HTMLHeadingElement>(null);
  const newsTitleRef = useRef<HTMLHeadingElement>(null);
  const contactTitleRef = useRef<HTMLHeadingElement>(null);
  const [showMobileMenu, setShowMobileMenu] = useState<boolean>(false);
  const testimonialsTitleRef = useRef<HTMLHeadingElement>(null);

  const scrollToSection = useCallback((sectionName: string) => {
    setTimeout(() => {
      switch (sectionName.toUpperCase()) {
        case "WORKS":
          workTitleRef &&
            workTitleRef.current &&
            scrollToTargetAdjusted(workTitleRef.current, 98);
          break;
        case "SERVICES":
          offerTitleRef &&
            offerTitleRef.current &&
            scrollToTargetAdjusted(offerTitleRef.current, 98);
          break;
        case "BLOG":
          newsTitleRef &&
            newsTitleRef.current &&
            scrollToTargetAdjusted(newsTitleRef.current, 128);
          break;
        case "TESTIMONIALS":
          testimonialsTitleRef &&
            testimonialsTitleRef.current &&
            scrollToTargetAdjusted(testimonialsTitleRef.current, 128);
          break;
        case "CASE STUDIES":
          realizationsTitleRef &&
            realizationsTitleRef.current &&
            scrollToTargetAdjusted(realizationsTitleRef.current, 128);
          break;
        case "CONTACT":
          contactTitleRef &&
            contactTitleRef.current &&
            scrollToTargetAdjusted(contactTitleRef.current, 208);
          break;
        default:
          return;
      }
    }, 100);
  }, []);

  return (
    <AppContext.Provider
      value={{
        offerTitleRef,
        workTitleRef,
        realizationsTitleRef,
        newsTitleRef,
        testimonialsTitleRef,
        contactTitleRef,
        showMobileMenu,
        setShowMobileMenu,
        scrollToSection,
      }}
    >
      {props.children}
    </AppContext.Provider>
  );
};

export { AppContext, AppContextProvider };
