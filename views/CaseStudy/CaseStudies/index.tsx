import React, { useContext } from "react";
import { Col, Container, Row } from "reactstrap";
import styles from "./style.module.scss";
import { useState } from "react";
import { AppContext } from "../../../store/app-context";
import { textToPath } from "../../../utils/helpers/main";
import Image from "next/image";
import realizationsData from "data/casestudies";
import Link from "next/link";
import Pagination from "components/Pagination";
import arrow from "./arrow-up-right.svg";

const Realizations = () => {
  const [current, setCurrentRealization] = useState<number>(0);
  const appContext = useContext(AppContext);
  const [page, setPage] = useState(0);
  const perPage = 3;
  const pages = Math.ceil(realizationsData.length / perPage);
  return (
    <div className={styles.container}>
      <Container>
        <header>
          <h2
            ref={appContext.realizationsTitleRef}
            className={styles.title}
          >
            More case studies
          </h2>
          <Link
              href="/case-studies"
          >
            View all case studies
          </Link>
        </header>
        <p>
          Discover the inspiring stories of our clients projects.
        </p>
        <Row>
          {realizationsData.slice(page * perPage, page * perPage + perPage).map((item, index) => (
            <Col key={index} lg={4}>
              <Link
                href={`/case-studies/${textToPath(item.title)}/`}
              >
                <div className={styles.img}>
                  <Image
                    alt={item.imgAlt}
                    className="img-fluid"
                    src={item.imgSrc}
                  />
                </div>
              </Link>
              <h3 className={styles.title}>{item.title}</h3>

              <p className={styles.desc}>
                {item.desc}
              </p>
              <Link
                className={styles.readMore}
                href={`/case-studies/${textToPath(item.title)}/`}
              >
                Read more
                <Image
                  alt="arrow"
                  src={arrow}
                />
              </Link>
            </Col>
          ))}
        </Row>
        <Pagination
          active="#32EB51"
          page={page}
          pages={pages}
          setPage={setPage}
        />
      </Container>
    </div>
  );
};

export default Realizations;
