import React from "react";
import styles from "./style.module.scss";
import { useRouter } from "next/router";
import Head from "next/head";
import { Col, Container, Row } from "reactstrap";
import { textToPath } from "utils/helpers/main";
import caseStudies from "data/casestudies";
import { ParallaxBanner } from "react-scroll-parallax";
import Image from "next/image";
import review from "./review.png";
import star from "./star.svg";
import Realizations from "./CaseStudies";
import Technology from "./Technologies";

const Clients = () => {
  const router = useRouter();
  const { id } = router.query;
  const client = caseStudies.find(data => textToPath(data.title) === id);

  if (!client) {
    return (
      <Container className={`${styles["container"]} py-5`}>
        404
      </Container>
    );
  }
  const { title, desc, imgSrc, imgAlt } = client;
  return (
    <>
      <Head>
        <title>{title}</title>
        <meta key="ogimage" property="og:image" content="https://softwarelogic.co/og/casestudy.png" />
      </Head>
      <ParallaxBanner
        layers={[{ image: "/hero.webp", speed: -15 }]}
        className={styles.hero}
      >
        <header className="container">
          <small>
            Case study 🗂️&nbsp;&nbsp;&nbsp; 8 min read
          </small>
          <h1 className={styles["title"]}>{title}</h1>
          <div className={styles["content"]} dangerouslySetInnerHTML={{ __html: desc }} />
        </header>
      </ParallaxBanner>
      <Container className={styles.container}>
        <div className={styles.img}>
          <Image
            alt={imgAlt}
            className="img-fluid"
            src={imgSrc}
            width={1200}
            height={752}
          />
        </div>
      </Container>

      <Container className={styles.container2}>
        <div className={styles.stats}>
          <Row>
            {client.statistics?.map(stat => (
              <Col key={stat.name}>
                <strong>
                  {stat.value}
                </strong>
                {stat.name}
              </Col>
            ))}
          </Row>
        </div>
      </Container>

      <Container className={styles.container3} dangerouslySetInnerHTML={{ __html: client.content1 }}>
      </Container>
      <div className={styles.scope}>
        <Container>
          <h2>
            Scope of work
          </h2>
          <Row>
            {client.scopeOfWork.map((v, i) => (
              <Col key={i} lg={4} className="mb-4 mb-lg-0">
                <div className={styles.card}>
                  <i className={styles.category}>
                    <img src="/icon/lightning.svg" />
                  </i>
                  <div className={styles.content}>
                    <h3 className={styles.title}>
                      {v.title}
                    </h3>
                    {v.list}
                  </div>
                </div>
              </Col>
            ))}
          </Row>
        </Container>
      </div>


      <Container className={styles.container3} dangerouslySetInnerHTML={{ __html: client.content2 }}>
      </Container>

      <div className={styles.review}>
        <Container>
          <div className={styles.stars}>
            {new Array(client.review.stars).fill(true).map((v, i) => (
              <Image
                key={i}
                alt="star"
                src={star}
              />
            ))}
          </div>
          <p>
            {client.review.content}
          </p>
          <div>
            <div className={styles.author}>
              {client.review?.author.name}
            </div>
            <div className={styles.company}>
              {client.review?.author.company}
            </div>
          </div>
        </Container>
      </div>

      <Container className={styles.container3}>
        <div className={styles.conclusion} dangerouslySetInnerHTML={{ __html: client.content3 }} />
      </Container>

      {client.technologies && (
        <Technology technologies={client.technologies} />
      )}

      <Realizations />

      <script type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: JSON.stringify({
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": title,
            "description": desc,
          })
        }}
      />
    </>
  );
};

export default Clients;
