import React from "react";
import styles from "./style.module.scss";
import { Col, Container, Row } from "reactstrap";
import Link from "next/link";

const Technology = ({ technologies }: { technologies: string[] }) => {
  return (
    <Container className={styles.container}>
      <h2>Technologies used</h2>
      <p className="pb-3">We applied our expertise in the following technologies:</p>
      <Row>
        {technologies.map((t) => (
          <Col xs={{ size: 6 }} md={{ size: 3 }} className={styles["column"]} key={t}>
            <div>
              {t}
            </div>
          </Col>
        ))}
      </Row>
    </Container>
  );
};

export default Technology;
