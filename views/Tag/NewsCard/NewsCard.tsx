import React from "react";
import Card from "../../../components/Card/Card";
import CheckButton from "../../../components/CheckButton/CheckButton";
import styles from "./NewsCard.module.scss";
import Link from "next/link";

interface INewsCardProps {
  title: string;
  content: string;
  date: string;
  slug: string;
  minutesToRead?: number;
}

const NewsCard = ({ minutesToRead, ...props }: INewsCardProps) => {
  const date = props.date
    ? `${props.date.slice(8, 10)}.${props.date.slice(5, 7)}.${props.date.slice(
      0,
      4
    )}`
    : null;

  return (
    <Card className={styles["card"]}>
      <h2 className={styles.title}>
        <Link
          href={`/blog/${props.slug}/`}
          dangerouslySetInnerHTML={{
            __html: props.title
          }}
        />
      </h2>
      {date !== null && (
        <p className={styles["date"]}>
          {date}
          {minutesToRead && `, ${minutesToRead} minutes to read`}
        </p>
      )}
      <div
        className={styles["content"]}
        dangerouslySetInnerHTML={{
          __html: props.content,
        }}
      />
      <CheckButton
        to={`/blog/${props.slug}/`}
        className={styles["check-button"]}
        text="read more"
      />
    </Card>
  );
};

export default NewsCard;
