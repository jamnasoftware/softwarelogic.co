import React from "react";
import styles from "./News.module.scss";
import { Col, Container, Row } from "reactstrap";
import NewsCard from "./NewsCard/NewsCard";
import { textToPath } from "utils/helpers/main";
import { WP_REST_API_Tag } from "wp-types";

type Post = {
  title: string;
  content: string;
  date: string;
  minutesToRead: number;
}
type Props = {
  tag: WP_REST_API_Tag;
  posts: Post[];
  total: number;
  page: number;
}

const Tag = ({ tag, posts = [] }: Props) => {
  return (
    <Container className={`${styles["news-container"]}`}>
      <h1 className="text-center">
        Blog: {tag?.name}
      </h1>
      <Row>
        {posts.length > 0 ? (
          posts.map((post: any, index: number) => (
            <Col style={{ paddingBottom: 15 }} key={index} xs={{ size: 12 }} md={{ size: 6 }} lg={{ size: 4 }}>
              <NewsCard
                {...post}
              />
            </Col>
          ))
        ) : (
          <p>Loading error.</p>
        )}
      </Row>

      <script type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: JSON.stringify({
            "@context": "https://schema.org/",
            "@type": "Blog",
            "@id": "https://softwarelogic.com/blog/",
            "mainEntityOfPage": "https://softwarelogic.com/blog/",
            "name": "Softwarelogic Blog",
            "blogPost": posts.map(post => ({
              "@type": "BlogPosting",
              "@id": `/blog/${textToPath(post.title)}`,
              "mainEntityOfPage": `/blog/${textToPath(post.title)}`,
              "headline": post.title,
              "name": post.title,
              "description": post.content,
              datePublished: new Date(post.date).toISOString(),
              dateModified: new Date(post.date).toISOString(),
              url: `/blog/${textToPath(post.title)}`,
              image: "https://softwarelogic.co/webp/main-hero.webp",
              author: {
                name: "admin"
              }
            })),
          })
        }}
      />
    </Container>
  );
};

export default Tag;
