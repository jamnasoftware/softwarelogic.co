import React, { useContext } from "react";
import styles from "./styles.module.scss";
import { ParallaxBanner } from "react-scroll-parallax";
import { Col, Container, Row } from "reactstrap";
import { AppContext } from "store/app-context";
import Head from "next/head";
import services from "data/services";
import OfferCard from "./OfferCard/OfferCard";

const Home = () => {
  const appContext = useContext(AppContext);
  return (
    <>
      <Head>
        <meta key="ogimage" property="og:image" content="https://softwarelogic.co/og/services.png" />
      </Head>
      <ParallaxBanner
        layers={[{ image: "/hero.webp", speed: -15 }]}
        className={`${styles["hero"]}`}
      >
        <Container>
          <div
            className={styles["hero-text"]}
          >
            Tailored Software Solutions for&nbsp;Your Business Needs
          </div>
          <div
            className={styles["hero-subtext"]}
          >
            Partner with us to transform your digital landscape and unlock your business's full potential.
          </div>
          <button className={styles.explore} onClick={() => appContext.scrollToSection("SERVICES")} >
            Explore
            <img src="/icon/arrow-down.svg" />
          </button>
        </Container>
      </ParallaxBanner>

      <Container className={styles.container}>
        <h2 ref={appContext.offerTitleRef}>
          Services
        </h2>
        <p>
          We offer extensive expertise in:
        </p>
        <Row>
          {services.map((offer, index) => (
            <Col key={index} className={styles["card-column"]} xs={{ size: 12 }} sm={{ size: 6 }} md={{ size: 4 }}>
              <OfferCard imgSrc={offer.imgSrc} title={offer.title}>
                {offer.content}
              </OfferCard>
            </Col>
          ))}
        </Row>
      </Container>
    </>
  );
};

export default Home;
