import Image from "next/image";
import Link from "next/link";
import { useParams } from "next/navigation";
import { Col, Row } from "reactstrap";
import styles from "./styles.module.scss";
import { useEffect, useState } from "react";
import { WP_REST_API_Post, WP_REST_API_Posts } from "wp-types";

import arrow from "./arrow-up-right.svg";
import placeholder from "./placeholder.png";

const Other = () => {
  const { id } = useParams() || {};
  const [state, setState] = useState<WP_REST_API_Posts>([]);
  useEffect(() => {
    const fetchData = async () => {
      const posts = await fetch(
        `https://serwer2272706.home.pl/autoinstalator/wordpressplugins2/wp-json/wp/v2/posts?per_page=4`
      ).then(r => r.json());
      setState(posts.map((post: WP_REST_API_Post) => ({
        ...post,
        image: post.content.rendered.match(/<img.*src="(\S*)"/)?.[1],
        minutesToRead: Math.round(post.content.rendered.split(' ').length / 200),
      })));
    }
    fetchData();
  }, []);
  const filtered = state.filter((post: WP_REST_API_Post) => post.slug !== id).slice(0, 3);
  return (
    <div className={styles.container}>
      <h2>
        Other blog posts
      </h2>
      <Row>
        {filtered.map((item, index) => (
          <Col key={index} lg={4}>
            <Link
              href={`/blog/${item.slug}/`}
            >

              <div className={styles.img}>
                <Image
                  alt="placeholder"
                  className="img-fluid"
                  src={item.image as string || placeholder}
                  width={1024}
                  height={585}
                />
              </div>
              <header className={styles.header}>
                <h3 className={styles.title}
                  dangerouslySetInnerHTML={{
                    __html: item.title.rendered
                  }}
                />
                <Image
                  alt="arrow"
                  src={arrow}
                />
              </header>

              <div className={styles.desc}
                dangerouslySetInnerHTML={{
                  __html: item.excerpt.rendered,
                }}
              />
              <div className={styles.footer}>
                {(item.minutesToRead as string) && `${item.minutesToRead} minutes to read`}
                <div className={styles.date}>
                  {`${item.date.slice(8, 10)}.${item.date.slice(5, 7)}.${item.date.slice(
                    0,
                    4
                  )}`}
                </div>
              </div>

            </Link>
          </Col>
        ))}
      </Row>
    </div>
  )
}

export default Other;
