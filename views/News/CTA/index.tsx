import Link from "next/link";
import styles from "./styles.module.scss";

import dropui from "/public/logos/dropui.png";
import timecamp from "/public/logos/timecamp.png";
import skinwallet from "/public/logos/skinwallet.png";
import fffrree from "/public/logos/fffrree.png";
import imker from "/public/logos/imker.png";
import Image from "next/image";

const logos = [dropui, timecamp, skinwallet, fffrree, imker];

const CTA = () => (
  <>
    <div className={styles.container}>
      <h2 className={styles.title}>
        Want to more know?
      </h2>
      <p>
        Explore our case studies to see how we have halped our clients achieve their goals. Discover real-world examples of our work and the results we have delivered. Get inspired and see how we can help your business succed.
      </p>
      <Link
        href="/case-studies/"
      >
        Check case studies
      </Link>
    </div>
    <div className={styles.trusted}>
      <p>
        Trusted by:
      </p>
      <div>
        {logos.map(img => (
          <Image
            alt="clients logo"
            src={img}
          />
        ))}
      </div>
    </div>
  </>
);

export default CTA;
