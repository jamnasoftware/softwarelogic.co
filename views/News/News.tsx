import React, { useEffect } from "react";
import LoadingSpinner from "components/LoadingSpinner/LoadingSpinner";
import Module from "components/Module/Module";
import styles from "./News.module.scss";
import { useRouter } from "next/router";
import Head from "next/head";
import { Col, Container, Row } from "reactstrap";
import parse from "html-react-parser";
import { createRoot } from "react-dom/client";
import { WP_REST_API_Post } from "wp-types";
import placeholder from "./image.png";
import Image from "next/image";
import Other from "./Other";
import CTA from "./CTA";

const stripHTML = (htmlString: string) => htmlString.replace(/(<([^>]+)>)/ig, '');;

const News = ({ newsData }: { newsData: WP_REST_API_Post }) => {
  const router = useRouter();

  useEffect(() => {
    if (typeof document === "undefined") {
      return;
    }
    let cta = document.getElementById("cta");
    if (!cta) {
      const pNodes = document.querySelectorAll("h2");
      const pNode = pNodes[Math.floor(pNodes.length / 3)];
      cta = document.createElement("div");
      cta.setAttribute("id", "cta");
      pNode.before(cta);
    }
    if (cta!.innerText.length) {
      return;
    }
    const root = createRoot(cta!);
    root.render(<CTA />)
  }, []);

  if (!newsData) {
    return (
      <Module className={styles["news-container"]}>
        <LoadingSpinner />
      </Module>
    );
  }

  const date = new Date(Date.parse(newsData?.date)).toLocaleDateString("en-us", {
    weekday: "long",
    year: "numeric",
    month: "numeric",
    day: "numeric",
  });

  const minutesToRead = Math.round(newsData.content.rendered.split(' ').length / 200);
  const image = newsData.content.rendered.match(/<img.*src="(\S*)"/)?.[1] || null;
  const headers = [...newsData.content.rendered.matchAll(/<h2>(.*)<\/h2>/g)];
  const nav = headers.map(header => header[1]);
  return (
    <Container className={`${styles["news-container"]}`}>
      <Head>
        <title>{parse(newsData.title.rendered)}</title>
        <meta property="og:type" content="article" key="type" />
        <meta property="og:title" content={newsData.title.rendered} key="title" />
        <meta property="og:description" content={stripHTML(newsData.excerpt.rendered)} key="description" />
        <meta property="og:url" content={`https://softwarelogic.co${router.asPath}`} key="url" />
        {image && (
          <meta property="og:image" content={image} key="ogimage" />
        )}
      </Head>
      <header>
        <h1 className={styles["title"]} dangerouslySetInnerHTML={{ __html: newsData?.title?.rendered }} />
        <p className={styles["date"]}>{date}, {minutesToRead} minutes to read</p>
      </header>
      <Image
        alt="image"
        className="img-fluid"
        src={image || placeholder}
        width={1024}
        height={585}
      />

      <Row>
        <Col lg={{ size: 3, offset: 1 }}>
          <nav>
            <h3>
              Table of contents
            </h3>
            <ol>
              {nav.map(item => (
                <li key={item}>
                  <a href="#"
                    dangerouslySetInnerHTML={{
                      __html: item,
                    }}
                    onClick={(e) => {
                      e.preventDefault();
                      const h2s = document.querySelectorAll('h2');
                      const h2 = [...h2s].find(node => node.innerText === item.replaceAll('&amp;', '&'));
                      if (!h2) {
                        return;
                      }
                      h2.scrollIntoView();
                    }}
                  />
                </li>
              ))}
            </ol>
          </nav>
        </Col>
        <Col lg={{ size: 6, offset: 1 }}>
          <div className={styles["content"]} dangerouslySetInnerHTML={{ __html: newsData?.content?.rendered }} />
        </Col>
      </Row>

      {newsData && (
        <script type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "https://schema.org/",
              "@type": "BlogPosting",
              "headline": newsData.title.rendered,
              "name": newsData.title.rendered,
              "description": stripHTML(newsData.excerpt.rendered),
              datePublished: new Date(newsData.date).toISOString(),
              dateModified: new Date(newsData.modified).toISOString(),
              image: "https://softwarelogic.co/webp/main-hero.webp",
              author: {
                name: "admin"
              }
            })
          }}
        />
      )}
      <Other />
    </Container>
  );
};

export default News;
