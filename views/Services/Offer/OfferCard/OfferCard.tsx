import React from "react";
import styles from "./OfferCard.module.scss";
import CheckButton from "../../../../components/CheckButton/CheckButton";
import { textToPath } from "../../../../utils/helpers/main";
import icon from "./code-bracket.svg";
import Image from "next/image";

interface ICardProps {
  title: string;
  imgSrc: string;
  children: React.ReactNode;
}

const OfferCard = (props: ICardProps) => {
  return (
    <div className={styles.card}>
      <Image
        alt="icon"
        src={icon}
      />
      <h3 className={styles.title}>{props.title}</h3>
      <p>
        {props.children}
      </p>
      <CheckButton to={`/services/${textToPath(props.title)}/`} className={styles["check-button"]} text="Read more" />
    </div>
  );
};

export default OfferCard;
