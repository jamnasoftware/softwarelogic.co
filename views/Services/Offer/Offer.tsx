import React from "react";
import { Col, Container, Row } from "reactstrap";
import OfferCard from "./OfferCard/OfferCard";
import styles from "./Offer.module.scss";
import services from "data/services";

const Offer = () => {
  return (
    <Container className={styles.container}>
      <h2>
        Other services
      </h2>
      <Row>
        {services.map((offer, index) => (
          <Col key={index} className={styles["card-column"]} xs={{ size: 12 }} sm={{ size: 6 }} md={{ size: 4 }}>
            <OfferCard imgSrc={offer.imgSrc} title={offer.title}>
              {offer.content}
            </OfferCard>
          </Col>
        ))}
      </Row>
    </Container>
  );
};

export default Offer;