import React from "react";
import styles from "./index.module.scss";
import Head from "next/head";
import { Col, Container, Row } from "reactstrap";
import parse from "html-react-parser";
import { ParallaxBanner } from "react-scroll-parallax";
import image from "./Image.png"
import Image from "next/image";
import Offer from "./Offer/Offer";
import gear from "./gear.svg";
import icon from "./icon.svg";

const Services = ({ title, content, about, about2, takeaways, why }: { title: string, content: string, about: string, about2: string, takeaways: string[], why: { title: string, desc: string }[] }) => {
  return (
    <>
      <Head>
        <title>{parse(title)}</title>
        <meta key="ogimage" property="og:image" content="https://softwarelogic.co/og/service.png" />
      </Head>

      <ParallaxBanner
        layers={[{ image: "/hero.webp", speed: -15 }]}
        className={`${styles["hero"]}`}
      >
        <Container>
          <Row>
            <Col lg={5}>
              <div
                className={styles["hero-text"]}
                dangerouslySetInnerHTML={{ __html: title }}
              />
              <div
                className={styles["hero-subtext"]}
              >
                {content}
              </div>
            </Col>
            <Col lg={{ size: 5, offset: 2 }}>
              <Image
                alt={title}
                className="img-fluid"
                src={image}
              />
            </Col>
          </Row>
        </Container>
      </ParallaxBanner>

      <Container className={styles.container1}>
        <Row>
          <Col lg={4}>
            <h2>
              About the service
            </h2>
            <Image
              alt="about"
              src={icon}
            />
          </Col>
          <Col lg={4}>
            <p>
              {about}
            </p>
          </Col>
          <Col lg={4}>
            <p>
              {about2}
            </p>
          </Col>
        </Row>
      </Container>

      <Container className={styles.container2}>
        <h2>
          Key Takeaways
        </h2>
        <Row>
          {takeaways.map((v, i) => (
            <Col key={i} lg={4}>
              <div className={styles.card}>
                <strong>
                  {String(i + 1).padStart(2)}
                </strong>
                {v}
              </div>
            </Col>
          ))}
        </Row>
      </Container>

      <div className={styles.container3}>
        <Container>
          <h2>
            Why Choose Our {title} Services?
          </h2>
          <Row>
            {why.map((item, i) => (
              <Col key={i} lg={4}>
                <Image
                  alt="gear"
                  src={gear}
                />
                <h3>
                  {item.title}
                </h3>
                <p>
                  {item.desc}
                </p>
              </Col>
            ))}
          </Row>
        </Container>
      </div>
      <Offer />
    </>
  );
};

export default Services;
