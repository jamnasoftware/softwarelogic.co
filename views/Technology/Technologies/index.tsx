import React from "react";
import styles from "./styles.module.scss";
import { Col, Container, Row } from "reactstrap";
import Link from "next/link";
import technologies from "data/technologies";

const list = Object.entries(technologies).map(([key, value]) => ({
  title: value.title,
  url: `/technology/${key}`,
}));

const Technologies = () => {
  return (
    <Container className={styles.container}>
      <h2 className="mb-1">Other technologies</h2>
      <p className="pb-3">
        We offer extensive expertise in:
      </p>
      <Row>
        {list.map((t) => (
          <Col xs={{ size: 6 }} md={{ size: 3 }} className={styles["column"]} key={t.title}>
            <Link
              href={t.url}
            >
              {t.title}
            </Link>
          </Col>
        ))}
      </Row>
    </Container>
  );
};

export default Technologies;
