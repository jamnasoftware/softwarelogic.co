import React from "react";
import styles from "./index.module.scss";
import Head from "next/head";
import { Col, Container, Row } from "reactstrap";
import parse from "html-react-parser";
import { ParallaxBanner } from "react-scroll-parallax";
import Image from "next/image";
import icon from "./icon.svg";
import Technologies from "./Technologies";
import gear from "./gear.svg";
import placeholder from "./placeholder.png";

type Props = {
  title: string,
  section1: {
    title: string,
    column1: string,
    column2: string
  },
  takeaways: string[],
  section3: {
    title: string,
    content: string,
  }
  why: {
    title: string,
    content: {
      title: string,
      content: string
    }[]
  }
}

const Technology = ({ title, section1, takeaways, section3, why }: Props) => {
  return (
    <>
      <Head>
        <title>{parse(title)}</title>
        <meta key="ogimage" property="og:image" content="https://softwarelogic.co/og/technology.png" />
      </Head>
      <ParallaxBanner
        layers={[{ image: "/hero.webp", speed: -15 }]}
        className={styles.hero}
      >
        <header className="container">
          <small>
            Technology
          </small>
          <h1 className={styles.title}>
            {parse(title)}
          </h1>
        </header>
      </ParallaxBanner>
      <Container className={styles.container1}>
        <Row>
          <Col lg={4}>
            <h2 className={styles.title}>
              {section1.title}
            </h2>
            <Image
              alt="about"
              src={icon}
            />
          </Col>
          <Col lg={4}>
            {section1.column1}
          </Col>
          <Col lg={4}>
            {section1.column2}
          </Col>
        </Row>
      </Container>

      <Container className={styles.container2}>
        <h2>
          Key Takeaways
        </h2>
        <Row>
          {takeaways.map((v, i) => (
            <Col key={i} lg={4}>
              <div className={styles.card}>
                <strong>
                  {String(i + 1).padStart(2)}
                </strong>
                {v}
              </div>
            </Col>
          ))}
        </Row>
      </Container>

      <Container className={styles.container3}>
        <Row>
          <Col lg={4}>
            <h2>
              {section3.title}
            </h2>
            <p>
              {section3.content}
            </p>
          </Col>
          <Col lg={{ size: 5, offset: 3 }}>
            <Image
              alt="placeholder"
              className="img-fluid"
              src={placeholder}
            />
          </Col>
        </Row>
      </Container>

      <div className={styles.container4}>
        <Container>
          <h2>
            {why.title}
          </h2>
          <Row>
            {why.content.map((item, i) => (
              <Col key={i} lg={4}>
                <Image
                  alt="gear"
                  src={gear}
                />
                <h3>
                  {item.title}
                </h3>
                <p>
                  {item.content}
                </p>
              </Col>
            ))}
          </Row>
        </Container>
      </div>

      <Technologies />

      <script type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: JSON.stringify({
            "@context": "https://schema.org",
            "@type": "WebPage",
            "name": title,
            "description": section1.column1,
          })
        }}
      />
    </>
  );
};

export default Technology;
