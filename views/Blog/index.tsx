import React, { useCallback, useEffect, useState } from "react";
import styles from "./styles.module.scss";
import { Col, Container, Row } from "reactstrap";
import { textToPath } from "utils/helpers/main";
import { ParallaxBanner } from "react-scroll-parallax";
import Image from "next/image";
import Link from "next/link";
import arrow from "./arrow-up-right.svg";
import { WP_REST_API_Post, WP_REST_API_Posts } from "wp-types";
import { useRouter } from "next/router";
import placeholder from "./placeholder.png";
import { Post } from "types";
import Head from "next/head";

export type Props = {
  posts: Post[];
  total: number;
  page: number;
}

const Blog = ({ posts, total, page }: Props) => {
  const { query } = useRouter();
  const pagination = !!query.id;
  const [state, setState] = useState(posts);
  const [lastPage, setLastPage] = useState(page);

  useEffect(() => {
    let observer = new IntersectionObserver(async ([entry]) => {
      if (entry.isIntersecting && state.length < total) {
        const posts = await fetch(
          `https://serwer2272706.home.pl/autoinstalator/wordpressplugins2/wp-json/wp/v2/posts?per_page=9&page=${lastPage + 1}`
        ).then(r => r.json());
        setLastPage(lastPage + 1);
        setState(prev => ([...prev, ...posts.map((post: WP_REST_API_Post) => ({
          ...post,
          image: post.content.rendered.match(/<img.*src="(\S*)"/)?.[1],
          minutesToRead: Math.round(post.content.rendered.split(' ').length / 200),
        }))]));
      }
    });
    const ref = document.querySelector('#loading');
    if (ref) {
      observer.observe(ref);
    }
    return () => {
      if (ref) {
        observer.unobserve(ref);
      }
    }
  }, [lastPage]);

  return (
    <>
      <Head>
        <meta key="ogimage" property="og:image" content="https://softwarelogic.co/og/blog.png" />
      </Head>
      <ParallaxBanner
        layers={[{ image: "/hero.webp", speed: -15 }]}
        className={styles.hero}
      >
        <header className="container">
          <h1 className={styles["title"]}>
            Blog
          </h1>
          <div className={styles["content"]}>
           Stay ahead with industry news, expert insights, and cutting-edge technologies!
          </div>
        </header>
      </ParallaxBanner>
      <div className={styles.background}>
        <Container className={styles.container}>

          <Row>
            {state.map((item, index) => (
              <Col key={index} lg={4}>
                <Link
                  href={`/blog/${item.slug}/`}
                >

                  <div className={styles.img}>
                    <Image
                      alt="placeholder"
                      className="img-fluid"
                      src={item.image as string || placeholder}
                      width={1024}
                      height={585}
                    />
                  </div>
                  <header className={styles.header}>
                    <h3 className={styles.title}
                      dangerouslySetInnerHTML={{
                        __html: item.title.rendered
                      }}
                    />
                    <Image
                      alt="arrow"
                      src={arrow}
                    />
                  </header>

                  <div className={styles.desc}
                    dangerouslySetInnerHTML={{
                      __html: item.excerpt.rendered,
                    }}
                  />
                  <div className={styles.footer}>
                    {item.minutesToRead && `${item.minutesToRead} minutes to read`}
                    <div className={styles.date}>
                      {`${item.date.slice(8, 10)}.${item.date.slice(5, 7)}.${item.date.slice(
                        0,
                        4
                      )}`}
                    </div>
                  </div>

                </Link>
              </Col>
            ))}
          </Row>

        </Container>
        {!pagination && (
          <div id="loading" className="text-center p-5">Loading...</div>
        )}
      </div>
    </>
  );
};

export default Blog;
