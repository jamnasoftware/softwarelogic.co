import { Container } from "reactstrap";
import styles from "./Companies.module.scss";
import dropui from "/public/logos/dropui.png";
import fffrree from "/public/logos/fffrree.png";
import timecamp from "/public/logos/timecamp.png";
import skinwallet from "/public/logos/skinwallet.png";
import imker from "/public/logos/imker.png";
import porownywarka from "/public/logos/porownywarka.png";
import cateromarket from "/public/logos/cateromarket.png";
import cloud from "/public/logos/cloud.png";
import idosell from "/public/logos/idosell.png";
import redsky from "/public/logos/redsky.png";
import diaval from "/public/logos/diaval.png";
import isotrade from "/public/logos/isotrade.png";
import ministerstwo from "/public/logos/ministerstwo.png";
import us from "/public/logos/us.png";
import easyprotect from "/public/logos/easyprotect.png";
import Image from "next/image";
import { Fragment } from "react";

const companies = {
  "dropui": dropui,
  "fffrree": fffrree,
  "timecamp": timecamp,
  "skinwallet": skinwallet,
  "imker": imker,
  "porownywarka": porownywarka,
  "cateromarket": cateromarket,
  "cloud": cloud,
  "idosell": idosell,
  "redsky": redsky,
  "diaval": diaval,
  "isotrade": isotrade,
  "ministerstwo": ministerstwo,
  "us": us,
  "easyprotect": easyprotect,
};
const Companies = () => {
  return (
    <Container className={styles.companies}>
      <div>
        {new Array(2).fill(true).map((v, i) => (
          <Fragment key={i}>
            {Object.entries(companies).map(([name, img]) => (
              <Image
                key={name}
                alt={name}
                src={img}
              />
            ))}
          </Fragment>
        ))}
      </div>
    </Container>
  );
};

export default Companies;
