import React, { useReducer, useCallback, useContext } from "react";
import { Col, Container, Row } from "reactstrap";
import useFetch from "../../../utils/hooks/use-fetch";
import LoadingSpinner from "../../../components/LoadingSpinner/LoadingSpinner";
import NewsCard from "./NewsCard/NewsCard";
import { AppContext } from "../../../store/app-context";
import styles from "./News.module.scss";
import { textToPath } from "utils/helpers/main";
import { WP_REST_API_Posts } from "wp-types";
import Link from "next/link";

type NewsReducerState = {
  newsData: WP_REST_API_Posts;
  newsLoading: boolean;
};

type NewsReducerAction = {
  type: "SET_NEWS_DATA" | "SET_NEWS_LOADING";
  value: any;
};

const newsReducer = (state: NewsReducerState, action: NewsReducerAction) => {
  switch (action.type) {
    case "SET_NEWS_DATA":
      return {
        ...state,
        newsData: (action.value as WP_REST_API_Posts).map(post => ({
          ...post,
          image: post.content.rendered.match(/<img.*src="(\S*)"/)?.[1],
        }))
      };
    case "SET_NEWS_LOADING":
      return { ...state, newsLoading: action.value };
    default:
      throw new Error();
  }
};

const News = () => {
  const appContext = useContext(AppContext);
  const [newsState, newsDispatch] = useReducer(newsReducer, {
    newsData: [],
    newsLoading: true,
  });
  const { newsData, newsLoading } = newsState;

  const { error } = useFetch({
    url: "https://serwer2272706.home.pl/autoinstalator/wordpressplugins2/wp-json/wp/v2/posts?per_page=2",
    onBegin: useCallback(() => newsDispatch({ type: "SET_NEWS_LOADING", value: true }), []),
    onSuccess: useCallback((data: any) => {
      newsDispatch({
        type: "SET_NEWS_DATA",
        value: data,
      });
    }, []),
    onEnd: useCallback(() => newsDispatch({ type: "SET_NEWS_LOADING", value: false }), []),
  });

  if (error) {
    return null;
  }
  return (
    <Container className={styles.container}>
      <Row>
        <Col lg={4}>
          <h2 ref={appContext.newsTitleRef}>
            Blog
          </h2>
          <div className={styles.description}>
            Real success. Real impact. See our innovations in action!
          </div>
          <Link href="/blog" className={styles.readAll}>
            View all posts
          </Link>
        </Col>
        <Col lg={8}>
          {newsLoading ? (
            <LoadingSpinner />
          ) : (
            <>
              {newsData.length > 0 ? (
                newsData.map((news: any, index: number) => (
                  <NewsCard
                    key={index}
                    index={index}
                    title={news.title?.rendered}
                    content={news.excerpt?.rendered}
                    date={news.date}
                    slug={news.slug}
                    image={news.image}
                  />
                ))
              ) : (
                <p>Loading error.</p>
              )}
            </>
          )}

        </Col>
      </Row>

      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: JSON.stringify({
            "@context": "https://schema.org/",
            "@type": "Blog",
            "@id": "https://softwarelogic.com/blog/",
            mainEntityOfPage: "https://softwarelogic.com/blog/",
            name: "Softwarelogic Blog",
            blogPost: newsData.map((post) => ({
              "@type": "BlogPosting",
              "@id": `/blog/${textToPath(post.title.rendered)}`,
              mainEntityOfPage: `/blog/${textToPath(post.title.rendered)}`,
              headline: post.title.rendered,
              name: post.title.rendered,
              description: post.excerpt.rendered,
              datePublished: new Date(post.date).toISOString(),
              dateModified: new Date(post.modified).toISOString(),
              url: `/blog/${textToPath(post.title.rendered)}/`,
              image: "https://softwarelogic.co/webp/main-hero.webp",
              author: {
                name: "admin"
              }
            })),
          }),
        }}
      />
    </Container>
  );
};

export default News;
