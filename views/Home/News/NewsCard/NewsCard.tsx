import React from "react";
import styles from "./NewsCard.module.scss";
import Link from "next/link";
import { Col, Row } from "reactstrap";
import placeholder1 from "./placeholder1.png";
import Image from "next/image";
import arrow from "./arrow-up-right.svg";

interface INewsCardProps {
  index: number;
  title: string;
  content: string;
  date: string;
  slug: string;
  minutesToRead?: number;
  image?: string;
}

const NewsCard = ({ index, image, minutesToRead, ...props }: INewsCardProps) => {
  const date = props.date
    ? `${props.date.slice(8, 10)}.${props.date.slice(5, 7)}.${props.date.slice(
      0,
      4
    )}`
    : null;

  return (
    <Link
      href={`/blog/${props.slug}/`}
    >
      <Row className={styles.container} style={{ marginBottom: index % 2 ? 0 : 48 }}>
        <Col lg={6}>
          <Image
            alt="blog post"
            className="img-fluid mb-2"
            height={585}
            src={image || placeholder1}
            width={1024}
          />
        </Col>
        <Col lg={6}>
          <h2
            className={styles.title}
            dangerouslySetInnerHTML={{
              __html: props.title
            }}
          />

          <div
            className={styles["content"]}
            dangerouslySetInnerHTML={{
              __html: props.content,
            }}
          />

          <footer className={styles.footer}>
            {date !== null && (
              <p className={styles["date"]}>
                {date}
                {minutesToRead && `, ${minutesToRead} minutes to read`}
              </p>
            )}
            <Image
              alt="arrow"
              src={arrow}
            />
          </footer>
        </Col>
      </Row>
    </Link>
  );
};

export default NewsCard;
