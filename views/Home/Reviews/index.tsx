import Image from "next/image";
import iconUser from "./iconUser.svg";
import { useContext, useEffect, useState } from "react";
import leftArrow from "./arrows/left.svg";
import rightArrow from "./arrows/right.svg";
import styles from "./styles.module.scss";
import data from "data/reviews";
import { Container } from "reactstrap";
import { AppContext } from "store/app-context";

const Reviews = () => {
  const [page, setPage] = useState(0);
  const [width, setWidth] = useState<number>(0);
  const appContext = useContext(AppContext);

  useEffect(() => {
    setWidth(window.innerWidth);
  }, []);

  const params = {
    track: width > 700 ?
      (data.length * 0.5 * width) :
      (data.length * width),
    translate: width > 700 ?
      Math.max(-((data.length - 2) * 0.5 * width), Math.min(0, 0.25 * width - page * 0.5 * width)) :
      Math.max(-((data.length - 1) * width), Math.min(0, -page * width)),
    item: width > 700 ? 0.5 * width : width,
  };

  // Obliczamy ile slajdów jest widocznych na ekranie
  const slidesPerView = width > 700 ? 2 : 1;

  // Obliczamy maksymalny możliwy indeks strony
  const maxPage = Math.max(0, data.length - slidesPerView);

  const handlePrev = () => {
    if (page > 0) {
      setPage(page - 1);
    }
  };

  const handleNext = () => {
    if (page < maxPage) {
      setPage(page + 1);
    }
  };

  return (
    <div className={styles.reviews}>
      <Container>
        <h2 ref={appContext.testimonialsTitleRef} className={styles.title}>
          Testimonials
        </h2>
        <div
          className={styles.track}
          style={{
            width: `${params.track}px`,
            transform: `translateX(${params.translate}px)`,
          }}
        >
          {data.map((review, index) => (
            <div
              key={index}
              className={styles.review}
              style={{ width: `${params.item}px` }}
            >
              <p>{review.content}</p>
              <p className={styles.recommendation}>{review.recommendation}</p>
              <footer>
                {/* #TODO: Add default image */}
                <Image
                  alt="author"
                  src={
                    review.author.img === "default"
                      ? iconUser
                      : review.author.img
                  }
                />
                <div>
                  <div className={styles.author}>{review.author.name}</div>
                  <div className={styles.company}>{review.author.company}</div>
                </div>
              </footer>
            </div>
          ))}
        </div>
        <div className={styles.arrows}>
          <button
            onClick={handlePrev}
            className={styles.arrow}
            disabled={page === 0}
          >
            <Image
              alt="left arrow"
              src={leftArrow}
              className={page === 0 ? styles.disabled : styles.active}
            />
          </button>
          <button
            onClick={handleNext}
            className={styles.arrow}
            disabled={page >= maxPage}
          >
            <Image
              alt="right arrow"
              src={rightArrow}
              className={page >= maxPage ? styles.disabled : styles.active}
            />
          </button>
        </div>
      </Container>
    </div>
  );
};

export default Reviews;
