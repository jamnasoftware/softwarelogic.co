import React from "react";
import styles from "./OfferCard.module.scss";
import { textToPath } from "../../../../utils/helpers/main";
import Image from "next/image";
import Link from "next/link";
import arrow from "./icon.svg";

interface ICardProps {
  imgSrc: string;
  title: string;
  children: React.ReactNode;
}

const OfferCard = (props: ICardProps) => {
  return (
    <Link href={`/services/${textToPath(props.title)}/`}>
      <div className={styles.card}>
        <div className={styles.icon}>
          <Image
            alt="icon"
            src={props.imgSrc}
            width={30}
            height={30}
          />
        </div>
        <h3 className={styles.title}>{props.title}</h3>
        <p>
          {props.children}
        </p>
        <Image
          className={styles.readMore}
          alt="arrow right"
          src={arrow}
        />
      </div>
    </Link>
  );
};

export default OfferCard;
