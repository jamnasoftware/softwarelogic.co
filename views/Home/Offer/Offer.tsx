import React, { useContext, useEffect, useState } from "react";
import { Container } from "reactstrap";
import Image from "next/image";
import OfferCard from "./OfferCard/OfferCard";
import styles from "./Offer.module.scss";
import { AppContext } from "../../../store/app-context";
import services from "data/services";
import leftArrow from "./left.svg";
import rightArrow from "./right.svg";

const Offer = () => {
  const appContext = useContext(AppContext);
  const [page, setPage] = useState(0);
  const [width, setWidth] = useState<number>(0);

  useEffect(() => {
    const handleResize = () => setWidth(window.innerWidth);
    handleResize(); // Ustaw początkową szerokość
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  const slidesPerView = width > 700 ? 2 : 1;
  const maxPage = Math.ceil(services.length / slidesPerView) - 2; // Oblicz maksymalną stronę

  const params = {
    track: services.length * (width / slidesPerView), // Ustal szerokość tracka
    translate: -page * (width / slidesPerView), // Ustal przesunięcie
    item: width / slidesPerView, // Ustal szerokość pojedynczego elementu
  };

  const handlePrev = () => {
    if (page > 0) {
      setPage(page - 1);
    }
  };

  const handleNext = () => {
    if (page < maxPage) {
      setPage(page + 1);
    }
  };

  return (
    <div className={styles.offerSection}>
      <Container>
        <h2 ref={appContext.offerTitleRef}>
          Discover how we can<br />
          <span>power your growth!</span>
        </h2>
        <div
          className={styles.track}
          style={{
            width: `${params.track}px`,
            transform: `translateX(${params.translate}px)`,
            display: 'flex', // Upewnij się, że track jest elastyczny
          }}
        >
          {services.map((offer, index) => (
            <div
              key={index}
              className={styles.offerCard}
              style={{ width: `${params.item}px` }}
            >
              <OfferCard title={offer.title} imgSrc={offer.imgSrc}>
                {offer.content}
              </OfferCard>
            </div>
          ))}
        </div>
        <div className={styles.arrows}>
          <button
            onClick={handlePrev}
            className={styles.arrow}
            disabled={page === 0}
          >
            <Image
              src={leftArrow}
              alt="Previous"
              className={page === 0 ? styles.disabled : styles.active}
            />
          </button>
          <button
            onClick={handleNext}
            className={styles.arrow}
            disabled={page >= maxPage}
          >
            <Image
              src={rightArrow}
              alt="Next"
              className={page >= maxPage ? styles.disabled : styles.active}
            />
          </button>
        </div>
      </Container>
    </div>
  );
};

export default Offer;
