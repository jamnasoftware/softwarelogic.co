import Image from "next/image";
import aboutusImage from "./aboutusImage.png";
import styles from "./styles.module.scss";
import { Container } from "reactstrap";

const AboutUs = () => (
    <Container className={styles.container}>
        <div className={styles.content}>
            <h2>Who <span className={styles.colored}>we are</span></h2>
            <p>
                SoftwareLogic.co is a <strong>leading software development company</strong> with a team of <strong>15+ expert engineers</strong>, delivering <strong>custom web</strong>, <strong>mobile</strong>, and <strong>desktop applications</strong> for <strong>Windows</strong> & <strong>Linux</strong>.
            </p>
            <p>
                We specialize in <strong>Kubernetes (K8s) development</strong>, <strong>AI-powered solutions</strong>, and <strong>outsourcing services</strong>, helping startups and enterprises scale fast with <strong>high-performance</strong>, <strong>secure</strong>, and <strong>scalable software</strong>.
            </p>
            <a href="mailto:office@softwarelogic.co?subject=Estimate%20Project" className={styles.estimate}>
                Get an estimate →
            </a>
        </div>
        <div className={styles.imageWrapper}>
            <div className={styles.background} />
            <Image
                className={styles.image}
                alt="about us"
                src={aboutusImage}
            />
        </div>
    </Container>
);

export default AboutUs;
