import React, { useContext } from "react";
import { Col, Container, Row } from "reactstrap";
import styles from "./style.module.scss";
import { useState } from "react";
import { AppContext } from "../../../store/app-context";
import { textToPath } from "../../../utils/helpers/main";
import Image from "next/image";
import realizationsData from "../../../data/casestudies";
import Link from "next/link";
import Pagination from "components/Pagination";
import arrow from "./arrow-up-right.svg";

const Realizations = () => {
  const appContext = useContext(AppContext);
  const [page, setPage] = useState(0);
  const perPage = 3;
  const pages = Math.ceil(realizationsData.length / perPage);
  return (
    <div className={styles.container}>
      <Container>
        <Row className={styles.header}>
          <Col xs={12} lg="auto">
            <h2
              ref={appContext.realizationsTitleRef}
              className={styles.title}
            >
              Case Studies
            </h2>
            <p>
              Real success. Real impact. See our innovations in action!
            </p>
          </Col>
          <Col lg="auto">
            <Link
              href="/case-studies"
            >
              View all case studies
            </Link>
          </Col>
        </Row>
        <Row>
          {realizationsData.slice(page * perPage, page * perPage + perPage).map((item, index) => (
            <Col key={index} lg={4} className={styles.column}>

              <Link
                href={`/case-studies/${textToPath(item.title)}/`}
              >
                <div className={styles.img}>
                  <Image
                    alt={item.imgAlt}
                    className="img-fluid"
                    src={item.imgSrc}
                  />
                </div>
              </Link>
              <h3 className={styles.title}>{item.title}</h3>

              <p className={styles.desc}>
                {item.desc}
              </p>
              <Link
                className={styles.readMore}
                href={`/case-studies/${textToPath(item.title)}/`}
              >
                Check case study
                <Image
                  alt="arrow"
                  src={arrow}
                />
              </Link>
            </Col>
          ))}
        </Row>
        <Pagination
          active="#32EB51"
          hidePages
          page={page}
          pages={pages}
          setPage={setPage}
        />
      </Container>
    </div>
  );
};

export default Realizations;
