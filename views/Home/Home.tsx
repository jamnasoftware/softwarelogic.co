import React, { useContext } from "react";
import Companies from "./Companies/Companies";
import styles from "./Home.module.scss";
import Offer from "./Offer/Offer";
import Technology from "./Technology/Technology";
import CaseStudies from "./CaseStudies";
import News from "./News/News";
import { Container } from "reactstrap";
import AboutUs from "./AboutUs";
import Reviews from "./Reviews";
import Stats from "./Stats";
import { AppContext } from "store/app-context";
import Image from "next/image";
import arrow from "public/icon/arrow-down.svg";

const Home = () => {
  const appContext = useContext(AppContext);
  return (
    <>
      <div
        className={styles.hero}
      >
        <Container>
          <div
            className={styles["hero-text"]}
          >
          Build. Scale. Dominate.
          </div>
          <div
            className={styles["hero-subtext"]}
          >
            AI-powered software for startups & enterprises. We craft complex business apps, fuel growth with AI, and turn ideas into market-leading products. Let’s build the future—together!
          </div>
          <div>
            <a className={styles["contact-us"]} href="mailto:office@softwarelogic.co?subject=Estimate%20Project">
              📝 Get an Estimate
            </a>
            <button className={styles["learn-more"]} onClick={() => appContext.scrollToSection("CASE STUDIES")}>
              Case studies
            </button>
          </div>
          <button className={styles.explore} onClick={() => appContext.scrollToSection("SERVICES")} >
            Explore
            <Image
              alt="arrow down"
              src={arrow}
            />
          </button>
        </Container>
      </div>
      <Companies />
      <Offer />
      <Stats />
      <AboutUs />
      <Reviews />
      <Technology />
      <CaseStudies />
      <News />
    </>
  );
};

export default Home;
