import React from "react";
import styles from "./Technology.module.scss";
import { Col, Container, Row } from "reactstrap";
import Link from "next/link";
import technologies from "data/technologies";

const list = Object.entries(technologies).map(([key, value]) => ({
  title: value.title,
  url: `/technology/${key}`,
}));

const Technology = () => {
  return (
    <Container className={styles.container}>
      <h2>Technology that powers your growth</h2>
      <p className="pb-3">Expertise in:</p>
      <Row>
        {list.map((t) => (
          <Col xs={{ size: 6 }} md={{ size: 3 }} className={styles["column"]} key={t.title}>
            <Link
              href={t.url}
            >
              {t.title}
            </Link>
          </Col>
        ))}
      </Row>
    </Container>
  );
};

export default Technology;
