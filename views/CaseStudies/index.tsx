import React, { useCallback, useState } from "react";
import styles from "./styles.module.scss";
import Head from "next/head";
import { Col, Container, Row } from "reactstrap";
import { textToPath } from "utils/helpers/main";
import casestudies from "data/casestudies";
import { ParallaxBanner } from "react-scroll-parallax";
import Image from "next/image";
import Link from "next/link";

import arrow from "./arrow-up-right.svg";

const CaseStudies = () => {
  return (
    <>
      <Head>
        <title>Case studies</title>
        <meta key="ogimage" property="og:image" content="https://softwarelogic.co/og/casestudies.png" />
      </Head>
      <ParallaxBanner
        layers={[{ image: "/hero.webp", speed: -15 }]}
        className={styles.hero}
      >
        <header className="container">
          <h1 className={styles["title"]}>
            Case studies
          </h1>
          <div className={styles["content"]}>
            Discover the inspiring stories of our clients projects.
          </div>
        </header>
      </ParallaxBanner>
      <div className={styles.background}>
        <Container className={styles.container}>

          <Row>
            {casestudies.map((item, index) => (
              <Col key={index} lg={4}>

                <Link
                  href={`/case-studies/${textToPath(item.title)}/`}
                >
                  <div className={styles.img}>
                    <Image
                      alt={item.imgAlt}
                      className="img-fluid"
                      src={item.imgSrc}
                    />
                  </div>
                </Link>

                <h3 className={styles.title}>{item.title}</h3>

                <p className={styles.desc}>
                  {item.desc}
                </p>
                <Link
                  className={styles.readMore}
                  href={`/case-studies/${textToPath(item.title)}/`}
                >
                  Read more
                  <Image
                    alt="arrow"
                    src={arrow}
                  />
                </Link>
              </Col>
            ))}
          </Row>

        </Container>
      </div>
    </>
  );
};

export default CaseStudies;
