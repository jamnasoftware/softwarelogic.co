import { useCallback, useEffect, useState } from "react";

interface IUseRequestConfig {
  url: string;
  method?: string;
  headers?: HeadersInit;
  credentials?: RequestCredentials;
  body?: any;
  onlyAuth?: boolean;
  sendCondition?: boolean;
  loadingBar?: boolean;
  onBegin?: () => void;
  onSuccess?: <T extends any>(data: T) => void;
  onEnd?: () => void;
  errorToastMessageText?: string;
}

const useFetch = (config: IUseRequestConfig) => {
  const [sendingState, setSendingState] = useState<
    "START" | "MIDDLE" | "END" | null
  >(null);
  const [error, setError] = useState<boolean>(false);

  const {
    url,
    method,
    headers,
    credentials,
    body,
    onlyAuth,
    sendCondition,
    loadingBar,
    onBegin,
    onSuccess,
    onEnd,
    errorToastMessageText,
  } = config;

  const [fetchRequest, setFetchRequest] = useState<boolean>(false);
  const fetchRequestHandler = useCallback(
    () => setFetchRequest((prev) => !prev),
    []
  );

  useEffect(() => {
    let isMounted = true;

    const sendRequest = async () => {
      setError(false);

      if (sendCondition === false) {
        onEnd && onEnd();
        setSendingState("END");
        return;
      }

      onBegin && onBegin();
      setSendingState("START");

      try {
        const response = await fetch(url, {
          method,
          headers,
          credentials,
          body: JSON.stringify(body),
        });

        if (!isMounted) {
          onEnd && onEnd();
          setSendingState("END");
          return;
        }

        setSendingState("MIDDLE");

        if (response.status === 200) {
          if (isMounted) {
            let data = await response.json();
            onSuccess && onSuccess(data);
          }
        } else {
          if (onlyAuth && errorToastMessageText) {
            if (isMounted) {
              setError(true);
            }
          }
        }
      } catch (e) {
        setError(true);
      }

      if (isMounted) {
        onEnd && onEnd();
        setSendingState("END");
      }
    };

    sendRequest();

    return () => {
      isMounted = false;
    };
  }, [
    body,
    credentials,
    errorToastMessageText,
    headers,
    loadingBar,
    method,
    onBegin,
    onEnd,
    onSuccess,
    onlyAuth,
    sendCondition,
    url,
    fetchRequest,
  ]);

  return {
    sendingState,
    error,
    fetchRequest: fetchRequestHandler,
  };
};

export default useFetch;
