
export const scrollToTargetAdjusted = (element: HTMLElement, offset: number) => {
  window.scrollTo({
        top: element.getBoundingClientRect().top + window.pageYOffset - offset,
        behavior: "smooth",
  });
};

export const textToPath = (text: string) => {
  return text
  .toLocaleLowerCase()
  .normalize('NFD')
  .replace(/[\u0300-\u036f]/g, "")
  .replace(/[^a-zA-Z ]/g, "")
  .split(" ")
  .join("-");
};