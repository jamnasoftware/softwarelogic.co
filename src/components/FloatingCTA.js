import Link from 'next/link';
import FontAwesomeIcon from "../components/common/FontAwesomeIcon";
import { useState, useEffect } from 'react';
import PrimaryButton from "@/components/common/PrimaryButton";

export default function FloatingCTA() {
  // Inicjalizuj stan jako false
  const [showAnimation, setShowAnimation] = useState(false);
  
  useEffect(() => {
    // Aktywuj animację tylko po renderowaniu na kliencie
    setShowAnimation(true);
    
    // Usuń animację po 3 sekundach
    const timer = setTimeout(() => {
      setShowAnimation(false);
    }, 3000);
    
    return () => clearTimeout(timer);
  }, []);

  return (
    <div className="fixed bottom-6 right-6 z-40">
      <PrimaryButton
          animateIn
          href="#kontakt" >
        <span className="mr-2">Bezpłatna konsultacja</span>
        <FontAwesomeIcon icon="arrow-right" className="h-5 w-5"/>
      </PrimaryButton>
    </div>
  );
}