import React, { useState, useEffect, useRef } from "react";
import Link from "next/link";
import Image from "next/image";
import { useRouter } from "next/router";
import FontAwesomeIcon from "../common/FontAwesomeIcon";
import PrimaryButton from "@/components/common/PrimaryButton";
import {
  faLaptopCode,
  faDesktop,
  faMobileAlt,
  faNetworkWired,
  faCloud,
  faBolt,
  faLayerGroup,
  faAngleRight,
  faBars,
  faTimes
} from '@fortawesome/free-solid-svg-icons';

/**
 * Główne menu nawigacyjne
 * 
 * @returns {JSX.Element} Komponent nagłówka
 */
export default function Header() {
  const [isScrolled, setIsScrolled] = useState(false);
  const [isMobileMenuOpen, setIsMobileMenuOpen] = useState(false);
  const [isOtherMenuOpen, setIsOtherMenuOpen] = useState(false);
  const [mounted, setMounted] = useState(false);
  const router = useRouter();
  const dropdownRef = useRef(null);

  // Efekt dla stanu przewijania
  useEffect(() => {
    setMounted(true);
    
    const handleScroll = () => {
      setIsScrolled(window.scrollY > 0);
    };

    // Początkowy stan
    setIsScrolled(window.scrollY > 0);
    
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  }, []);

  // Efekt dla kliknięcia poza menu
  useEffect(() => {
    const handleClickOutside = (event) => {
      if (dropdownRef.current && !dropdownRef.current.contains(event.target)) {
        setIsOtherMenuOpen(false);
      }
    };

    document.addEventListener("mousedown", handleClickOutside);
    return () => document.removeEventListener("mousedown", handleClickOutside);
  }, []);

  // Główne pozycje menu
  const mainMenuItems = [
    {
      name: "Aplikacje webowe",
      href: "/oferta/aplikacje-webowe",
      icon: faLaptopCode,
      description: "Nowoczesne rozwiązania webowe z czystym, responsywnym designem."
    },
    {
      name: "Aplikacje desktop",
      href: "/oferta/aplikacje-desktop",
      icon: faDesktop,
      description: "Precyzyjnie zaprojektowane aplikacje działające spójnie na różnych platformach."
    },
    {
      name: "Mikroserwisy i integratory",
      href: "/oferta/mikroserwisy-i-integratory",
      icon: faNetworkWired,
      description: "Elastyczne architektury mikroserwisów usprawniające komunikację systemów IT."
    },
    {
      name: "DevOps i Cloud",
      href: "/oferta/devops-i-cloud",
      icon: faCloud,
      description: "Zaawansowane rozwiązania optymalizujące procesy wytwarzania oprogramowania."
    }
  ];

  // Pozostałe pozycje menu
  const otherItems = [
    {
      name: "Aplikacje mobilne",
      href: "/oferta/aplikacje-mobilne",
      icon: faMobileAlt,
      description: "Innowacyjne aplikacje, które przekształcają pomysły w intuicyjne narzędzia."
    },
    {
      name: "Aplikacje No-Code/Low-Code",
      href: "/oferta/aplikacje-nocode-lowcode",
      icon: faBolt,
      description: "Szybkie narzędzia do modelowania prostych rozwiązań technologicznych."
    }
  ];

  /**
   * Sprawdza, czy element menu jest aktywny
   * 
   * @param {string} itemHref - URL elementu menu
   * @returns {boolean} Czy element jest aktywny
   */
  const isActive = (itemHref) => {
    if (!itemHref) return false;
    return router.pathname === itemHref || router.asPath === itemHref;
  };

  /**
   * Sprawdza, czy którykolwiek z elementów "Pozostałe" jest aktywny
   * 
   * @returns {boolean} Czy element jest aktywny
   */
  const isAnyOtherActive = otherItems.some(item => isActive(item.href));

  /**
   * Obsługuje nawigację z obsługą błędów
   * 
   * @param {React.MouseEvent} e - Wydarzenie kliknięcia
   * @param {string} href - Docelowy URL
   */
  const handleNavigation = (e, href) => {
    e.preventDefault();
    if (!mounted) return;

    try {
      router.push(href);
    } catch (error) {
      console.error("Navigation error:", error);
      window.location.href = href; // fallback
    }
  };

  return (
    <>
      {/* Nagłówek na pełną szerokość */}
      <header
        className={`fixed w-full z-50 transition-all duration-300 ${
          isScrolled
            ? "bg-white shadow-md border-b border-gray-200"
            : "bg-white border-b border-gray-200"
        }`}
      >
        <div className="w-full mx-auto px-4 sm:px-6 lg:px-8">
          <div className="flex justify-between items-center h-20">
            {/* Logo */}
            <div className="flex items-center">
              <Link
                href="/"
                className="shrink-0 transition-transform duration-300 hover:scale-105"
              >
                <Image
                  src="/software-logic-logo_purple.svg"
                  alt="SoftwareLogic"
                  width={160}
                  height={80}
                  className="h-20 w-auto py-4"
                />
              </Link>

              {/* Menu główne - widoczne na większych ekranach */}
              <nav className="hidden md:flex md:ml-6">
                <div className="flex border-l border-gray-200 mx-auto">
                  {/* Główne elementy menu */}
                  {mounted &&
                    mainMenuItems.map((item, index) => (
                      <MenuItemDesktop 
                        key={index} 
                        item={item} 
                        isActive={isActive(item.href)} 
                      />
                    ))}

                  {/* Przycisk "Pozostałe" z menu rozwijane */}
                  <div
                    className="relative border-r border-gray-200"
                    ref={dropdownRef}
                  >
                    <button
                      onClick={() => setIsOtherMenuOpen(!isOtherMenuOpen)}
                      className={`relative group flex items-center font-medium py-3 px-5 transition-all duration-300 border-b-2 ${
                        isAnyOtherActive
                          ? "text-purple-700 border-purple-700 bg-purple-50/30"
                          : "text-gray-700 hover:text-purple-700 hover:bg-purple-50/30 border-transparent hover:border-purple-700"
                      }`}
                      aria-expanded={isOtherMenuOpen}
                      aria-haspopup="true"
                    >
                      <span
                        className={`mr-2 text-purple-600 transform ${
                          isAnyOtherActive
                            ? "scale-100"
                            : "scale-0 group-hover:scale-100"
                        } transition-transform duration-300 origin-left`}
                      >
                        <FontAwesomeIcon
                          icon={faLayerGroup}
                          className="text-sm"
                        />
                      </span>
                      <span className="relative">
                        Pozostałe
                        <span
                          className={`absolute bottom-0 left-0 w-full h-0 bg-gradient-to-r from-purple-500/0 via-purple-600/50 to-purple-500/0 ${
                            isAnyOtherActive ? "h-0.5" : "group-hover:h-0.5"
                          } transition-all duration-300 -mb-0.5`}
                        ></span>
                      </span>
                      <svg
                        className={`ml-1 h-5 w-5 transition-transform duration-300 ${
                          isOtherMenuOpen || isAnyOtherActive
                            ? "rotate-180 text-purple-700"
                            : ""
                        }`}
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 20 20"
                        fill="currentColor"
                      >
                        <path
                          fillRule="evenodd"
                          d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                          clipRule="evenodd"
                        />
                      </svg>
                    </button>

                    {/* Menu rozwijane "Pozostałe" */}
                    {isOtherMenuOpen && (
                      <div className="absolute left-0 mt-2 w-[400px] bg-white shadow-xl z-50 overflow-hidden border-0 border-t-2 border-purple-600 animate-fadeIn rounded-lg">
                        <div className="p-5">
                          <h3 className="text-lg font-bold text-gray-800 mb-4 border-b border-gray-200 pb-2 flex items-center">
                            <span className="bg-gradient-to-r from-purple-600 to-purple-800 h-5 w-1 mr-2 rounded"></span>
                            Pozostałe usługi
                          </h3>
                          <div className="grid grid-cols-1 gap-4">
                            {otherItems.map((item, index) => (
                              <OtherMenuItem 
                                key={index} 
                                item={item} 
                                isActive={isActive(item.href)}
                                onClick={() => setIsOtherMenuOpen(false)}
                              />
                            ))}
                          </div>
                          
                          {/* CTA w menu rozwijane */}
                          <div className="mt-6 pt-4 border-t border-gray-200">
                            <div className="bg-gradient-to-br from-purple-50 to-indigo-50 p-4 rounded-lg shadow-sm">
                              <p className="text-gray-900 font-medium mb-2">
                                Potrzebujesz niestandardowego rozwiązania?
                              </p>
                              <PrimaryButton
                                href="mailto:office@softwarelogic.co"
                                size="sm"
                              >
                                Bezpłatna konsultacja
                              </PrimaryButton>
                            </div>
                          </div>
                        </div>
                      </div>
                    )}
                  </div>
                </div>
              </nav>
            </div>

            {/* Przyciski po prawej stronie */}
            <div className="flex items-center space-x-4">
              {/* CTA - całkowicie ukryty na ekranach mniejszych niż lg */}
              <div className="hidden lg:block">
                <PrimaryButton
                  href="#kontakt"
                  icon={<svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-5 w-5 transform group-hover:translate-x-1 transition-transform duration-300"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fillRule="evenodd"
                      d="M10.293 5.293a1 1 0 011.414 0l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414-1.414L12.586 11H5a1 1 0 110-2h7.586l-2.293-2.293a1 1 0 010-1.414z"
                      clipRule="evenodd"
                    />
                  </svg>}
                >
                  Bezpłatna konsultacja
                </PrimaryButton>
              </div>
              
              {/* Przycisk menu mobilnego */}
              <button
                onClick={() => setIsMobileMenuOpen(true)}
                type="button"
                className="md:hidden p-2 rounded-md text-gray-700 hover:text-purple-700 transition-colors"
                aria-label="Otwórz menu mobilne"
              >
                <FontAwesomeIcon icon={faBars} className="h-6 w-6" />
              </button>
            </div>
          </div>
        </div>
      </header>

      {/* Menu mobilne */}
      {isMobileMenuOpen && (
        <div className="md:hidden fixed inset-0 z-50 overflow-auto bg-white animate-fadeIn">
          <div className="pt-20 pb-6 px-4">
            {/* Przycisk zamknięcia */}
            <button
              onClick={() => setIsMobileMenuOpen(false)}
              type="button"
              className="absolute top-4 right-4 p-2 rounded-full text-gray-700 hover:bg-gray-100 transition-colors"
              aria-label="Zamknij menu"
            >
              <FontAwesomeIcon icon={faTimes} className="h-6 w-6" />
            </button>

            {/* CTA na górze menu mobilnego */}
            <div className="mb-6">
              <PrimaryButton
                href="#kontakt"
                onClick={() => setIsMobileMenuOpen(false)}
                className="w-full justify-center"
                icon={<svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-5 w-5"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                >
                  <path
                    fillRule="evenodd"
                    d="M10.293 5.293a1 1 0 011.414 0l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414-1.414L12.586 11H5a1 1 0 110-2h7.586l-2.293-2.293a1 1 0 010-1.414z"
                    clipRule="evenodd"
                  />
                </svg>}
                iconRight={false}
              >
                Bezpłatna konsultacja
              </PrimaryButton>
            </div>

            <nav className="flex flex-col space-y-6">
              {/* Główne menu mobilne */}
              <div className="space-y-4">
                <h3 className="text-lg font-bold text-gray-900 pb-2 border-b border-gray-100 flex items-center">
                  <span className="bg-gradient-to-r from-purple-600 to-purple-800 h-4 w-1 rounded mr-2"></span>
                  Główne usługi
                </h3>
                <div className="space-y-3">
                  {mainMenuItems.map((item, index) => (
                    <MenuItemMobile 
                      key={index} 
                      item={item} 
                      isActive={isActive(item.href)}
                      onClick={() => setIsMobileMenuOpen(false)}
                    />
                  ))}
                </div>
              </div>

              {/* Menu "Pozostałe" mobilne */}
              <div className="space-y-4">
                <h3 className="text-lg font-bold text-gray-900 pb-2 border-b border-gray-100 flex items-center">
                  <span className="bg-gradient-to-r from-purple-600 to-purple-800 h-4 w-1 rounded mr-2"></span>
                  Pozostałe usługi
                </h3>
                <div className="space-y-2">
                  {otherItems.map((item, index) => (
                    <MenuItemMobile 
                      key={index} 
                      item={item} 
                      isActive={isActive(item.href)}
                      onClick={() => setIsMobileMenuOpen(false)}
                    />
                  ))}
                </div>
              </div>
            </nav>
          </div>
        </div>
      )}

      {/* Animacja fadeIn */}
      <style jsx global>{`
        @keyframes fadeIn {
          from {
            opacity: 0;
            transform: translateY(-10px);
          }
          to {
            opacity: 1;
            transform: translateY(0);
          }
        }
        .animate-fadeIn {
          animation: fadeIn 0.3s ease-out forwards;
        }
      `}</style>
    </>
  );
}

/**
 * Element menu desktopowego
 * 
 * @param {Object} props
 * @param {Object} props.item - Dane elementu menu
 * @param {boolean} props.isActive - Czy element jest aktywny
 * @returns {JSX.Element} Element menu
 */
function MenuItemDesktop({ item, isActive }) {
  const { name, href, icon } = item;
  const isDisabled = !href;

  const baseClassName = "relative group flex items-center font-medium py-3 px-5 transition-all duration-300 border-b-2 border-r border-gray-200";
  const className = isDisabled
    ? `${baseClassName} text-gray-500 cursor-not-allowed opacity-60 border-transparent`
    : isActive
    ? `${baseClassName} text-purple-700 border-b-purple-700 bg-purple-50/30`
    : `${baseClassName} text-gray-700 hover:text-purple-700 hover:border-b-purple-700 hover:bg-purple-50/30 border-b-transparent`;

  const content = (
    <>
      <span
        className={`mr-2 text-purple-600 transform ${
          isActive ? "scale-100" : "scale-0 group-hover:scale-100"
        } transition-transform duration-300 origin-left`}
      >
        <FontAwesomeIcon icon={icon} className="text-sm" />
      </span>
      <span className="relative">
        {name}
        <span
          className={`absolute bottom-0 left-0 w-full h-0 bg-gradient-to-r from-purple-500/0 via-purple-600/50 to-purple-500/0 ${
            isActive ? "h-0.5" : "group-hover:h-0.5"
          } transition-all duration-300 -mb-0.5`}
        ></span>
      </span>
      {isDisabled && <span className="ml-1 text-xs">(Wkrótce)</span>}
    </>
  );

  return isDisabled ? (
    <span className={className} title="Ta funkcja będzie dostępna wkrótce">
      {content}
    </span>
  ) : (
    <Link href={href} className={className}>
      {content}
    </Link>
  );
}

/**
 * Element mobilnego menu
 * 
 * @param {Object} props
 * @param {Object} props.item - Dane elementu menu
 * @param {boolean} props.isActive - Czy element jest aktywny
 * @param {Function} props.onClick - Funkcja wywoływana po kliknięciu
 * @returns {JSX.Element} Element menu mobilnego
 */
function MenuItemMobile({ item, isActive, onClick }) {
  const { name, href, icon, description } = item;
  const isDisabled = !href;

  const baseClassName = "flex items-center p-4 transition-all duration-300 border-l-2 relative overflow-hidden group";
  const className = isDisabled
    ? `${baseClassName} bg-gray-100 cursor-not-allowed opacity-60 border-gray-300`
    : isActive
    ? `${baseClassName} bg-purple-50/30 border-purple-600`
    : `${baseClassName} bg-white hover:bg-purple-50/30 border-transparent hover:border-purple-600`;

  const content = (
    <>
      {/* Efekt tła */}
      <div className="absolute inset-0 bg-gradient-to-r from-purple-500/5 to-purple-700/5 opacity-0 group-hover:opacity-100 transition-opacity duration-300"></div>

      {/* Ikona */}
      <div
        className={`flex-shrink-0 w-10 h-10 rounded-full ${
          isActive ? "bg-purple-700" : "bg-purple-600"
        } flex items-center justify-center mr-3 shadow-md`}
      >
        <FontAwesomeIcon icon={icon} className="text-white text-lg" />
      </div>

      {/* Treść */}
      <div className="text-left flex-grow">
        <div className="flex items-center">
          <p className={`font-medium text-base ${isActive ? "text-purple-700" : "text-gray-800"}`}>
            {name}
            {isDisabled && (
              <span className="ml-2 text-xs font-normal">(Wkrótce)</span>
            )}
          </p>
          {!isDisabled && (
            <div
              className={`ml-auto text-purple-600 ${
                isActive ? "opacity-100" : "opacity-0 group-hover:opacity-100"
              } transition-opacity duration-300`}
            >
              <FontAwesomeIcon icon={faAngleRight} />
            </div>
          )}
        </div>
        <p className="text-sm text-gray-600 mt-1 pr-4">{description}</p>
      </div>
    </>
  );

  return isDisabled ? (
    <div
      className={className}
      title="Ta funkcja będzie dostępna wkrótce"
    >
      {content}
    </div>
  ) : (
    <Link
      href={href}
      className={className}
      onClick={onClick}
    >
      {content}
    </Link>
  );
}

/**
 * Element menu rozwijanego "Pozostałe"
 * 
 * @param {Object} props
 * @param {Object} props.item - Dane elementu menu
 * @param {boolean} props.isActive - Czy element jest aktywny
 * @param {Function} props.onClick - Funkcja wywoływana po kliknięciu
 * @returns {JSX.Element} Element menu rozwijanego
 */
function OtherMenuItem({ item, isActive, onClick }) {
  const { name, href, icon, description } = item;
  const isDisabled = !href;

  const baseClassName = "flex items-center p-4 rounded-lg transition-all duration-300";
  const className = isDisabled
    ? `${baseClassName} opacity-60 cursor-not-allowed border-gray-300 bg-gray-100`
    : isActive
    ? `${baseClassName} border-purple-600 bg-purple-50/30`
    : `${baseClassName} bg-white hover:bg-purple-50/30 hover:shadow-sm`;

  return (
    <Link
      href={href || "#"}
      className={className}
      onClick={onClick}
    >
      <div
        className={`flex-shrink-0 w-10 h-10 rounded-full ${
          isActive ? "bg-purple-700" : "bg-purple-600"
        } flex items-center justify-center mr-3 shadow-sm`}
      >
        <FontAwesomeIcon
          icon={icon}
          className="text-white text-lg"
        />
      </div>
      <div>
        <div
          className={`font-medium ${
            isActive ? "text-purple-700" : "text-gray-900"
          }`}
        >
          {name}
          {isDisabled && (
            <span className="ml-2 text-xs font-normal">
              (Wkrótce)
            </span>
          )}
        </div>
        <p className="text-sm text-gray-600 mt-1">
          {description}
        </p>
      </div>
    </Link>
  );
}