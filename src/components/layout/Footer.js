import React, { useState, useEffect } from 'react';
import Image from 'next/image';
import Link from 'next/link';
import FontAwesomeIcon from "../common/FontAwesomeIcon";
import { faLinkedin, faFacebook } from '@fortawesome/free-brands-svg-icons';

/**
 * Komponent stopki strony
 * 
 * @returns {JSX.Element} Stopka strony
 */
export default function Footer() {
  const [email, setEmail] = useState('');
  const [currentYear, setCurrentYear] = useState('2025'); // Domyślna wartość statyczna
  
  useEffect(() => {
    // Aktualizuj rok tylko po stronie klienta
    setCurrentYear(new Date().getFullYear().toString());
  }, []);
  
  /**
   * Obsługuje wysłanie formularza newslettera
   * 
   * @param {React.FormEvent} e - Wydarzenie formularza
   */
  const handleSubmit = (e) => {
    e.preventDefault();
    console.log('Newsletter submit:', email);
    alert('Dziękujemy za zapisanie się do newslettera!');
    setEmail('');
  };

  // Dane menu
  const menuItems = [
    {
      title: "Oferta",
      links: [
        { text: "Aplikacje webowe", href: "/oferta/aplikacje-webowe" },
        { text: "Aplikacje desktop", href: "/oferta/aplikacje-desktop" },
        { text: "Mikroserwisy i integracje", href: "/oferta/mikroserwisy-i-integracje" },
        { text: "DevOps i cloud", href: "/oferta/devops-i-cloud" }
      ]
    },
    {
      title: "Oferta",
      links: [
        { text: "Aplikacje mobilne", href: "/oferta/aplikacje-mobilne" },
        { text: "Aplikacje no-code i low-code", href: "/oferta/aplikacje-nocode-lowcode" }
      ]
    }
  ];
  
  return (
    <footer className="bg-gray-900 py-12 text-white">
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div className="grid grid-cols-1 md:grid-cols-3 gap-8">
          {/* Logo i opis firmy */}
          <div>
            <Link href="/" className="inline-block">
              <Image
                src="/software-logic-logo_white.svg"
                alt="SoftwareLogic"
                width={160}
                height={80}
                className="h-20 w-auto py-4"
              />
            </Link>
            <p className="text-gray-400 mb-6">
              Tworzymy dedykowane rozwiązania technologiczne, które zwiększają efektywność, obniżają koszty i budują
              przewagę konkurencyjną.
            </p>
            <div className="flex space-x-4">
              <SocialLink 
                href="https://www.linkedin.com/company/softwarelogic-co/" 
                icon={faLinkedin}
                label="LinkedIn"
              />
              <SocialLink 
                href="https://www.facebook.com/softwarelogic.co/" 
                icon={faFacebook}
                label="Facebook"
              />
            </div>
          </div>

          {/* Menu - kolumny */}
          {menuItems.map((menu, index) => (
            <div key={index} className={index === 0 ? "md:ms-auto" : ""}>
              <h3 className="font-bold text-lg mb-4">{menu.title}</h3>
              <ul className="space-y-3">
                {menu.links.map((link, linkIndex) => (
                  <li key={linkIndex}>
                    <Link 
                      href={link.href} 
                      className="text-gray-400 hover:text-white transition-colors"
                    >
                      {link.text}
                    </Link>
                  </li>
                ))}
              </ul>
            </div>
          ))}
        </div>

        {/* Copyright */}
        <div className="mt-12 pt-8 border-t border-gray-800 text-center text-gray-500">
          <p>&copy; {currentYear} SoftwareLogic. Wszelkie prawa zastrzeżone.</p>
        </div>
      </div>
    </footer>
  );
}

/**
 * Komponent linku do mediów społecznościowych
 * 
 * @param {Object} props
 * @param {string} props.href - URL linku
 * @param {import('@fortawesome/fontawesome-svg-core').IconDefinition} props.icon - Ikona FontAwesome
 * @param {string} props.label - Etykieta dostępności
 * @returns {JSX.Element} Link do mediów społecznościowych
 */
function SocialLink({ href, icon, label }) {
  return (
    <a 
      href={href} 
      className="text-gray-400 hover:text-white transition-colors"
      aria-label={label}
      target="_blank"
      rel="noopener noreferrer"
    >
      <FontAwesomeIcon icon={icon} />
    </a>
  );
}