import React from 'react';
import PrimaryButton from "@/components/common/PrimaryButton";
import FontAwesomeIcon from "@/components/common/FontAwesomeIcon";
import { 
  faArrowRight, 
  faDesktop, 
  faServer, 
  faDatabase, 
  faCloud 
} from '@fortawesome/free-solid-svg-icons';

/**
 * Komponent sekcji technologii dla podstron usług
 * 
 * @param {Object} props
 * @param {Object} props.data - Dane technologii
 * @returns {JSX.Element} Komponent technologii
 */
export default function Technologies({ data }) {
  return (
    <section id="technologie" className="py-20 bg-gray-50">
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        {/* Nagłówek sekcji */}
        <div className="text-center mb-16">
          <h2 className="text-3xl md:text-4xl font-bold mb-4">{data.title}</h2>
          <p className="text-lg text-gray-600 max-w-3xl mx-auto">
            {data.subtitle}
          </p>
        </div>
        
        <div className="grid grid-cols-1 md:grid-cols-2 gap-8 mb-16">
          {/* Frontend */}
          <div className="bg-white rounded-lg shadow-md p-8">
            <div className="flex items-center mb-6">
              <div className="w-12 h-12 rounded-full bg-purple-600/10 flex items-center justify-center text-purple-600 mr-4">
                <FontAwesomeIcon icon={faDesktop} className="text-xl" />
              </div>
              <h3 className="text-xl font-bold">Frontend</h3>
            </div>
            
            <div className="grid grid-cols-2 gap-4">
              {data.frontendTechs.map((tech, index) => (
                <TechnologyItem key={index} tech={tech} />
              ))}
            </div>
            
            <div className="mt-6 p-4 bg-purple-600/5 rounded-lg">
              <h4 className="font-semibold mb-2">Dlaczego te technologie?</h4>
              <p className="text-gray-600 text-sm">
                {data.frontendDescription}
              </p>
            </div>
          </div>
          
          {/* Backend */}
          <div className="bg-white rounded-lg shadow-md p-8">
            <div className="flex items-center mb-6">
              <div className="w-12 h-12 rounded-full bg-purple-600/10 flex items-center justify-center text-purple-600 mr-4">
                <FontAwesomeIcon icon={faServer} className="text-xl" />
              </div>
              <h3 className="text-xl font-bold">Backend</h3>
            </div>
            
            <div className="grid grid-cols-2 gap-4">
              {data.backendTechs.map((tech, index) => (
                <TechnologyItem key={index} tech={tech} />
              ))}
            </div>
            
            <div className="mt-6 p-4 bg-purple-600/5 rounded-lg">
              <h4 className="font-semibold mb-2">Dlaczego te technologie?</h4>
              <p className="text-gray-600 text-sm">
                {data.backendDescription}
              </p>
            </div>
          </div>
        </div>
        
        {/* Bazy danych i pamięć podręczna */}
        <div className="bg-white rounded-lg shadow-md p-8 mb-16">
          <div className="flex items-center mb-6">
            <div className="w-12 h-12 rounded-full bg-purple-600/10 flex items-center justify-center text-purple-600 mr-4">
              <FontAwesomeIcon icon={faDatabase} className="text-xl" />
            </div>
            <h3 className="text-xl font-bold">Bazy danych i pamięć podręczna</h3>
          </div>
          
          <div className="grid grid-cols-2 md:grid-cols-4 gap-4">
            {data.databaseTechs.map((tech, index) => (
              <TechnologyItem key={index} tech={tech} />
            ))}
          </div>
          
          <div className="mt-6 p-4 bg-purple-600/5 rounded-lg">
            <h4 className="font-semibold mb-2">Dlaczego te technologie?</h4>
            <p className="text-gray-600 text-sm">
              {data.databaseDescription}
            </p>
          </div>
        </div>
        
        {/* Infrastruktura i DevOps */}
        <div className="bg-white rounded-lg shadow-md p-8 mb-16">
          <div className="flex items-center mb-6">
            <div className="w-12 h-12 rounded-full bg-purple-600/10 flex items-center justify-center text-purple-600 mr-4">
              <FontAwesomeIcon icon={faCloud} className="text-xl" />
            </div>
            <h3 className="text-xl font-bold">Infrastruktura i DevOps</h3>
          </div>
          
          <div className="grid grid-cols-2 md:grid-cols-4 gap-4">
            {data.infraTechs.map((tech, index) => (
              <TechnologyItem key={index} tech={tech} />
            ))}
          </div>
          
          <div className="mt-6 p-4 bg-purple-600/5 rounded-lg">
            <h4 className="font-semibold mb-2">Dlaczego te technologie?</h4>
            <p className="text-gray-600 text-sm">
              {data.infraDescription}
            </p>
          </div>
        </div>
        
        {/* Jak wybieramy technologie */}
        <div className="mb-16">
          <h3 className="text-2xl font-bold mb-8 text-center">Jak dobieramy technologie?</h3>
          
          <div className="grid grid-cols-1 md:grid-cols-3 gap-6">
            {data.techSelection.map((step) => (
              <TechSelectionStep key={step.number} step={step} />
            ))}
          </div>
        </div>
        
        {/* CTA */}
        <div className="text-center">
          <PrimaryButton
            href="#realizacje"
            icon={<FontAwesomeIcon icon={faArrowRight} className="h-5 w-5" />}
          >
            Zobacz nasze realizacje
          </PrimaryButton>
        </div>
      </div>
    </section>
  );
}

/**
 * Komponent pojedynczej technologii
 * 
 * @param {Object} props
 * @param {Object} props.tech - Dane technologii
 * @param {string} props.tech.icon - Klasa ikony
 * @param {string} props.tech.name - Nazwa technologii
 * @returns {JSX.Element} Komponent technologii
 */
function TechnologyItem({ tech }) {
  return (
    <div className="flex items-center bg-gray-50 p-3 rounded-lg hover:bg-gray-100 transition-colors duration-200">
      <i className={`${tech.icon} text-xl text-purple-600 mr-3`}></i>
      <span className="text-gray-700">{tech.name}</span>
    </div>
  );
}

/**
 * Komponent kroku wyboru technologii
 * 
 * @param {Object} props
 * @param {Object} props.step - Dane kroku
 * @param {number} props.step.number - Numer kroku
 * @param {string} props.step.title - Tytuł kroku
 * @param {string} props.step.description - Opis kroku
 * @returns {JSX.Element} Komponent kroku wyboru technologii
 */
function TechSelectionStep({ step }) {
  const { number, title, description } = step;
  
  return (
    <div className="bg-white rounded-lg p-6 shadow-md hover:shadow-lg transition-all duration-300">
      <div className="w-12 h-12 rounded-full bg-purple-600/10 flex items-center justify-center text-purple-600 mb-4">
        <div className="text-xl font-bold">{number}</div>
      </div>
      <h4 className="font-bold text-lg mb-2">{title}</h4>
      <p className="text-gray-600">
        {description}
      </p>
    </div>
  );
}