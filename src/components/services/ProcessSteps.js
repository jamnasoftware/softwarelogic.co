import React from 'react';
import Link from 'next/link';
import FontAwesomeIcon from "@/components/common/FontAwesomeIcon";
import PrimaryButton from "@/components/common/PrimaryButton";
import StarryBackground from '@/components/common/StarryBackground';
import { faLightbulb, faRocket, faArrowRight } from '@fortawesome/free-solid-svg-icons';

/**
 * Komponent sekcji kroków procesu
 * 
 * @param {Object} props
 * @param {Object} props.data - Dane kroków procesu
 * @param {string} props.data.title - Tytuł sekcji
 * @param {string} props.data.subtitle - Podtytuł sekcji
 * @param {Array} props.data.steps - Kroki procesu
 * @param {string} props.data.ctaTitle - Tytuł CTA
 * @param {string} props.data.ctaSubtitle - Podtytuł CTA
 * @param {string} props.data.ctaButtonText - Tekst przycisku CTA
 * @returns {JSX.Element} Komponent kroków procesu
 */
export default function ProcessSteps({ data }) {
  return (
    <section id="proces" className="py-20 bg-gray-50">
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        {/* Nagłówek sekcji */}
        <div className="text-center mb-16">
          <h2 className="text-3xl md:text-4xl font-bold mb-4">{data.title}</h2>
          <p className="text-lg text-gray-600 max-w-3xl mx-auto">
            {data.subtitle}
          </p>
        </div>
        
        {/* Kroki procesu z osią czasu */}
        <div className="mb-20 relative">
          {/* Centralna oś czasu */}
          <div className="hidden lg:block absolute left-[30px] top-[60px] bottom-0 w-[1px] bg-purple-200"></div>
          
          {/* Kroki procesu */}
          {data.steps.map((step) => (
            <ProcessStep key={step.number} step={step} />
          ))}
        </div>
        
        {/* CTA z kosmicznym tłem */}
        <div className="rounded-lg p-8 md:p-10 shadow-lg relative overflow-hidden">
          {/* Kosmiczne tło */}
          <div className="absolute inset-0 bg-gradient-to-r from-[#312e81] to-[#4c1d95] z-0"></div>
          
          {/* Tło gwiaździste */}
          <StarryBackground />
          
          <div className="max-w-3xl mx-auto relative z-10 text-center">
            <h3 className="text-2xl font-bold mb-4 text-white">{data.ctaTitle}</h3>
            <p className="text-lg text-white/90 mb-8 md:px-8">
              {data.ctaSubtitle}
            </p>
            <div className="flex flex-wrap justify-center">
              <Link 
                href="#kontakt" 
                className="btn-primary bg-white text-purple-600 hover:bg-gray-100 inline-flex items-center px-6 py-3 rounded-lg font-medium transition-colors"
              >
                <FontAwesomeIcon icon={faRocket} className="h-5 w-5 mr-2" />
                <span>{data.ctaButtonText}</span>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

/**
 * Komponent kroku procesu
 * 
 * @param {Object} props
 * @param {Object} props.step - Dane kroku procesu
 * @returns {JSX.Element} Krok procesu
 */
function ProcessStep({ step }) {
  const { number, title, actions, duration, durationDescription, result, importance } = step;
  
  return (
    <div className="relative pt-8 pb-8 first:pt-0 last:pb-0">
      <div className="flex">
        {/* Numer kroku */}
        <div className="hidden lg:block relative">
          <div className="absolute left-0 top-0 w-[60px] h-[60px] rounded-full bg-purple-700 text-white flex items-center justify-center z-10 text-2xl font-bold">
            {number}
          </div>
        </div>
        
        {/* Treść */}
        <div className="lg:ml-[85px] w-full">
          <div className="bg-white rounded-lg shadow-md p-6">
            <div className="flex items-center mb-5">
              {/* Numer kroku w widoku mobilnym */}
              <div className="lg:hidden w-14 h-14 rounded-full bg-purple-700 text-white flex items-center justify-center mr-4 text-2xl font-bold">
                {number}
              </div>
              
              <div>
                <h3 className="text-2xl font-bold text-gray-900">{title}</h3>
              </div>
            </div>
            
            {/* Treść z 3 kolumnami */}
            <div className="grid grid-cols-1 md:grid-cols-3 gap-6 mb-6">
              {/* Kolumna "Co robimy" */}
              <div>
                <h4 className="font-semibold text-lg mb-3">Co robimy</h4>
                <ul className="space-y-2">
                  {actions.map((action, i) => (
                    <li key={i} className="flex items-start">
                      <svg className="h-5 w-5 text-purple-700 flex-shrink-0 mr-2 mt-0.5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 13l4 4L19 7" />
                      </svg>
                      <span className="text-gray-700">{action}</span>
                    </li>
                  ))}
                </ul>
              </div>
              
              {/* Kolumna "Czas trwania" */}
              <div>
                <h4 className="font-semibold text-lg mb-3">Czas trwania</h4>
                <div className="flex items-center text-purple-700 font-medium mb-2">
                  <div className="w-5 h-5 rounded-full bg-purple-700 text-white flex items-center justify-center mr-2 text-xs">t</div>
                  <span>{duration}</span>
                </div>
                <p className="text-gray-600 text-sm">
                  {durationDescription}
                </p>
              </div>
              
              {/* Kolumna "Rezultat" */}
              <div>
                <h4 className="font-semibold text-lg mb-3">Rezultat</h4>
                <p className="text-gray-700">
                  {result}
                </p>
              </div>
            </div>
            
            {/* Sekcja "Dlaczego to ważne" */}
            <div className="bg-purple-50 rounded-lg p-4">
              <h4 className="font-semibold mb-2 flex items-center">
                <div className="w-5 h-5 rounded-full bg-purple-700 text-white flex items-center justify-center mr-2 text-xs">i</div>
                <span>Dlaczego to ważne?</span>
              </h4>
              <p className="text-gray-700">
                {importance}
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}