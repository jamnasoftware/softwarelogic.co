import React, { useState } from 'react';
import Link from 'next/link';
import PrimaryButton from "@/components/common/PrimaryButton";
import FontAwesomeIcon from "@/components/common/FontAwesomeIcon";
import StarryBackground from '@/components/common/StarryBackground';
import { faArrowRight, faCheckCircle, faCommentDots } from '@fortawesome/free-solid-svg-icons';

/**
 * Komponent sekcji FAQ dla podstron usług
 * 
 * @param {Object} props
 * @param {Object} props.data - Dane FAQ
 * @param {string} props.data.title - Tytuł sekcji
 * @param {string} props.data.subtitle - Podtytuł sekcji
 * @param {Array} props.data.faqItems - Lista pytań i odpowiedzi
 * @param {string} props.data.ctaTitle - Tytuł wezwania do działania
 * @param {string} props.data.ctaText - Tekst wezwania do działania
 * @param {string} props.data.ctaButtonText - Tekst przycisku CTA
 * @returns {JSX.Element} Komponent FAQ
 */
export default function FAQ({ data }) {
  const [activeIndex, setActiveIndex] = useState(0); // Domyślnie pierwsze pytanie otwarte

  /**
   * Przełącza stan akordeonu
   * 
   * @param {number} index - Indeks pytania do przełączenia
   */
  const toggleAccordion = (index) => {
    setActiveIndex(activeIndex === index ? null : index);
  };

  return (
    <section className="py-20 bg-white">
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div className="mb-16">
          {/* Nagłówek sekcji */}
          <div className="text-center mb-12">
            <h2 className="text-3xl md:text-4xl font-bold mb-4">{data.title}</h2>
            <p className="text-lg text-gray-600 max-w-3xl mx-auto">
              {data.subtitle}
            </p>
          </div>
          
          {/* Lista pytań i odpowiedzi */}
          <div className="space-y-4 mb-12">
            {data.faqItems.map((item, index) => (
              <FaqItem 
                key={index}
                item={item}
                isActive={activeIndex === index}
                onClick={() => toggleAccordion(index)}
              />
            ))}
          </div>
          
          {/* CTA po FAQ z kosmicznym tłem */}
          <div className="rounded-lg p-8 md:p-10 shadow-lg relative overflow-hidden">
            {/* Kosmiczne tło */}
            <div className="absolute inset-0 bg-gradient-to-r from-[#312e81] to-[#4c1d95] z-0"></div>
            
            {/* Tło gwiaździste */}
            <StarryBackground />
            
            <div className="max-w-3xl mx-auto relative z-10 text-center">
              <h3 className="text-2xl font-bold mb-4 text-white">{data.ctaTitle}</h3>
              <p className="text-lg text-white/90 mb-8 md:px-8">
                {data.ctaText}
              </p>
              <div className="flex flex-wrap justify-center">
                <Link href="#kontakt" className="btn-primary">
                  <FontAwesomeIcon icon={faCommentDots} className="h-5 w-5 mr-2" />
                  Bezpłatna konsultacja
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

/**
 * Komponent pojedynczego elementu FAQ (pytanie i odpowiedź)
 * 
 * @param {Object} props
 * @param {Object} props.item - Dane elementu FAQ
 * @param {string} props.item.question - Pytanie
 * @param {Array} props.item.answer - Odpowiedź (tekst lub obiekty specjalne)
 * @param {boolean} props.isActive - Czy element jest aktywny (rozwinięty)
 * @param {Function} props.onClick - Funkcja wywoływana po kliknięciu
 * @returns {JSX.Element} Element FAQ
 */
function FaqItem({ item, isActive, onClick }) {
  return (
    <div 
      className={`border ${isActive ? 'border-purple-600/30 shadow-lg' : 'border-gray-200'} rounded-lg overflow-hidden transition-all duration-300`}
    >
      <button 
        className={`w-full text-left p-5 flex justify-between items-center ${isActive ? 'bg-purple-600/5' : 'bg-white hover:bg-gray-50'} transition-colors duration-200`}
        onClick={onClick}
        aria-expanded={isActive}
      >
        <h4 className={`text-lg font-bold ${isActive ? 'text-purple-600' : 'text-gray-800'}`}>
          {item.question}
        </h4>
        <div 
          className={`flex-shrink-0 ml-4 h-8 w-8 flex items-center justify-center rounded-full ${isActive ? 'bg-purple-600 text-white' : 'bg-gray-100 text-gray-500'} transition-all duration-300`}
        >
          <svg 
            className={`h-4 w-4 transition-transform duration-300 ${isActive ? 'rotate-180' : ''}`} 
            fill="none" 
            viewBox="0 0 24 24" 
            stroke="currentColor"
          >
            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M19 9l-7 7-7-7" />
          </svg>
        </div>
      </button>
      
      <div 
        className={`transition-all duration-300 ease-in-out ${isActive ? 'max-h-[1000px] opacity-100' : 'max-h-0 opacity-0'}`}
        aria-hidden={!isActive}
      >
        <div className="p-5 pt-0 bg-white">
          <div className="pt-4 border-t border-gray-100">
            {item.answer.map((paragraph, pidx) => (
              <FaqAnswerParagraph key={pidx} paragraph={paragraph} />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}

/**
 * Komponent akapitu odpowiedzi FAQ
 * 
 * @param {Object} props
 * @param {string|Object} props.paragraph - Treść akapitu lub obiekt specjalny
 * @returns {JSX.Element} Element akapitu odpowiedzi
 */
function FaqAnswerParagraph({ paragraph }) {
  if (typeof paragraph === 'string') {
    return (
      <p className="text-gray-600 mb-4 leading-relaxed">
        {paragraph}
      </p>
    );
  } else if (paragraph.title) {
    // Format tytułowy
    return (
      <div className="mb-4 p-4 bg-purple-600/5 rounded-lg border border-purple-600/10">
        <h5 className="font-semibold text-purple-600 mb-2 flex items-center">
          <span className="w-3 h-3 bg-purple-600 rounded-full mr-2"></span>
          {paragraph.title}
        </h5>
        <p className="text-gray-700 ml-5">{paragraph.content}</p>
      </div>
    );
  } else if (paragraph.list) {
    // Format listy
    return (
      <div className="mb-4 mt-2">
        <ul className="space-y-3">
          {paragraph.list.map((listItem, lidx) => (
            <li 
              key={lidx} 
              className="flex items-center pl-2 border-l-4 border-purple-600/30 bg-gray-50 p-3 rounded-r-lg"
            >
              <div className="flex-shrink-0 w-6 h-6 rounded-full bg-purple-600/10 flex items-center justify-center mr-3">
                <FontAwesomeIcon 
                  icon={faCheckCircle} 
                  className="h-3 w-3 text-purple-600" 
                />
              </div>
              <span className="text-gray-700">{listItem}</span>
            </li>
          ))}
        </ul>
      </div>
    );
  }
  return null;
}