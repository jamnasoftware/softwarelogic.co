import React from 'react';
import Link from 'next/link';
import PrimaryButton from "@/components/common/PrimaryButton";
import FontAwesomeIcon from "@/components/common/FontAwesomeIcon";
import StarryBackground from '@/components/common/StarryBackground';
import { faArrowRight, faCommentDots } from '@fortawesome/free-solid-svg-icons';

/**
 * Komponent przedstawiający różne typy usług
 * 
 * @param {Object} props
 * @param {Object} props.data - Dane typów usług
 * @param {string} props.data.title - Tytuł sekcji
 * @param {string} props.data.subtitle - Podtytuł sekcji
 * @param {Array} props.data.types - Lista typów usług
 * @param {string} props.data.ctaText - Tekst wezwania do działania
 * @param {string} props.data.ctaButtonText - Tekst przycisku CTA
 * @returns {JSX.Element} Komponent typów usług
 */
export default function ServiceTypes({ data }) {
  return (
    <section className="py-20 bg-white">
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        {/* Nagłówek sekcji */}
        <div className="text-center mb-16">
          <h2 className="text-3xl md:text-4xl font-bold mb-4">{data.title}</h2>
          <p className="text-lg text-gray-600 max-w-3xl mx-auto">
            {data.subtitle}
          </p>
        </div>
        
        {/* Karty typów usług */}
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8 mb-16">
          {data.types.map((type, index) => (
            <ServiceTypeCard key={index} type={type} />
          ))}
        </div>
        
        {/* CTA z kosmicznym tłem */}
        <div className="rounded-lg p-8 md:p-10 shadow-lg relative overflow-hidden">
          {/* Kosmiczne tło */}
          <div className="absolute inset-0 bg-gradient-to-r from-[#312e81] to-[#4c1d95] z-0"></div>
          
          {/* Tło gwiaździste */}
          <StarryBackground />
          
          <div className="max-w-3xl mx-auto relative z-10 text-center">
            <h3 className="text-2xl font-bold mb-4 text-white">Potrzebujesz wsparcia w realizacji projektu?</h3>
            <p className="text-lg text-white/90 mb-8 md:px-8">
              {data.ctaText}
            </p>
            <div className="flex flex-wrap justify-center">
              <Link href="#kontakt" className="btn-primary">
                <FontAwesomeIcon icon={faCommentDots} className="h-5 w-5 mr-2" />
                Bezpłatna konsultacja
              </Link>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

/**
 * Komponent karty typu usługi
 * 
 * @param {Object} props
 * @param {Object} props.type - Dane typu usługi
 * @param {string} props.type.icon - Klasa ikony
 * @param {string} props.type.title - Tytuł typu usługi
 * @param {string} props.type.description - Opis typu usługi
 * @returns {JSX.Element} Karta typu usługi
 */
function ServiceTypeCard({ type }) {
  const { icon, title, description } = type;
  
  return (
    <div className="bg-white rounded-lg shadow-md p-6 border-t-4 border-purple-600 hover:shadow-lg transition-all duration-300">
      <div className="w-16 h-16 mx-auto mb-4 rounded-full bg-purple-600/10 flex items-center justify-center">
        <i className={`${icon} text-3xl text-purple-600`}></i>
      </div>
      <h3 className="text-xl font-bold mb-3 text-center">{title}</h3>
      <p className="text-gray-600 text-center mb-4">
        {description}
      </p>
    </div>
  );
}