import React, { useState } from 'react';
import FontAwesomeIcon from "@/components/common/FontAwesomeIcon";
import { faEnvelope, faPhone } from '@fortawesome/free-solid-svg-icons';

/**
 * Komponent formularza kontaktowego dla podstron usług
 * 
 * @param {Object} props
 * @param {Object} props.data - Dane dla sekcji kontaktowej
 * @param {string} props.data.title - Tytuł sekcji
 * @param {string} props.data.subtitle - Podtytuł sekcji
 * @param {string} props.data.email - Adres e-mail kontaktowy
 * @param {string} props.data.phone - Numer telefonu kontaktowy
 * @param {string} props.data.formTitle - Tytuł formularza
 * @param {string} props.data.formId - ID formularza (do integracji z zewnętrznymi systemami)
 * @returns {JSX.Element} Komponent formularza kontaktowego
 */
export default function ContactForm({ data }) {
  const [formData, setFormData] = useState({
    name: '',
    email: '',
    company: '',
    message: ''
  });

  /**
   * Obsługuje zmiany wartości pól formularza
   * 
   * @param {Object} e - Event zmiany pola
   */
  const handleChange = (e) => {
    const { id, value } = e.target;
    setFormData((prev) => ({
      ...prev,
      [id]: value
    }));
  };

  /**
   * Obsługuje wysłanie formularza
   * 
   * @param {Object} e - Event wysłania formularza
   */
  const handleSubmit = (e) => {
    e.preventDefault();
    // Tutaj dodałbyś logikę do wysłania danych formularza, np. przez API
    console.log('Form submitted:', formData);
    
    // Reset formularza po wysłaniu
    setFormData({
      name: '',
      email: '',
      company: '',
      message: ''
    });
    
    // Pokazanie powiadomienia o sukcesie
    alert('Dziękujemy za wiadomość! Skontaktujemy się z Tobą w ciągu 24 godzin.');
  };

  return (
    <section id="kontakt" className="py-20 bg-gray-50">
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div className="grid grid-cols-1 lg:grid-cols-5 gap-8">
          {/* Informacje kontaktowe */}
          <div className="lg:col-span-2 bg-gradient-to-br from-purple-600 to-purple-800 text-white rounded-lg p-8 relative overflow-hidden">
            {/* Elementy dekoracyjne tła */}
            <div className="absolute bottom-0 right-0 w-80 h-80 bg-purple-500/30 rounded-full transform translate-x-1/2 translate-y-1/3"></div>
            <div className="absolute top-0 left-0 w-60 h-60 bg-purple-700/20 rounded-full transform -translate-x-1/2 -translate-y-1/3"></div>
            
            <div className="relative z-10">
              <h2 className="text-3xl font-bold mb-6">{data.title}</h2>
              <p className="text-white/90 mb-8">
                {data.subtitle}
              </p>
              
              <div className="space-y-6">
                {/* Email */}
                <ContactItem 
                  icon={faEnvelope} 
                  label="Email" 
                  value={data.email} 
                  href={`mailto:${data.email}`} 
                />
                
                {/* Telefon */}
                <ContactItem 
                  icon={faPhone} 
                  label="Telefon" 
                  value={data.phone} 
                  href={`tel:${data.phone.replace(/\s+/g, '')}`} 
                />
              </div>
            </div>
          </div>
          
          {/* Formularz */}
          <div className="lg:col-span-3 bg-white rounded-lg shadow-md p-8">
            <h3 className="text-2xl font-bold mb-6">{data.formTitle}</h3>
            <div id={data.formId}>
              {/* Tutaj implementowałbyś faktyczny formularz lub wczytywał zewnętrzny formularz */}
              <div id="contact-form"></div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

/**
 * Komponent elementu informacji kontaktowej
 * 
 * @param {Object} props
 * @param {import('@fortawesome/fontawesome-svg-core').IconDefinition} props.icon - Ikona FontAwesome
 * @param {string} props.label - Etykieta (np. "Email", "Telefon")
 * @param {string} props.value - Wartość (np. adres email, numer telefonu)
 * @param {string} [props.href] - Link (np. mailto:, tel:)
 * @returns {JSX.Element} Element informacji kontaktowej
 */
function ContactItem({ icon, label, value, href }) {
  return (
    <div className="flex items-start">
      <div className="flex-shrink-0 mt-1">
        <div className="bg-white/20 p-2 rounded-full">
          <FontAwesomeIcon icon={icon} />
        </div>
      </div>
      <div className="ml-4">
        <h4 className="font-semibold">{label}</h4>
        {href ? (
          <a href={href} className="text-white/90 hover:text-white transition-colors duration-300">
            {value}
          </a>
        ) : (
          <p className="text-white/90">{value}</p>
        )}
      </div>
    </div>
  );
}