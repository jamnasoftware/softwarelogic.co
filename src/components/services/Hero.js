import React, { useEffect, useState, useRef } from 'react';
import Image from 'next/image';
import Link from 'next/link';

/**
 * Zupełnie nowa koncepcja sekcji Hero dla SoftwareLogic
 */
export default function Hero({ data }) {
  const [displayText, setDisplayText] = useState("");
  const [isDeleting, setIsDeleting] = useState(false);
  const [currentFeature, setCurrentFeature] = useState(0);
  const [typingSpeed, setTypingSpeed] = useState(100);
  const [isMounted, setIsMounted] = useState(false);
  
  const features = data.features || [];
  const containerRef = useRef(null);

  // Efekt dla animacji pisania
  useEffect(() => {
    setIsMounted(true);
    
    // Jeśli brak funkcji, zakończ
    if (features.length === 0) return;
    
    let timeout;

    if (isDeleting) {
      // Usuwanie tekstu
      setTypingSpeed(30);
      timeout = setTimeout(() => {
        setDisplayText(prev => prev.substring(0, prev.length - 1));
        if (displayText === '') {
          setIsDeleting(false);
          setCurrentFeature(prevIndex => (prevIndex + 1) % features.length);
        }
      }, typingSpeed);
    } else {
      // Dodawanie tekstu
      const currentText = features[currentFeature];
      setTypingSpeed(50);
      if (displayText.length < currentText.length) {
        timeout = setTimeout(() => {
          setDisplayText(currentText.substring(0, displayText.length + 1));
        }, typingSpeed);
      } else {
        // Pauza przed usuwaniem
        timeout = setTimeout(() => {
          setIsDeleting(true);
        }, 2000);
      }
    }

    return () => clearTimeout(timeout);
  }, [displayText, isDeleting, currentFeature, features, typingSpeed]);

  return (
    <section className="relative bg-gradient-to-b from-[#0f172a] via-[#1e1b4b] to-[#312e81] text-white overflow-hidden pb-24">
      {/* Efekt gwiaździstego nieba z kolorowymi akcentami */}
      <div className="absolute inset-0 overflow-hidden">
        <div className="stars-container">
          <div className="stars"></div>
          <div className="stars2"></div>
          <div className="stars3"></div>
          <div className="nebula1"></div>
          <div className="nebula2"></div>
          <div className="shooting-star"></div>
        </div>
      </div>
      
      {/* Style dla animacji tła */}
      <style jsx global>{`
        @keyframes animStar {
          from { transform: translateY(0); }
          to { transform: translateY(-2000px); }
        }
        
        @keyframes pulsate {
          0% { opacity: 0.2; }
          50% { opacity: 0.5; }
          100% { opacity: 0.2; }
        }
        
        @keyframes shootingstar {
          0% { transform: translateX(0) translateY(0) rotate(315deg); opacity: 1; }
          70% { opacity: 1; }
          100% { transform: translateX(-1000px) translateY(1000px) rotate(315deg); opacity: 0; }
        }
        
        .stars-container {
          position: absolute;
          top: 0;
          left: 0;
          width: 100%;
          height: 300%;
          background: transparent;
        }
        
        .stars {
          background-image: radial-gradient(2px 2px at 20px 30px, #eee, rgba(0,0,0,0)),
                            radial-gradient(2px 2px at 40px 70px, #fff, rgba(0,0,0,0)),
                            radial-gradient(1px 1px at 90px 40px, #fff, rgba(0,0,0,0));
          background-repeat: repeat;
          background-size: 200px 200px;
          opacity: 0.4;
          animation: animStar 100s linear infinite;
          position: absolute;
          top: 0;
          left: 0;
          width: 100%;
          height: 100%;
        }
        
        .stars2 {
          background-image: radial-gradient(1px 1px at 100px 150px, #fff, rgba(0,0,0,0)),
                            radial-gradient(1px 1px at 200px 220px, #fff, rgba(0,0,0,0)),
                            radial-gradient(1px 1px at 300px 300px, #fff, rgba(0,0,0,0));
          background-repeat: repeat;
          background-size: 400px 400px;
          opacity: 0.5;
          animation: animStar 150s linear infinite;
          position: absolute;
          top: 0;
          left: 0;
          width: 100%;
          height: 100%;
        }
        
        .stars3 {
          background-image: radial-gradient(1px 1px at 50px 80px, #fff, rgba(0,0,0,0)),
                            radial-gradient(1px 1px at 150px 250px, #fff, rgba(0,0,0,0)),
                            radial-gradient(1px 1px at 250px 350px, #fff, rgba(0,0,0,0));
          background-repeat: repeat;
          background-size: 300px 300px;
          opacity: 0.3;
          animation: animStar 200s linear infinite;
          position: absolute;
          top: 0;
          left: 0;
          width: 100%;
          height: 100%;
        }
        
        .nebula1 {
          position: absolute;
          top: 20%;
          right: 15%;
          width: 150px;
          height: 150px;
          border-radius: 50%;
          background: radial-gradient(circle at center, rgba(138, 92, 246, 0.2) 0%, rgba(102, 51, 153, 0) 70%);
          animation: pulsate 10s ease-in-out infinite;
        }
        
        .nebula2 {
          position: absolute;
          bottom: 30%;
          left: 20%;
          width: 180px;
          height: 180px;
          border-radius: 50%;
          background: radial-gradient(circle at center, rgba(99, 102, 241, 0.2) 0%, rgba(51, 102, 153, 0) 70%);
          animation: pulsate 13s ease-in-out infinite;
          animation-delay: 2s;
        }
        
        .shooting-star {
          position: absolute;
          top: 20%;
          right: 5%;
          width: 100px;
          height: 2px;
          background: linear-gradient(to right, rgba(255,255,255,0) 0%, rgba(255,255,255,0.8) 50%, rgba(255,255,255,0) 100%);
          opacity: 0;
          transform: rotate(315deg);
          animation: shootingstar 6s ease-in-out infinite;
          animation-delay: 3s;
        }
        
        @keyframes float {
          0% { transform: translateY(0px); }
          50% { transform: translateY(-15px); }
          100% { transform: translateY(0px); }
        }
        
        .floating {
          animation: float 6s ease-in-out infinite;
        }
        
        .bg-glass {
          background: rgba(255, 255, 255, 0.05);
          backdrop-filter: blur(10px);
          border: 1px solid rgba(255, 255, 255, 0.1);
        }
        
        .text-gradient {
          background: linear-gradient(45deg, #8a5cf6, #6366f1);
          -webkit-background-clip: text;
          background-clip: text;
          color: transparent;
          display: inline;
        }
        
        .btn-primary {
          background: linear-gradient(to right, #6366f1, #a855f7);
          color: white;
          padding: 0.75rem 1.5rem;
          border-radius: 0.375rem;
          font-weight: 500;
          transition: all 0.3s ease;
          display: inline-flex;
          align-items: center;
          box-shadow: 0 4px 14px rgba(99, 102, 241, 0.4);
        }
        
        .btn-primary:hover {
          transform: translateY(-2px);
          box-shadow: 0 6px 20px rgba(99, 102, 241, 0.6);
        }
        
        .btn-secondary {
          background: transparent;
          color: white;
          padding: 0.75rem 1.5rem;
          border-radius: 0.375rem;
          font-weight: 500;
          transition: all 0.3s ease;
          display: inline-flex;
          align-items: center;
          border: 1px solid rgba(255, 255, 255, 0.3);
        }
        
        .typing-cursor {
          display: inline-block;
          width: 2px;
          height: 1em;
          margin-left: 2px;
          background-color: white;
          animation: blink 1s infinite;
          vertical-align: text-bottom;
        }
        
        @keyframes blink {
          0%, 100% { opacity: 1; }
          50% { opacity: 0; }
        }
        
        .typing-text {
          word-break: break-word;
          white-space: pre-wrap;
        }
      `}</style>
      
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 pt-32 pb-12 md:pb-16 relative z-10" ref={containerRef}>
        <div className="flex flex-col lg:flex-row items-center">
          {/* Lewa kolumna z tekstem */}
          <div className="lg:w-1/2 text-center lg:text-left mb-12 lg:mb-0">
            <h1 className="text-4xl md:text-5xl lg:text-6xl font-bold leading-tight mb-6">
              {data.title}
            </h1>
            
            <p className="text-xl text-white/80 mb-2">
              {data.subtitle || "Dostarczamy dedykowane aplikacje webowe, które:"}
            </p>
            
            {/* Feature tabs - efekt pisania maszyny do pisania bezpośrednio pod subtitle */}
            {features.length > 0 && isMounted && (
              <div className="max-w-xl mb-8">
                <div className="min-h-[80px] typing-container">
                  <p className="font-bold text-xl text-white typing-text">
                    {displayText}
                    <span className="typing-cursor"></span>
                  </p>
                </div>
              </div>
            )}
            
            {/* CTA Buttons */}
            <div className="flex flex-col sm:flex-row gap-4">
              <Link href="#proces" className="btn-primary">
                <svg className="w-5 h-5 mr-2" viewBox="0 0 20 20" fill="currentColor">
                  <path fillRule="evenodd" d="M10.293 5.293a1 1 0 011.414 0l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414-1.414L12.586 11H5a1 1 0 110-2h7.586l-2.293-2.293a1 1 0 010-1.414z" clipRule="evenodd" />
                </svg>
                Poznaj nasz proces
              </Link>
              
              <Link href="#kontakt" className="btn-secondary">
                Bezpłatna konsultacja
              </Link>
            </div>
          </div>
          
          {/* Prawa kolumna z ilustracją */}
          <div className="lg:w-1/2 pl-0 lg:pl-12">
            <div className="relative">
              {/* 3D Element */}
              <div className="absolute -top-20 -right-20 w-64 h-64 bg-gradient-to-br from-indigo-600/30 to-purple-600/30 rounded-full blur-3xl"></div>
              
              {/* Main Image or illustration */}
              <div className="relative z-10 floating">
                {data.image ? (
                  <div className="rounded-xl overflow-hidden shadow-2xl">
                    <Image 
                      src={data.image.src || "https://images.pexels.com/photos/7988079/pexels-photo-7988079.jpeg"} 
                      alt={data.image.alt || "Ilustracja aplikacji"} 
                      width={600}
                      height={450}
                      className="w-full h-auto"
                    />
                  </div>
                ) : (
                  <div className="rounded-xl overflow-hidden shadow-2xl">
                    <Image 
                      src="https://images.pexels.com/photos/7988079/pexels-photo-7988079.jpeg" 
                      alt="Programista pracujący nad kodem" 
                      width={600}
                      height={450}
                      className="w-full h-auto"
                    />
                  </div>
                )}
              </div>
              
              {/* Stats card - używamy danych z obiektu */}
              {data.rows && data.rows.length > 0 && (
                <div className="absolute bottom-8 -left-10 bg-glass rounded-lg p-4 shadow-lg z-20 hidden md:block">
                  <div className="flex items-center space-x-4">
                    <div className="p-2 bg-indigo-600/30 rounded-lg">
                      <svg className="w-6 h-6 text-indigo-400" fill="none" stroke="currentColor" viewBox="0 0 24 24">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M13 10V3L4 14h7v7l9-11h-7z" />
                      </svg>
                    </div>
                    <div>
                      <div className="text-sm text-white/70">Projekty</div>
                      <div className="text-lg font-semibold text-white">30+</div>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
      
      {/* Delikatna fala na dole - bardziej płaska */}
      <div className="absolute bottom-0 left-0 right-0">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 60" className="w-full h-auto">
          <path fill="#ffffff" fillOpacity="1" d="M0,16L48,18.7C96,21,192,27,288,29.3C384,32,480,32,576,26.7C672,21,768,11,864,10.7C960,11,1056,21,1152,24C1248,27,1344,21,1392,18.7L1440,16L1440,60L1392,60C1344,60,1248,60,1152,60C1056,60,960,60,864,60C768,60,672,60,576,60C480,60,384,60,288,60C192,60,96,60,48,60L0,60Z"></path>
        </svg>
      </div>
    </section>
  );
}