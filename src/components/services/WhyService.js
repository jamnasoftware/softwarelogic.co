import React from 'react';
import PrimaryButton from "@/components/common/PrimaryButton";
import FontAwesomeIcon from "@/components/common/FontAwesomeIcon";
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';

/**
 * Komponent sekcji korzyści z usługi
 * 
 * @param {Object} props
 * @param {Object} props.data - Dane sekcji korzyści
 * @param {string} props.data.title - Tytuł sekcji
 * @param {string} props.data.subtitle - Podtytuł sekcji
 * @param {Array} props.data.advantages - Lista korzyści
 * @param {string} props.data.ctaLink - Link przycisku CTA
 * @param {string} props.data.ctaText - Tekst przycisku CTA
 * @returns {JSX.Element} Komponent korzyści z usługi
 */
export default function WhyService({ data }) {
  return (
    <section className="py-20 bg-white">
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        {/* Nagłówek sekcji */}
        <div className="text-center mb-12">
          <h2 className="text-3xl md:text-4xl font-bold mb-4">{data.title}</h2>
          <p className="text-lg text-gray-600 max-w-3xl mx-auto">
            {data.subtitle}
          </p>
        </div>
        
        {/* Karty korzyści */}
        <div className="grid grid-cols-1 md:grid-cols-3 gap-8 mb-12">
          {data.advantages.map((advantage, index) => (
            <AdvantageCard key={index} advantage={advantage} />
          ))}
        </div>
        
        {/* CTA */}
        <div className="text-center">
          <PrimaryButton 
            href={data.ctaLink}
            icon={<FontAwesomeIcon icon={faArrowRight} className="h-5 w-5" />}
          >
            {data.ctaText}
          </PrimaryButton>
        </div>
      </div>
    </section>
  );
}

/**
 * Komponent karty korzyści
 * 
 * @param {Object} props
 * @param {Object} props.advantage - Dane korzyści
 * @param {string} props.advantage.icon - Klasa ikony
 * @param {string} props.advantage.title - Tytuł korzyści
 * @param {string} props.advantage.description - Opis korzyści
 * @returns {JSX.Element} Karta korzyści
 */
function AdvantageCard({ advantage }) {
  const { icon, title, description } = advantage;
  
  return (
    <div className="bg-white rounded-lg shadow-md p-6 hover:shadow-lg transition-all duration-300">
      <div className="w-12 h-12 rounded-full bg-purple-600/10 flex items-center justify-center text-purple-600 mb-4">
        <i className={`${icon} text-xl`}></i>
      </div>
      <h3 className="text-xl font-bold mb-2">{title}</h3>
      <p className="text-gray-600">
        {description}
      </p>
    </div>
  );
}