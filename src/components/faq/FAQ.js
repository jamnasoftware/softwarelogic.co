import React, { useState } from 'react';
import Link from 'next/link';
import PrimaryButton from "@/components/common/PrimaryButton";
import FontAwesomeIcon from "@/components/common/FontAwesomeIcon";
import StarryBackground from '@/components/common/StarryBackground';
import { faArrowRight, faCheckCircle, faCommentDots } from '@fortawesome/free-solid-svg-icons';

/**
 * Dane FAQ
 */
const faqData = {
  title: "Najczęściej zadawane pytania",
  subtitle: "Znaleźliśmy odpowiedzi na najczęstsze pytania dotyczące naszych usług",
  ctaTitle: "Masz więcej pytań?",
  ctaText: "Nie znalazłeś odpowiedzi na swoje pytanie? Nasz zespół ekspertów jest gotów pomóc Ci w realizacji Twojego projektu.",
  ctaButtonText: "Skontaktuj się z nami",
  faqItems: [
    {
      question: "Ile kosztuje realizacja projektu IT?",
      answer: [
        "Koszt zależy od charakteru i złożoności projektu. Nasza oferta obejmuje: aplikacje webowe, desktop i mobilne, rozwiązania no-code oraz low-code, usługi DevOps i cloud, integracje systemów oraz architektury mikroserwisowe. Projekty startują już od kilku tysięcy złotych za prostsze rozwiązania, do kilkuset tysięcy zł za zaawansowane platformy i ekosystemy technologiczne.",
        "Po analizie potrzeb przygotowujemy szczegółową wycenę dopasowaną do Twojego biznesu. Zawsze staramy się zoptymalizować koszty, proponując rozwiązania, które najlepiej realizują cele biznesowe w zadanym budżecie."
      ]
    },
    {
      question: "Jak długo trwa realizacja projektu?",
      answer: [
        "Proces składa się z 6 kroków, a całkowity czas realizacji zależy od złożoności i zakresu. Dla prostszych aplikacji MVP możemy dostarczyć działające rozwiązanie już w 2-3 miesiące, podczas gdy bardziej złożone projekty trwają zwykle od kilku do kilkunastu miesięcy",
        "Zawsze pracujemy w metodologii zwinnej, dostarczając funkcjonalności etapowo, dzięki czemu już w trakcie procesu rozwoju otrzymujesz wartość biznesową."
      ]
    },
    {
      question: "Jakie są formy rozliczenia projektu?",
      answer: [
        "Oferujemy dwa główne modele rozliczeń, dostosowane do charakteru projektu:",
        { 
          title: "Fixed Price (Stała cena)", 
          content: "Idealny dla projektów o dobrze zdefiniowanym zakresie. Ustalamy cenę na początku współpracy, co daje pełną przewidywalność budżetu. Ten model sprawdza się najlepiej dla mniejszych projektów lub jasno określonych MVP."
        },
        { 
          title: "Time & Materials (Rozliczenie godzinowe)", 
          content: "Elastyczne rozliczenie oparte o rzeczywisty czas pracy. Stawka: 150 zł netto/godzina. Ten model sprawdza się przy projektach rozwojowych, gdzie zakres może ewoluować, dając maksymalną elastyczność i transparentność."
        }
      ]
    },
    {
      question: "Czy oferujecie wsparcie po wdrożeniu?",
      answer: [
        "Tak, oferujemy kompleksowe wsparcie techniczne po wdrożeniu rozwiązań. Nasze usługi obejmują:",
        {
          list: [
            "Monitoring i utrzymanie techniczne",
            "Regularne aktualizacje bezpieczeństwa",
            "Dalszy rozwój funkcjonalności"
          ]
        },
        "Oferujemy różne pakiety SLA (Service Level Agreement) dostosowane do potrzeb i skali Twojego biznesu."
      ]
    },
    {
      question: "Kto będzie właścicielem kodu aplikacji?",
      answer: [
        "Po zakończeniu projektu i dokonaniu pełnej płatności, Ty jako klient otrzymujesz pełne prawa własności intelektualnej do kodu źródłowego i wszystkich komponentów rozwiązania. Oznacza to swobodę w dalszym rozwoju, modyfikacji i wykorzystaniu systemu bez dodatkowych opłat licencyjnych. Wyjątkiem są jedynie komponenty open-source, które pozostają objęte swoimi oryginalnymi licencjami."
      ]
    },
    {
      question: "Czy mogę wprowadzać zmiany w projekcie w trakcie realizacji?",
      answer: [
        "Tak, dzięki podejściu zwinnemu (Agile) możesz wprowadzać zmiany podczas realizacji projektu. Pracujemy w 2-tygodniowych sprintach, po każdym z nich prezentujemy postępy i zbieramy feedback.",
        "Dla projektów rozliczanych w modelu Fixed Price, znaczące zmiany zakresu mogą wymagać renegocjacji. Przy modelu Time & Materials masz pełną swobodę modyfikacji zakresu w trakcie trwania projektu."
      ]
    },
    {
      question: "Jakie technologie najlepiej sprawdzą się w moim projekcie?",
      answer: [
        "Dobór technologii zależy od wielu czynników, w tym:",
        {
          list: [
            "Charakteru i złożoności projektu",
            "Oczekiwanej skalowalności",
            "Integracji z istniejącymi systemami"
          ]
        },
        "Podczas konsultacji analizujemy Twoje potrzeby i rekomendujemy optymalne rozwiązania technologiczne, które zapewnią wydajność, bezpieczeństwo i przyszłościowość systemu."
      ]
    },
    {
      question: "Czy potrzebuję własnego zespołu IT do utrzymania systemów?",
      answer: [
        "Nie, nie jest to konieczne. Oferujemy kompleksowe usługi utrzymania i rozwoju, w tym zarządzanie infrastrukturą i usługi DevOps, dzięki czemu Twoja firma może skupić się na swojej podstawowej działalności.",
        "Jeśli jednak posiadasz własny zespół IT, możemy przekazać wiedzę i dokumentację, aby umożliwić im przejęcie odpowiedzialności za utrzymanie systemu po zakończeniu projektu."
      ]
    }
  ]
};

/**
 * Komponent sekcji FAQ (Najczęściej Zadawane Pytania)
 * 
 * @returns {JSX.Element} Komponent FAQ
 */
export default function FAQ() {
  const [activeIndex, setActiveIndex] = useState(0); // Domyślnie pierwsza odpowiedź otwarta

  /**
   * Przełącza stan akordeonu
   * 
   * @param {number} index - Indeks pytania do przełączenia
   */
  const toggleAccordion = (index) => {
    setActiveIndex(activeIndex === index ? null : index);
  };

  return (
    <section className="py-20 bg-white">
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div className="mb-16">
          {/* Nagłówek sekcji */}
          <div className="text-center mb-12">
            <h2 className="text-3xl md:text-4xl font-bold mb-4">{faqData.title}</h2>
            <p className="text-lg text-gray-600 max-w-3xl mx-auto">
              {faqData.subtitle}
            </p>
          </div>
          
          {/* Lista pytań i odpowiedzi */}
          <div className="space-y-4 mb-12">
            {faqData.faqItems.map((item, index) => (
              <FaqItem 
                key={index}
                item={item}
                isActive={activeIndex === index}
                onClick={() => toggleAccordion(index)}
              />
            ))}
          </div>
          
          {/* CTA po FAQ z kosmicznym tłem */}
          <div className="rounded-lg p-8 md:p-10 shadow-lg relative overflow-hidden">
            {/* Kosmiczne tło */}
            <div className="absolute inset-0 bg-gradient-to-r from-[#312e81] to-[#4c1d95] z-0"></div>
            
            {/* Tło gwiaździste - użycie wspólnego komponentu StarryBackground */}
            <StarryBackground />
            
            <div className="max-w-3xl mx-auto relative z-10 text-center">
              <h3 className="text-2xl font-bold mb-4 text-white">{faqData.ctaTitle}</h3>
              <p className="text-lg text-white/90 mb-8 md:px-8">
                {faqData.ctaText}
              </p>
              <div className="flex flex-wrap justify-center">
                <Link href="#kontakt" className="btn-primary">
                  <FontAwesomeIcon icon={faCommentDots} className="h-5 w-5 mr-2" />
                  Bezpłatna konsultacja
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

/**
 * Komponent pojedynczego elementu FAQ
 * 
 * @param {Object} props
 * @param {Object} props.item - Dane elementu FAQ
 * @param {boolean} props.isActive - Czy element jest aktywny (otwarty)
 * @param {Function} props.onClick - Funkcja obsługująca kliknięcie
 * @returns {JSX.Element} Element FAQ
 */
function FaqItem({ item, isActive, onClick }) {
  return (
    <div 
      className={`border ${isActive ? 'border-purple-600/30 shadow-lg' : 'border-gray-200'} rounded-lg overflow-hidden transition-all duration-300`}
    >
      <button 
        className={`w-full text-left p-5 flex justify-between items-center ${isActive ? 'bg-purple-600/5' : 'bg-white hover:bg-gray-50'} transition-colors duration-200`}
        onClick={onClick}
        aria-expanded={isActive}
      >
        <h4 className={`text-lg font-bold ${isActive ? 'text-purple-600' : 'text-gray-800'}`}>
          {item.question}
        </h4>
        <div 
          className={`flex-shrink-0 ml-4 h-8 w-8 flex items-center justify-center rounded-full ${isActive ? 'bg-purple-600 text-white' : 'bg-gray-100 text-gray-500'} transition-all duration-300`}
        >
          <svg 
            className={`h-4 w-4 transition-transform duration-300 ${isActive ? 'rotate-180' : ''}`} 
            fill="none" 
            viewBox="0 0 24 24" 
            stroke="currentColor"
          >
            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M19 9l-7 7-7-7" />
          </svg>
        </div>
      </button>
      
      <div 
        className={`transition-all duration-300 ease-in-out ${isActive ? 'max-h-[1000px] opacity-100' : 'max-h-0 opacity-0'}`}
        aria-hidden={!isActive}
      >
        <div className="p-5 pt-0 bg-white">
          <div className="pt-4 border-t border-gray-100">
            {item.answer.map((paragraph, pidx) => (
              <FaqAnswerParagraph key={pidx} paragraph={paragraph} />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}

/**
 * Komponent akapitu odpowiedzi FAQ
 * 
 * @param {Object} props
 * @param {string|Object} props.paragraph - Treść akapitu lub obiekt specjalny
 * @returns {JSX.Element} Element akapitu odpowiedzi
 */
function FaqAnswerParagraph({ paragraph }) {
  if (typeof paragraph === 'string') {
    return (
      <p className="text-gray-600 mb-4 leading-relaxed">
        {paragraph}
      </p>
    );
  } else if (paragraph.title) {
    // Format tytułowy
    return (
      <div className="mb-4 p-4 bg-purple-600/5 rounded-lg border border-purple-600/10">
        <h5 className="font-semibold text-purple-600 mb-2 flex items-center">
          <span className="w-3 h-3 bg-purple-600 rounded-full mr-2"></span>
          {paragraph.title}
        </h5>
        <p className="text-gray-700 ml-5">{paragraph.content}</p>
      </div>
    );
  } else if (paragraph.list) {
    // Format listy
    return (
      <div className="mb-4 mt-2">
        <ul className="space-y-3">
          {paragraph.list.map((listItem, lidx) => (
            <li 
              key={lidx} 
              className="flex items-center pl-2 border-l-4 border-purple-600/30 bg-gray-50 p-3 rounded-r-lg"
            >
              <div className="flex-shrink-0 w-6 h-6 rounded-full bg-purple-600/10 flex items-center justify-center mr-3">
                <FontAwesomeIcon 
                  icon={faCheckCircle} 
                  className="h-3 w-3 text-purple-600" 
                />
              </div>
              <span className="text-gray-700">{listItem}</span>
            </li>
          ))}
        </ul>
      </div>
    );
  }
  return null;
}