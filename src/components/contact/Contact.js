import React from 'react';
import {
  faEnvelope,
  faPhone,
  faMapMarkerAlt,
  faCalendarAlt
} from '@fortawesome/free-solid-svg-icons';
import FontAwesomeIcon from "@/components/common/FontAwesomeIcon";
import PrimaryButton from "@/components/common/PrimaryButton";

/**
 * Sekcja kontaktowa z formularzem i informacjami kontaktowymi
 * 
 * @returns {JSX.Element} Komponent sekcji kontaktowej
 */
export default function Contact() {
  return (
    <section id="kontakt" className="py-20 bg-gray-50">
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        {/* Nagłówek sekcji */}
        <div className="text-center mb-12">
          <h2 className="text-3xl md:text-4xl font-bold mb-4">Skontaktuj się z nami</h2>
          <p className="text-lg text-gray-600 max-w-3xl mx-auto">
            Porozmawiajmy o Twoim projekcie i sprawdźmy, jak możemy pomóc
          </p>
        </div>

        <div className="flex flex-col lg:flex-row gap-8">
          {/* Formularz kontaktowy */}
          <div className="lg:w-2/3 bg-white rounded-lg shadow-md p-8">
            <div id="contact-form"></div>
          </div>

          {/* Dane kontaktowe i CTA */}
          <div className="lg:w-1/3 flex flex-col gap-6">
            {/* Dane kontaktowe */}
            <div className="bg-white rounded-lg shadow-md p-6">
              <h3 className="text-xl font-bold mb-4">JAMNA Software Sp. z o.o.</h3>
              <div className="space-y-4">
                <ContactItem 
                  icon={faEnvelope} 
                  label="Email" 
                  value="office@softwarelogic.co" 
                  href="mailto:office@softwarelogic.co" 
                />
                
                <ContactItem 
                  icon={faPhone} 
                  label="Telefon" 
                  value="+48 724 792 148" 
                  href="tel:+48724792148" 
                />
                
                <ContactItem 
                  icon={faMapMarkerAlt} 
                  label="Adres" 
                  value="73-110 Stargard" 
                />
              </div>
            </div>

            {/* Bezpłatna konsultacja */}
            <div className="bg-gradient-to-r from-purple-600 to-purple-800 rounded-lg shadow-md p-6 text-white flex-1">
              <h3 className="text-xl font-bold mb-4">Bezpłatna konsultacja</h3>
              <p className="mb-6">
                Umów 30-minutową konsultację, podczas której omówimy Twój projekt i zaproponujemy rozwiązanie.
              </p>
              <PrimaryButton 
                href="mailto:office@softwarelogic.co"
                variant="white"
                icon={<FontAwesomeIcon icon={faCalendarAlt} />}
                iconRight={false}
                className="w-full justify-center"
              >
                Zarezerwuj termin
              </PrimaryButton>
            </div>

            {/* Godziny pracy */}
            <div className="bg-white rounded-lg shadow-md p-6">
              <h3 className="text-xl font-bold mb-4">Godziny pracy</h3>
              <div className="space-y-2">
                <div className="flex justify-between">
                  <span className="text-gray-600">Poniedziałek - Piątek:</span>
                  <span className="font-medium">9:00 - 17:00</span>
                </div>
                <div className="flex justify-between">
                  <span className="text-gray-600">Weekend:</span>
                  <span className="font-medium">Zamknięte</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

/**
 * Komponent elementu kontaktowego z ikoną
 * 
 * @param {Object} props
 * @param {import('@fortawesome/fontawesome-svg-core').IconDefinition} props.icon - Ikona FontAwesome
 * @param {string} props.label - Etykieta (np. "Email", "Telefon")
 * @param {string} props.value - Wartość do wyświetlenia
 * @param {string} [props.href] - Opcjonalny link (np. mailto:, tel:)
 * @returns {JSX.Element} Element kontaktowy
 */
function ContactItem({ icon, label, value, href }) {
  return (
    <div className="flex items-start">
      <div className="flex-shrink-0 mt-1">
        <FontAwesomeIcon icon={icon} className="text-purple-600" />
      </div>
      <div className="ml-4">
        <p className="font-medium">{label}</p>
        {href ? (
          <a 
            href={href} 
            className="text-gray-600 hover:text-purple-600 transition-colors"
          >
            {value}
          </a>
        ) : (
          <p className="text-gray-600">{value}</p>
        )}
      </div>
    </div>
  );
}