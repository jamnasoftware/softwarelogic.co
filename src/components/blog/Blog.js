import Link from 'next/link'
import BlogCard from "@/components/blog/BlogCard";

export default function Blog() {
  const posts = [
    {
      title: "Architektura mikroserwisów - kiedy warto ją zastosować?",
      description: "Analiza zalet i wad architektury mikroserwisów oraz wskazówki dotyczące jej zastosowania.",
      image: "https://images.pexels.com/photos/546819/pexels-photo-546819.jpeg",
      category: "Architektura",
      date: "12 mar 2025"
    },
    {
      title: "10 praktyk DevOps, które przyspieszą Twój cykl rozwoju",
      description: "Sprawdzone strategie automatyzacji i wdrażania, które pomogą przyspieszyć dostarczanie oprogramowania.",
      image: "https://images.pexels.com/photos/574070/pexels-photo-574070.jpeg",
      category: "DevOps",
      date: "28 lut 2025"
    },
    {
      title: "Jak wykorzystać AI do automatyzacji procesów biznesowych",
      description: "Praktyczne przykłady wdrożenia sztucznej inteligencji w codziennych operacjach biznesowych.",
      image: "https://images.pexels.com/photos/3861958/pexels-photo-3861958.jpeg",
      category: "AI",
      date: "5 lut 2025"
    }
  ]

  return (
      <section id="blog" className="py-20 bg-gray-50">
        <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
          <div className="text-center mb-12">
            <h2 className="text-3xl md:text-4xl font-bold mb-4">Blog technologiczny</h2>
            <p className="text-lg text-gray-600 max-w-3xl mx-auto">
              Dzielimy się wiedzą i doświadczeniem w budowaniu skutecznych rozwiązań
            </p>
          </div>

          {/* Wybrane artykuły - 3 kolumny */}
          <div className="grid grid-cols-1 md:grid-cols-3 gap-8 mb-12">
            {posts.map((post, index) => (
                <BlogCard key={index} post={post} />
            ))}
          </div>

          {/* Płynne przejście do sekcji kontakt */}
          <div className="flex flex-col md:flex-row items-center justify-between bg-white rounded-xl p-8 shadow-lg">
            <div className="mb-6 md:mb-0 md:mr-6">
              <h3 className="text-2xl font-bold mb-2">Chcesz być na bieżąco?</h3>
              <p className="text-gray-600">Zapisz się do naszego newslettera i otrzymuj najnowsze artykuły i porady technologiczne</p>
            </div>
            <div className="w-full md:w-auto">
              <form className="flex flex-col sm:flex-row gap-3">
                <input
                    type="email"
                    placeholder="Twój adres email"
                    className="px-4 py-3 rounded-lg border border-gray-300 w-full sm:w-64 focus:outline-none focus:ring-2 focus:ring-primary/30 focus:border-primary"
                />
                <button
                    type="submit"
                    className="btn-primary px-6 py-3 rounded-lg font-medium shadow-md hover:shadow-lg whitespace-nowrap"
                >
                  Zapisz się
                </button>
              </form>
            </div>
          </div>
        </div>
      </section>
  )
}