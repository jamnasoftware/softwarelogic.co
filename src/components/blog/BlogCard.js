import Image from 'next/image'
import Link from 'next/link'
import FontAwesomeIcon from "@/components/common/FontAwesomeIcon";

import { faArrowRight } from '@fortawesome/free-solid-svg-icons'

export default function BlogCard({ post }) {
    const { title, description, image, category, date } = post

    return (
        <div className="bg-white rounded-xl overflow-hidden shadow-lg hover:shadow-xl transition-all h-full">
            <div className="h-48 overflow-hidden">
                <Image
                    src={image}
                    alt={title}
                    width={600}
                    height={400}
                    className="w-full h-full object-cover"
                />
            </div>
            <div className="p-6">
                <div className="flex items-center justify-between mb-3">
                    <span className="bg-primary/10 text-primary text-xs font-medium px-3 py-1 rounded-full">{category}</span>
                    <span className="text-gray-500 text-sm">{date}</span>
                </div>
                <h3 className="font-bold text-xl mb-3">{title}</h3>
                <p className="text-gray-600 mb-4">{description}</p>
                <Link href="#" className="inline-flex items-center text-primary font-medium hover:underline">
                    <span>Czytaj więcej</span>
                    <FontAwesomeIcon icon={faArrowRight} className="h-5 w-5 ml-2" />
                </Link>
            </div>
        </div>
    )
}