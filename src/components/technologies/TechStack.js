import React from 'react';
import FontAwesomeIcon from "@/components/common/FontAwesomeIcon";
import {
  faCode,
  faDatabase,
  faFlask,
  faServer,
  faCloudUploadAlt,
  faDharmachakra,
  faWindowRestore,
  faAtom,
  faCube,
  faMobileAlt,
  faSearch,
  faExchangeAlt,
  faRocket,
  faNetworkWired,
  faFileCode,
  faLayerGroup,
  faChartLine,
  faProjectDiagram
} from '@fortawesome/free-solid-svg-icons';
import {
  faJs,
  faReact,
  faAngular,
  faVuejs,
  faSass,
  faPython,
  faNodeJs,
  faPhp,
  faApple,
  faAndroid,
  faAws,
  faDocker
} from '@fortawesome/free-brands-svg-icons';

/**
 * Dane technologii pogrupowane według kategorii
 */
const technologiesData = [
  // Frontend
  { name: "JavaScript", icon: faJs, category: "frontend" },
  { name: "TypeScript", icon: faFileCode, category: "frontend" },
  { name: "React", icon: faReact, category: "frontend" },
  { name: "Angular", icon: faAngular, category: "frontend" },
  { name: "Vue.js", icon: faVuejs, category: "frontend" },
  { name: "Next.js", icon: faCode, category: "frontend", customIcon: true },
  { name: "SASS/SCSS", icon: faSass, category: "frontend" },
  
  // Backend
  { name: "Python", icon: faPython, category: "backend" },
  { name: "Node.js", icon: faNodeJs, category: "backend" },
  { name: "Java", icon: faCode, category: "backend", customIcon: true },
  { name: "C#", icon: faCode, category: "backend", customIcon: true },
  { name: "Go", icon: faCode, category: "backend", customIcon: true },
  { name: "Django", icon: faServer, category: "backend" },
  { name: "Flask", icon: faFlask, category: "backend" },
  { name: "FastAPI", icon: faRocket, category: "backend" },
  { name: "PHP", icon: faPhp, category: "backend" },
  { name: "GraphQL", icon: faLayerGroup, category: "backend" },
  
  // Bazy danych
  { name: "PostgreSQL", icon: faDatabase, category: "database" },
  { name: "MongoDB", icon: faDatabase, category: "database" },
  { name: "Redis", icon: faDatabase, category: "database" },
  { name: "Elasticsearch", icon: faSearch, category: "database" },
  
  // Mobilne
  { name: "Swift", icon: faApple, category: "mobile" },
  { name: "Kotlin", icon: faAndroid, category: "mobile" },
  { name: "Flutter", icon: faMobileAlt, category: "mobile" },
  { name: "React Native", icon: faReact, category: "mobile" },
  
  // Aplikacje desktopowe
  { name: "C++", icon: faCode, category: "desktop" },
  { name: "Qt", icon: faCube, category: "desktop" },
  { name: "wxWidgets", icon: faWindowRestore, category: "desktop" },
  { name: "Electron.js", icon: faAtom, category: "desktop" },
  
  // Cloud i DevOps
  { name: "AWS", icon: faAws, category: "cloud" },
  { name: "Docker", icon: faDocker, category: "cloud" },
  { name: "Kubernetes", icon: faDharmachakra, category: "cloud" },
  { name: "OpenShift", icon: faCloudUploadAlt, category: "cloud" },
  { name: "Terraform", icon: faProjectDiagram, category: "cloud" },
  { name: "ArgoCD", icon: faDharmachakra, category: "cloud" },
  { name: "Proxmox", icon: faServer, category: "cloud" },
  { name: "GitHub Actions", icon: faCode, category: "cloud", customIcon: true },
  { name: "Azure", icon: faCode, category: "cloud", customIcon: true },
  
  // Komunikacja i Monitorowanie
  { name: "RabbitMQ", icon: faExchangeAlt, category: "monitoring" },
  { name: "Kafka", icon: faNetworkWired, category: "monitoring" },
  { name: "Prometheus", icon: faChartLine, category: "monitoring" },
  { name: "Grafana", icon: faChartLine, category: "monitoring" }
];

/**
 * Komponent stosu technologicznego
 * 
 * @returns {JSX.Element} Komponent TechStack
 */
export default function TechStack() {
  return (
    <div className="mb-12">
      <div className="bg-white rounded-lg p-8 shadow-md">
        <div className="flex items-center mb-8">
          <div className="w-14 h-14 rounded-full bg-purple-600/10 flex items-center justify-center text-purple-600 mr-4">
            <FontAwesomeIcon icon={faCode} className="text-2xl" />
          </div>
          <h3 className="text-xl font-bold">Technologie, w których się specjalizujemy</h3>
        </div>

        <div className="grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-6 gap-4">
          {technologiesData.map((tech, index) => (
            <TechItem key={index} tech={tech} />
          ))}
        </div>
      </div>
    </div>
  );
}

/**
 * Komponent pojedynczej technologii
 * 
 * @param {Object} props
 * @param {Object} props.tech - Dane technologii
 * @param {string} props.tech.name - Nazwa technologii
 * @param {import('@fortawesome/fontawesome-svg-core').IconDefinition} props.tech.icon - Ikona
 * @param {boolean} [props.tech.customIcon] - Czy jest to niestandardowa ikona
 * @returns {JSX.Element} Element technologii
 */
function TechItem({ tech }) {
  const { name, icon, customIcon } = tech;
  
  return (
    <div className="flex items-center p-3 bg-gray-50 rounded-lg hover:bg-gray-100 transition-colors duration-200">
      <FontAwesomeIcon
        icon={icon}
        className={`text-xl text-purple-600 mr-3 ${customIcon ? 'fas' : ''}`}
      />
      <span className="text-sm font-medium text-gray-700">{name}</span>
    </div>
  );
}