import React from 'react';
import Link from 'next/link';
import FontAwesomeIcon from "@/components/common/FontAwesomeIcon";
import TechStack from '@/components/technologies/TechStack';
import StarryBackground from '@/components/common/StarryBackground';
import {
  faBrain,
  faDesktop,
  faMobileAlt,
  faLaptopCode,
  faServer,
  faCloud,
  faBolt,
  faTasks,
  faBalanceScale,
  faExpandArrowsAlt,
  faArrowRight,
  faCommentDots
} from '@fortawesome/free-solid-svg-icons';

/**
 * Dane technologii
 */
const technologiesData = [
  {
    icon: faLaptopCode,
    title: "Aplikacje webowe",
    description: "Tworzymy wydajne i responsywne aplikacje internetowe, które doskonale działają na wszystkich urządzeniach.",
    url: "/oferta/aplikacje-webowe"
  },
  {
    icon: faDesktop,
    title: "Aplikacje desktop",
    description: "Tworzymy wieloplatformowe aplikacje desktopowe dla Windows, macOS i Linux, łączące wydajność z intuicyjnością.",
    url: "/oferta/aplikacje-desktop"
  },
  {
    icon: faServer,
    title: "Mikroserwisy i integratory",
    description: "Projektujemy skalowalne architektury oparte na mikroserwisach, tworzymy wydajne i bezpieczne API oraz integratory umożliwiające sprawną komunikację między systemami.",
    url: "/oferta/mikroserwisy-i-integratory"
  },
  {
    icon: faCloud,
    title: "DevOps & Cloud",
    description: "Wdrażamy automatyzację procesów CI/CD, zarządzanie infrastrukturą i skalowalne rozwiązania chmurowe.",
    url: "/oferta/devops-i-cloud"
  },
  {
    icon: faMobileAlt,
    title: "Aplikacje mobilne",
    description: "Budujemy natywne i cross-platformowe aplikacje mobilne z intuicyjnym interfejsem i wydajną synchronizacją danych.",
    url: "/oferta/aplikacje-mobilne"
  },
  {
    icon: faBolt,
    title: "Aplikacje No-Code / Low-Code",
    description: "Uzupełniająco oferujemy szybkie rozwiązania bez kodu, idealne do prostszych zastosowań i prototypowania.",
    url: "/oferta/aplikacje-nocode-lowcode"
  }
];

/**
 * Komponent sekcji technologii
 * 
 * @returns {JSX.Element} Komponent technologii
 */
export default function Technologies() {
  return (
    <section id="technologie" className="py-20 bg-gray-50">
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        {/* Nagłówek sekcji */}
        <div className="text-center mb-12">
          <h2 className="text-3xl md:text-4xl font-bold mb-4">Technologie, które wykorzystujemy</h2>
          <p className="text-lg text-gray-600 max-w-3xl mx-auto">
            Oferujemy kompleksowe rozwiązania z wykorzystaniem szerokiej gamy technologii dostosowanych do Twoich potrzeb
          </p>
        </div>

        {/* Wszystkie technologie w jednym miejscu */}
        <TechStack />

        {/* Jak dobieramy technologie */}
        <div className="bg-white rounded-lg shadow-md p-8 mb-12">
          <h3 className="text-2xl font-bold mb-6 text-center">Jak dobieramy technologie?</h3>

          <div className="grid grid-cols-1 md:grid-cols-3 gap-6">
            <TechnologySelectionItem 
              icon={faTasks}
              title="Analiza wymagań"
              description="Dokładnie badamy potrzeby biznesowe, skalę projektu i oczekiwania co do rozwoju w przyszłości."
            />

            <TechnologySelectionItem 
              icon={faBalanceScale}
              title="Optymalizacja"
              description="Wybieramy technologie, które zapewniają optymalny stosunek kosztów do korzyści dla Twojego projektu."
            />

            <TechnologySelectionItem 
              icon={faExpandArrowsAlt}
              title="Przyszłościowość"
              description="Stawiamy na rozwiązania, które będą rozwijane i wspierane w długim terminie."
            />
          </div>
        </div>

        {/* CTA z gwiaździstym tłem */}
        <div className="rounded-lg shadow-lg p-8 text-center relative overflow-hidden mb-12">
          {/* Kosmiczne tło */}
          <div className="absolute inset-0 bg-gradient-to-r from-[#312e81] to-[#4c1d95] z-0"></div>
          
          {/* Tło gwiaździste */}
          <StarryBackground />
          
          <div className="relative z-10">
            <h3 className="text-2xl font-bold mb-4 text-white">Potrzebujesz wsparcia w doborze technologii dla swojego projektu?</h3>
            <p className="text-white/90 mb-6 max-w-3xl mx-auto">
              Nasz zespół ekspertów jest gotów pomóc Ci w wyborze optymalnych rozwiązań technologicznych, które spełnią Twoje wymagania biznesowe.
            </p>
            <div className="flex flex-wrap justify-center gap-4">
              <Link href="#kontakt" className="btn-primary">
                <FontAwesomeIcon icon={faCommentDots} className="h-5 w-5 mr-2" />
                Bezpłatna konsultacja
              </Link>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

/**
 * Komponent elementu zasad doboru technologii
 * 
 * @param {Object} props
 * @param {import('@fortawesome/fontawesome-svg-core').IconDefinition} props.icon - Ikona
 * @param {string} props.title - Tytuł
 * @param {string} props.description - Opis
 * @returns {JSX.Element} Element zasad doboru technologii
 */
function TechnologySelectionItem({ icon, title, description }) {
  return (
    <div className="flex flex-col items-center text-center">
      <div className="w-16 h-16 rounded-full bg-purple-600/10 flex items-center justify-center text-purple-600 mb-4">
        <FontAwesomeIcon icon={icon} className="text-2xl" />
      </div>
      <h4 className="font-bold text-lg mb-2">{title}</h4>
      <p className="text-gray-600">
        {description}
      </p>
    </div>
  );
}