import React from 'react';
import Link from 'next/link';
import FontAwesomeIcon from "@/components/common/FontAwesomeIcon";
import StarryBackground from '@/components/common/StarryBackground';
import {
  faSearch,
  faPencilRuler,
  faCode,
  faVial,
  faRocket,
  faHeadset,
  faSyncAlt,
  faShieldAlt,
  faUsers,
  faCalendarAlt,
  faCheckCircle,
  faCommentDots
} from '@fortawesome/free-solid-svg-icons';

/**
 * Dane kroków procesu
 */
const processSteps = [
  { 
    number: 1, 
    title: "Analiza", 
    description: "Określamy cele, zbieramy wymagania i definiujemy zakres projektu.", 
    icon: faSearch,
    details: "Na tym etapie przeprowadzamy dokładny wywiad, analizujemy konkurencję oraz tworzymy specyfikację funkcjonalną. Wspólnie ustalamy priorytety i kamienie milowe projektu.",
    benefits: [
      "Dogłębne zrozumienie potrzeb biznesowych",
      "Precyzyjnie określony zakres i cele projektu",
      "Identyfikacja potencjalnych ryzyk i wyzwań"
    ],
    duration: "1-2 tygodnie",
    durationDescription: "W zależności od złożoności projektu.",
    result: "Szczegółowa specyfikacja wymagań i propozycja rozwiązania z wyceną czasowo-kosztową.",
    importance: "Dokładna analiza potrzeb pozwala na precyzyjne dopasowanie rozwiązania, oszczędzając czas i zasoby w późniejszych etapach projektu.",
    actions: ["Zbieramy wymagania biznesowe", "Analizujemy procesy", "Definiujemy cele projektu"]
  },
  { 
    number: 2, 
    title: "Projektowanie", 
    description: "Tworzymy architekturę systemu i prototypy interfejsu.", 
    icon: faPencilRuler,
    details: "Projektujemy intuicyjne interfejsy użytkownika zgodne z najnowszymi standardami UX. Tworzymy interaktywne prototypy i makiety do wczesnej walidacji pomysłów.",
    benefits: [
      "Optymalna architektura techniczna systemu",
      "Intuicyjne i przyjazne interfejsy użytkownika",
      "Możliwość wczesnej weryfikacji rozwiązania"
    ],
    duration: "2-3 tygodnie",
    durationDescription: "Czas zależy od złożoności interfejsu.",
    result: "Interaktywne prototypy, diagramy architektury systemu i dokumentacja techniczna.",
    importance: "Dobrze zaprojektowany interfejs i architektura to fundament udanej aplikacji, który redukuje koszty zmian w późniejszych etapach o 70%.",
    actions: ["Tworzymy prototypy interfejsu", "Projektujemy architekturę systemu", "Wybieramy technologie"]
  },
  { 
    number: 3, 
    title: "Rozwój", 
    description: "Implementujemy rozwiązanie w iteracyjnych cyklach.", 
    icon: faCode,
    details: "Wykorzystujemy nowoczesne technologie i najlepsze praktyki programistyczne. Kod jest tworzony zgodnie z zasadami czystego kodu, co ułatwia jego późniejszy rozwój i utrzymanie.",
    benefits: [
      "Skalowalny i wydajny kod źródłowy",
      "Regularne dostarczanie działających funkcjonalności",
      "Bieżąca kontrola postępów i jakości"
    ],
    duration: "Podejście etapowe",
    durationDescription: "Czas rozwoju dostosowany do zakresu projektu i podzielony na etapy dostarczające konkretną wartość biznesową.",
    result: "Działająca aplikacja webowa z pełną funkcjonalnością, gotowa do testów.",
    importance: "Pracujemy w 2-tygodniowych sprintach z regularnymi prezentacjami postępów. Takie iteracyjne podejście pozwala na szybkie reagowanie na zmieniające się potrzeby.",
    actions: ["Implementujemy frontend i backend", "Integrujemy z zewnętrznymi systemami", "Przeprowadzamy regularne demo"]
  },
  { 
    number: 4, 
    title: "Testowanie", 
    description: "Przeprowadzamy kompleksowe testy i weryfikację jakości.", 
    icon: faVial,
    details: "Weryfikujemy działanie systemu na wszystkich poziomach: testy jednostkowe, integracyjne, wydajnościowe i użyteczności. Zapewniamy najwyższą jakość dzięki automatyzacji procesów testowych.",
    benefits: [
      "Eliminacja błędów i problemów technicznych",
      "Optymalizacja wydajności i bezpieczeństwa",
      "Walidacja doświadczeń użytkownika"
    ],
    duration: "Podejście etapowe",
    durationDescription: "Dokładne testowanie zapewnia wysoką jakość.",
    result: "Stabilna, bezpieczna i zoptymalizowana aplikacja gotowa do wdrożenia.",
    importance: "Gruntowne testowanie eliminuje 95% potencjalnych problemów przed wdrożeniem, co znacząco redukuje koszty utrzymania w przyszłości.",
    actions: ["Testy funkcjonalne i wydajnościowe", "Audyt bezpieczeństwa", "Optymalizacja wydajności"]
  },
  { 
    number: 5, 
    title: "Wdrożenie", 
    description: "Przeprowadzamy bezpieczne i kontrolowane wdrożenie.", 
    icon: faRocket,
    details: "Wdrażamy system w środowisku produkcyjnym z zachowaniem najwyższych standardów bezpieczeństwa. Zapewniamy płynną migrację danych i ciągłość działania procesów biznesowych.",
    benefits: [
      "Bezpieczna i bezproblemowa implementacja",
      "Minimalizacja przestojów i zakłóceń biznesowych",
      "Płynne przejście ze starego na nowy system"
    ],
    duration: "1-2 tygodnie",
    durationDescription: "Zależy od złożoności migracji danych.",
    result: "Działająca aplikacja w środowisku produkcyjnym, gotowa do użycia przez użytkowników końcowych.",
    importance: "Bezproblemowe wdrożenie zapewnia płynne przejście do korzystania z nowej aplikacji bez zakłóceń w działalności biznesowej.",
    actions: ["Konfiguracja środowiska produkcyjnego", "Migracja danych", "Wdrożenie aplikacji"]
  },
  { 
    number: 6, 
    title: "Wsparcie", 
    description: "Zapewniamy stałe wsparcie i dalszy rozwój systemu.", 
    icon: faHeadset,
    details: "Oferujemy szkolenia, pomoc techniczną oraz stały monitoring i utrzymanie aplikacji. Regularnie aktualizujemy system o nowe funkcje zgodnie z Twoimi potrzebami.",
    benefits: [
      "Szybka reakcja na pojawiające się problemy",
      "Ciągła optymalizacja i rozwój systemu",
      "Wsparcie techniczne dla użytkowników"
    ],
    duration: "Ciągłe wsparcie",
    durationDescription: "Zgodnie z wybranym pakietem SLA.",
    result: "Aplikacja stale rozwijana i dostosowywana do zmieniających się potrzeb biznesowych.",
    importance: "Ciągłe wsparcie i rozwój zapewniają, że aplikacja pozostaje aktualna, bezpieczna i dostosowana do zmieniających się potrzeb biznesowych przez cały okres jej użytkowania.",
    actions: ["Szkolenia użytkowników", "Wsparcie techniczne", "Rozwój nowych funkcji"]
  },
];

/**
 * Dane metodologii
 */
const methodologies = [
  {
    icon: faSyncAlt,
    title: "Zwinne podejście",
    description: "Elastyczne zarządzanie projektem z regularnymi prezentacjami postępów.",
  },
  {
    icon: faShieldAlt,
    title: "Gwarancja jakości",
    description: "Automatyczne testy i kontrola jakości na każdym etapie procesu.",
  },
  {
    icon: faUsers,
    title: "Transparentność",
    description: "Regularna komunikacja i pełen dostęp do statusu projektu.",
  }
];

/**
 * Komponent sekcji opisującej proces pracy
 * 
 * @returns {JSX.Element} Komponent procesu
 */
export default function Process() {
  return (
    <section id="proces" className="py-20 bg-gray-50">
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        {/* Nagłówek sekcji */}
        <div className="text-center mb-16">
          <h2 className="text-3xl md:text-4xl font-bold mb-4">Jak realizujemy projekty?</h2>
          <p className="text-lg text-gray-600 max-w-3xl mx-auto">
            Nasz sprawdzony proces zapewnia wysoką jakość i terminowość na każdym etapie współpracy
          </p>
        </div>

        {/* Kroki procesu z osią czasu */}
        <div className="mb-20 relative">
          {/* Centralna oś czasu - początek poniżej pierwszej ikony */}
          <div className="hidden lg:block absolute left-[30px] top-[60px] bottom-0 w-[1px] bg-purple-200"></div>
          
          {/* Kroki procesu */}
          {processSteps.map((step) => (
            <ProcessStep key={step.number} step={step} />
          ))}
        </div>

        {/* Karty metodologii */}
        <div className="grid grid-cols-1 md:grid-cols-3 gap-8 mb-16">
          {methodologies.map((methodology, index) => (
            <MethodologyCard key={index} methodology={methodology} />
          ))}
        </div>

        {/* CTA z kosmicznym tłem */}
        <div className="rounded-lg shadow-lg p-10 text-center relative overflow-hidden">
          {/* Kosmiczne tło */}
          <div className="absolute inset-0 bg-gradient-to-r from-[#312e81] to-[#4c1d95] z-0"></div>
          
          {/* Tło gwiaździste */}
          <StarryBackground />
          
          <div className="relative z-10">
            <h3 className="text-2xl font-bold mb-4 text-white">Gotowy na rozpoczęcie projektu?</h3>
            <p className="text-xl text-white/90 mb-8 max-w-2xl mx-auto">
              Skorzystaj z naszego doświadczenia i profesjonalnego procesu, który zapewnia 
              wysoką jakość i terminowość realizacji.
            </p>
            <Link href="#kontakt" className="btn-primary">
              <FontAwesomeIcon icon={faCommentDots} className="h-5 w-5 mr-2" />
              Bezpłatna konsultacja
            </Link>
          </div>
        </div>
      </div>
    </section>
  );
}

/**
 * Komponent kroku procesu
 * 
 * @param {Object} props
 * @param {Object} props.step - Dane kroku procesu
 * @returns {JSX.Element} Krok procesu
 */
function ProcessStep({ step }) {
  const { number, title, duration, durationDescription, result, importance, actions } = step;
  
  return (
    <div className="relative pt-8 pb-8 first:pt-0 last:pb-0">
      <div className="flex">
        {/* Numer kroku zamiast ikony */}
        <div className="hidden lg:block relative">
          <div className="absolute left-0 top-0 w-[60px] h-[60px] rounded-full bg-purple-700 text-white flex items-center justify-center z-10 text-2xl font-bold">
            {number}
          </div>
        </div>
        
        {/* Treść */}
        <div className="lg:ml-[85px] w-full">
          <div className="bg-white rounded-lg shadow-md p-6">
            <div className="flex items-center mb-5">
              {/* Numer kroku w widoku mobilnym */}
              <div className="lg:hidden w-14 h-14 rounded-full bg-purple-700 text-white flex items-center justify-center mr-4 text-2xl font-bold">
                {number}
              </div>
              
              <div>
                <h3 className="text-2xl font-bold text-gray-900">{title}</h3>
              </div>
            </div>
            
            {/* Treść z 3 kolumnami */}
            <div className="grid grid-cols-1 md:grid-cols-3 gap-6 mb-6">
              {/* Kolumna "Co robimy" */}
              <div>
                <h4 className="font-semibold text-lg mb-3">Co robimy</h4>
                <ul className="space-y-2">
                  {actions.map((action, i) => (
                    <li key={i} className="flex items-start">
                      <svg className="h-5 w-5 text-purple-700 flex-shrink-0 mr-2 mt-0.5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M5 13l4 4L19 7" />
                      </svg>
                      <span className="text-gray-700">{action}</span>
                    </li>
                  ))}
                </ul>
              </div>
              
              {/* Kolumna "Czas trwania" */}
              <div>
                <h4 className="font-semibold text-lg mb-3">Czas trwania</h4>
                <div className="flex items-center text-purple-700 font-medium mb-2">
                  <div className="w-5 h-5 rounded-full bg-purple-700 text-white flex items-center justify-center mr-2 text-xs">t</div>
                  <span>{duration}</span>
                </div>
                <p className="text-gray-600 text-sm">
                  {durationDescription}
                </p>
              </div>
              
              {/* Kolumna "Rezultat" */}
              <div>
                <h4 className="font-semibold text-lg mb-3">Rezultat</h4>
                <p className="text-gray-700">
                  {result}
                </p>
              </div>
            </div>
            
            {/* Sekcja "Dlaczego to ważne" */}
            <div className="bg-purple-50 rounded-lg p-4">
              <h4 className="font-semibold mb-2 flex items-center">
                <div className="w-5 h-5 rounded-full bg-purple-700 text-white flex items-center justify-center mr-2 text-xs">i</div>
                <span>Dlaczego to ważne?</span>
              </h4>
              <p className="text-gray-700">
                {importance}
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

/**
 * Komponent karty metodologii
 * 
 * @param {Object} props
 * @param {Object} props.methodology - Dane metodologii
 * @returns {JSX.Element} Karta metodologii
 */
function MethodologyCard({ methodology }) {
  const { icon, title, description } = methodology;
  
  return (
    <div className="bg-white rounded-lg p-8 shadow-md text-center">
      <div className="w-16 h-16 bg-purple-100 rounded-full flex items-center justify-center text-purple-700 mb-6 mx-auto">
        <FontAwesomeIcon icon={icon} className="text-2xl" />
      </div>
      <h3 className="text-xl font-bold mb-4">{title}</h3>
      <p className="text-gray-600 text-lg">
        {description}
      </p>
    </div>
  );
}