import React from 'react';
import StarryBackground from '@/components/common/StarryBackground';

/**
 * Dane statystyk
 */
const statsData = [
  { value: '30+', label: 'Zrealizowanych projektów' },
  { value: '100%', label: 'Zadowolonych klientów' },
  { value: '16+', label: 'Ekspertów technologicznych' },
  { value: '7+', label: 'Lat doświadczenia' }
];

/**
 * Komponent sekcji statystyk z animowanym tłem gwiezdnym
 * 
 * @returns {JSX.Element} Komponent statystyk
 */
export default function Stats() {
  return (
    <section className="relative py-16 md:py-24 bg-gradient-to-r from-[#312e81] to-[#4c1d95] overflow-hidden">
      {/* Tło gwiaździste */}
      <StarryBackground />
      
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 relative z-10">
        <div className="grid grid-cols-2 lg:grid-cols-4 gap-8 text-center">
          {statsData.map((stat, index) => (
            <StatItem key={index} value={stat.value} label={stat.label} />
          ))}
        </div>
      </div>
    </section>
  );
}

/**
 * Komponent pojedynczej statystyki
 * 
 * @param {Object} props
 * @param {string} props.value - Wartość statystyki
 * @param {string} props.label - Etykieta statystyki
 * @returns {JSX.Element} Komponent statystyki
 */
function StatItem({ value, label }) {
  return (
    <div className="bg-white/5 backdrop-blur-sm rounded-xl p-6 transform transition-all duration-300 hover:scale-105 hover:bg-white/10">
      <p className="text-5xl font-bold text-white mb-3">{value}</p>
      <p className="text-white/80">{label}</p>
    </div>
  );
}