import Image from 'next/image'
import FontAwesomeIcon from "@/components/common/FontAwesomeIcon";
import { faAward, faClock } from '@fortawesome/free-solid-svg-icons'

/**
 * Komponent karty studium przypadku
 * 
 * @param {Object} props
 * @param {Object} props.caseStudy - Dane studium przypadku
 * @param {string} props.caseStudy.description - Opis projektu
 * @param {string} props.caseStudy.logo - Ścieżka do logo klienta
 * @param {string} props.caseStudy.category - Kategoria projektu
 * @param {string} props.caseStudy.tag - Tagi technologii oddzielone przecinkami
 * @param {string} props.caseStudy.timeframe - Czas trwania projektu (opcjonalnie)
 * @param {string} props.caseStudy.results - Wyniki projektu (opcjonalnie)
 */
export default function CaseStudyCard({ caseStudy }) {
    const { description, logo, category, tag, timeframe, results } = caseStudy
    
    // Parse the tag string into individual technology tags if it contains commas
    const technologies = tag ? tag.split(',').map(tech => tech.trim()).filter(tech => tech) : []
    
    return (
        <div className="bg-white rounded-lg border border-gray-100 shadow-md hover:shadow-xl transition-all duration-300 flex flex-col relative overflow-hidden h-full">
            {/* Gradient accent bar - matches the solutions section */}
            <div className="h-2 w-full bg-gradient-to-r from-purple-600 to-purple-800 absolute top-0 left-0 right-0 z-10"></div>
            
            {/* Decorative elements */}
            <div className="absolute top-0 right-0 w-40 h-40 bg-purple-600/5 rounded-full transform translate-x-20 -translate-y-20"></div>
            <div className="absolute bottom-0 left-0 w-32 h-32 bg-purple-600/5 rounded-full transform -translate-x-16 translate-y-16"></div>
            
            {/* Main content area */}
            <div className="p-6 pt-8 flex-1 relative z-0">
                {/* Category and logo */}
                <div className="flex justify-between items-center mb-5">
                    {/* Category with title styling matching other sections */}
                    {category && (
                        <h3 className="text-xl font-bold text-gray-800">{category}</h3>
                    )}
                    
                    {/* Logo without grayscale effect */}
                    {logo && (
                        <div className="h-10 flex-shrink-0 bg-gray-50/50 p-1 rounded-lg">
                            <Image
                                src={logo}
                                alt={`Logo klienta`}
                                width={80}
                                height={32}
                                className="h-full w-auto max-w-[80px] object-contain opacity-100 transition-all duration-300"
                            />
                        </div>
                    )}
                </div>
                
                {/* Description - without truncation */}
                <div className="mb-5">
                    <p className="text-gray-600">{description}</p>
                </div>
                
                {/* Key metrics with styling inspired by solutions section */}
                <div className="flex flex-wrap gap-3 mt-auto">
                    {timeframe && (
                        <div className="bg-gray-50 py-2 px-3.5 rounded-lg text-sm flex items-center">
                            <div className="w-7 h-7 bg-purple-600/10 rounded-full flex items-center justify-center mr-2">
                                <FontAwesomeIcon icon={faClock} className="w-3.5 h-3.5 text-purple-600" />
                            </div>
                            <span className="text-gray-700">{timeframe}</span>
                        </div>
                    )}
                    
                    {results && (
                        <div className="bg-purple-600/10 py-2 px-3.5 rounded-lg text-sm flex items-center">
                            <div className="w-7 h-7 bg-purple-600/20 rounded-full flex items-center justify-center mr-2">
                                <FontAwesomeIcon icon={faAward} className="w-3.5 h-3.5 text-purple-600" />
                            </div>
                            <span className="text-purple-700 font-medium">{results}</span>
                        </div>
                    )}
                </div>
            </div>
            
            {/* Technologies section with styling matching the technology section */}
            <div className="p-6 bg-gray-50 relative z-0">
                <p className="text-sm font-medium text-gray-700 mb-3">Technologie:</p>
                <div className="h-20 overflow-y-auto pr-1 scrollbar-thin scrollbar-thumb-purple-600/20 scrollbar-track-gray-100">
                    <div className="flex flex-wrap gap-2">
                        {technologies.map((tech, index) => (
                            <span 
                                key={index} 
                                className="text-xs py-1.5 px-3 bg-white rounded-md text-gray-700 border border-gray-100 hover:bg-purple-600/5 hover:text-purple-700 transition-all duration-200"
                            >
                                {tech}
                            </span>
                        ))}
                    </div>
                </div>
            </div>
        </div>
    )
}