import { useState, useMemo } from "react";
import Link from 'next/link';
import CaseStudyCard from '@/components/case-studies/CaseStudyCard';
import FontAwesomeIcon from "@/components/common/FontAwesomeIcon";
import PrimaryButton from "@/components/common/PrimaryButton";
import StarryBackground from '@/components/common/StarryBackground';
import { faCode, faCommentDots, faPlus, faMinus } from '@fortawesome/free-solid-svg-icons';

// Dodanie ID do każdego case study
const caseStudiesData = [
  {
    id: 1,
    title: "timecamp.com",
    description: "Rozwijanie i utrzymywanie wieloplatformowej aplikacji desktopowej działającej na systemach Windows, Linux i macOS.",
    image: "https://www.inprocorp.com/globalassets/color--finish-images/standard-solid/greystone.jpg?width=500&height=500&mode=crop",
    logo: "/timecamp.png",
    category: "Aplikacja desktop",
    tag: "C++, wxWidgets, Windows API, Docker"
  },
  {
    id: 2,
    title: "timecamp.com",
    description: "Rozwijanie i utrzymywanie wieloplatformowej aplikacji desktopowej działającej na systemach Windows, Linux i macOS.",
    image: "https://www.inprocorp.com/globalassets/color--finish-images/standard-solid/greystone.jpg?width=500&height=500&mode=crop",
    logo: "/timecamp.png",
    category: "Aplikacja desktop",
    tag: "TypeScript, Electron.js, OCR, AI, Docker"
  },
  {
    id: 3,
    title: "timecamp.com",
    description: "Opracowanie i rozwój mikroserwisów oraz integratorów wspierających komunikację między systemami.",
    image: "https://www.inprocorp.com/globalassets/color--finish-images/standard-solid/greystone.jpg?width=500&height=500&mode=crop",
    logo: "/timecamp.png",
    category: "Mikroserwisy i integratory",
    tag: "Python, FastAPI, PostgreSQL, Docker"
  },
  {
    id: 4,
    title: "timecamp.com",
    description: "Opracowanie i utrzymanie integracji typu marketplace dla platformy monday.com.",
    image: "https://www.inprocorp.com/globalassets/color--finish-images/standard-solid/greystone.jpg?width=500&height=500&mode=crop",
    logo: "/timecamp.png",
    category: "Aplikacja webowa",
    tag: "TypeScript, React, Tailwind CSS, Docker"
  },
  {
    id: 5,
    title: "dropui.com",
    description: "Opracowanie i rozwój aplikacji w obszarze no-code/low-code z wykorzystaniem technologii AI, zdolnej do przetwarzania setek tysięcy zapytań API na godzinę.",
    image: "https://www.inprocorp.com/globalassets/color--finish-images/standard-solid/greystone.jpg?width=500&height=500&mode=crop",
    logo: "/dropui.png",
    category: "Aplikacja webowa",
    tag: "Python, Django, TypeScript, React, Vite, Tailwind CSS, Redis, RabbitMQ, PostgreSQL, Docker"
  },
  {
    id: 6,
    title: "dropui.com",
    description: "Opracowanie i utrzymanie integracji typu marketplace dla platformy idosell.com.",
    image: "https://www.inprocorp.com/globalassets/color--finish-images/standard-solid/greystone.jpg?width=500&height=500&mode=crop",
    logo: "/dropui.png",
    category: "Aplikacja webowa",
    tag: "PHP, PostgreSQL, Docker"
  },
  {
    id: 7,
    title: "dropui.com",
    description: "Opracowanie i utrzymanie środowiska z wykorzystaniem narzędzi DevOps oraz rozwiązań chmurowych.",
    image: "https://www.inprocorp.com/globalassets/color--finish-images/standard-solid/greystone.jpg?width=500&height=500&mode=crop",
    logo: "/dropui.png",
    category: "DevOps & Cloud",
    tag: "Docker, CI/CD, Kubernetes, OpenShift, Proxmox, Terraform, ArgoCD, Prometheus, Grafana"
  },
  {
    id: 8,
    title: "skinwallet.com",
    description: "Dostarczenie zespołu programistycznego wyspecjalizowanego w React i Node.js.",
    image: "https://www.inprocorp.com/globalassets/color--finish-images/standard-solid/greystone.jpg?width=500&height=500&mode=crop",
    logo: "/skinwallet.png",
    category: "Outsourcing",
    tag: "TypeScript, Node.js, React, Tailwind CSS, Docker"
  },
  {
    id: 9,
    title: "imker.pl",
    description: "Opracowanie i rozwój aplikacji klasy OMS (Order Management System) zdolnej do przetwarzania dziesiątek tysięcy zadań na godzinę.",
    image: "https://www.inprocorp.com/globalassets/color--finish-images/standard-solid/greystone.jpg?width=500&height=500&mode=crop",
    logo: "/imker.webp",
    category: "Aplikacja webowa",
    tag: "Python, Django, Flask, Bootstrap, Redis, RabbitMQ, PostgreSQL, Docker"
  },
  {
    id: 10,
    title: "imker.pl",
    description: "Opracowanie i rozwój forum dla twórców internetowych.",
    image: "https://www.inprocorp.com/globalassets/color--finish-images/standard-solid/greystone.jpg?width=500&height=500&mode=crop",
    logo: "/imker.webp",
    category: "Aplikacja webowa",
    tag: "Python, Django, Tailwind CSS, PostgreSQL, Docker"
  },
  {
    id: 11,
    title: "imker.pl",
    description: "Opracowanie i utrzymanie środowiska z wykorzystaniem narzędzi DevOps oraz rozwiązań chmurowych.",
    image: "https://www.inprocorp.com/globalassets/color--finish-images/standard-solid/greystone.jpg?width=500&height=500&mode=crop",
    logo: "/imker.webp",
    category: "DevOps & Cloud",
    tag: "Docker, CI/CD, Kubernetes, OpenShift, Proxmox, Terraform, ArgoCD, Prometheus, Grafana"
  },
  {
    id: 12,
    title: "btc.com.pl",
    description: "Opracowanie i rozwój mikroserwisów oraz integratorów wspierających komunikację między systemami.",
    image: "https://www.inprocorp.com/globalassets/color--finish-images/standard-solid/greystone.jpg?width=500&height=500&mode=crop",
    logo: "/btc.png",
    category: "Mikroserwisy i integratory",
    tag: "Python, FastAPI, Redis, RabbitMQ, PostgreSQL, Docker"
  },
  {
    id: 13,
    title: "simba erp",
    description: "Opracowanie i rozwój lekkiego systemu klasy ERP dla sektora MŚP.",
    image: "https://www.inprocorp.com/globalassets/color--finish-images/standard-solid/greystone.jpg?width=500&height=500&mode=crop",
    logo: "/eon.png",
    category: "Aplikacja webowa",
    tag: "Python, Django, Bootstrap, Redis, RabbitMQ, PostgreSQL, Docker, OCR"
  },
  {
    id: 13,
    title: "simba erp",
    description: "Opracowanie i rozwój lekkiego systemu klasy ERP dla sektora MŚP.",
    image: "https://www.inprocorp.com/globalassets/color--finish-images/standard-solid/greystone.jpg?width=500&height=500&mode=crop",
    logo: "/eon.png",
    category: "Aplikacja webowa",
    tag: "Python, Django, Bootstrap, Redis, RabbitMQ, PostgreSQL, Docker, OCR"
  },
  {
    id: 14,
    title: "simba erp",
    description: "Opracowanie i utrzymanie środowiska z wykorzystaniem narzędzi DevOps oraz rozwiązań chmurowych.",
    image: "https://www.inprocorp.com/globalassets/color--finish-images/standard-solid/greystone.jpg?width=500&height=500&mode=crop",
    logo: "/eon.png",
    category: "DevOps & Cloud",
    tag: "Docker, CI/CD, Kubernetes, OpenShift, Proxmox, Terraform, ArgoCD, Prometheus, Grafana"
  },
  {
    id: 15,
    title: "dreamlink.pl",
    description: "Dostarczenie zespołu programistycznego wyspecjalizowanego w React i Node.js.",
    image: "https://www.inprocorp.com/globalassets/color--finish-images/standard-solid/greystone.jpg?width=500&height=500&mode=crop",
    logo: "/dreamlink.svg",
    category: "Outsourcing",
    tag: "TypeScript, Node.js, React, Tailwind CSS, Docker"
  },
  {
    id: 16,
    title: "fffrree.com",
    description: "Opracowanie i rozwój platformy e-commerce B2B przeznaczonej na wiele rynków europejskich.",
    image: "https://www.inprocorp.com/globalassets/color--finish-images/standard-solid/greystone.jpg?width=500&height=500&mode=crop",
    logo: "/fffrree.png",
    category: "Aplikacja webowa",
    tag: "Python, Django, Bootstrap, Docker"
  },
  {
    id: 17,
    title: "fffrree.com",
    description: "Opracowanie i utrzymanie środowiska z wykorzystaniem narzędzi DevOps oraz rozwiązań chmurowych.",
    image: "https://www.inprocorp.com/globalassets/color--finish-images/standard-solid/greystone.jpg?width=500&height=500&mode=crop",
    logo: "/fffrree.png",
    category: "DevOps & Cloud",
    tag: "Docker, CI/CD, Kubernetes, OpenShift, Proxmox, Terraform, ArgoCD, Prometheus, Grafana"
  },
  {
    id: 18,
    title: "iso-trade.eu",
    description: "Opracowanie i rozwój mikroserwisów oraz integratorów wspierających komunikację między systemami.",
    image: "https://www.inprocorp.com/globalassets/color--finish-images/standard-solid/greystone.jpg?width=500&height=500&mode=crop",
    logo: "/isotrade.png",
    category: "Mikroserwisy i integratory",
    tag: "Python, FastAPI, Redis, RabbitMQ, PostgreSQL, Docker"
  },
  {
    id: 19,
    title: "iso-trade.eu",
    description: "Opracowanie i rozwój platformy do generowania kodów QR.",
    image: "https://www.inprocorp.com/globalassets/color--finish-images/standard-solid/greystone.jpg?width=500&height=500&mode=crop",
    logo: "/isotrade.png",
    category: "Aplikacja webowa",
    tag: "Python, Django, PostgreSQL, Docker"
  },
  {
    id: 20,
    title: "iso-trade.eu",
    description: "Opracowanie i utrzymanie środowiska z wykorzystaniem narzędzi DevOps oraz rozwiązań chmurowych.",
    image: "https://www.inprocorp.com/globalassets/color--finish-images/standard-solid/greystone.jpg?width=500&height=500&mode=crop",
    logo: "/isotrade.png",
    category: "DevOps & Cloud",
    tag: "Docker, CI/CD, Kubernetes, OpenShift, Proxmox, Terraform, ArgoCD, Prometheus, Grafana"
  },
  {
    id: 21,
    title: "easyprotect.pl",
    description: "Opracowanie i utrzymanie integracji typu marketplace dla platformy idosell.com.",
    image: "https://www.inprocorp.com/globalassets/color--finish-images/standard-solid/greystone.jpg?width=500&height=500&mode=crop",
    logo: "/easyprotect.png",
    category: "Aplikacja webowa",
    tag: "PHP, Docker"
  },
  {
    id: 22,
    title: "uniwersytet szczeciński",
    description: "Opracowanie i rozwój platformy wspierającej studentów w nauce programowania w języku Python.",
    image: "https://www.inprocorp.com/globalassets/color--finish-images/standard-solid/greystone.jpg?width=500&height=500&mode=crop",
    logo: "/us.png",
    category: "Aplikacja webowa",
    tag: "Python, Django, React, Docker"
  },
  {
    id: 23,
    title: "cateromarket.pl",
    description: "Opracowanie i przepisanie aplikacji z PHP na Pythona wraz z migracją bazy danych z MySQL do PostgreSQL.",
    image: "https://www.inprocorp.com/globalassets/color--finish-images/standard-solid/greystone.jpg?width=500&height=500&mode=crop",
    logo: "/cateromarket.png",
    category: "Aplikacja webowa",
    tag: "Python, Django, Bootstrap, PostgreSQL, Docker"
  },
  {
    id: 24,
    title: "cateromarket.pl",
    description: "Opracowanie i utrzymanie środowiska z wykorzystaniem narzędzi DevOps oraz rozwiązań chmurowych.",
    image: "https://www.inprocorp.com/globalassets/color--finish-images/standard-solid/greystone.jpg?width=500&height=500&mode=crop",
    logo: "/cateromarket.png",
    category: "DevOps & Cloud",
    tag: "Docker, CI/CD, Kubernetes, OpenShift, Proxmox, Terraform, ArgoCD, Prometheus, Grafana"
  },
];

/**
 * Komponent wyświetlający sekcję studiów przypadku
 * 
 * @param {Object} props
 * @param {Array} props.selectedIds - Tablica ID case studies do wyświetlenia
 * @param {boolean} props.showAll - Czy pokazać wszystkie case studies
 * @param {number} props.initialItemsToShow - Początkowa liczba wyświetlanych case studies
 * @param {string} props.sectionId - ID sekcji na stronie
 * @param {boolean} props.showMoreButton - Czy pokazać przycisk "Pokaż więcej"
 * @param {boolean} props.showInfoBox - Czy pokazać box informacyjny
 * @param {string} props.title - Tytuł sekcji
 * @param {string} props.subtitle - Podtytuł sekcji
 * @param {boolean} props.hidden - Czy sekcja jest ukryta
 */
export default function CaseStudies(props) { 
  // Obsługa props - sprawdzamy, czy są przekazane bezpośrednio czy w obiekcie caseStudies
  const {
    // Jeśli props są przekazane jako caseStudies: { ... }
    caseStudies = {},
    
    // Wartości domyślne lub przekazane bezpośrednio
    selectedIds = props.selectedIds || [],
    showAll = props.showAll || false,
    initialItemsToShow = props.initialItemsToShow || 6, // Domyślnie pokazuje 6 projektów, jak w oryginalnej wersji
    sectionId = props.sectionId || "case-studies",
    showMoreButton = props.showMoreButton !== undefined ? props.showMoreButton : true,
    showInfoBox = props.showInfoBox !== undefined ? props.showInfoBox : true,
    title = props.title || "Sprawdzone rozwiązania w praktyce",
    subtitle = props.subtitle || "Tak wspieramy naszych klientów w rozwiązywaniu problemów opisanych powyżej",
    hidden = props.hidden || false
  } = props;

  // Sprawdzamy, czy hidden zostało przekazane w obiekcie caseStudies
  const isHidden = hidden || caseStudies.hidden;

  // Jeśli hidden jest true, nie renderuj niczego
  if (isHidden) {
    return null;
  }

  // Używamy wartości z obiektu caseStudies, jeśli są dostępne
  const actualSelectedIds = caseStudies.selectedIds || selectedIds;
  const actualShowInfoBox = caseStudies.showInfoBox !== undefined ? caseStudies.showInfoBox : showInfoBox;
  
  const [showAllCases, setShowAllCases] = useState(false);

  // Filtruj case studies na podstawie przekazanych props
  const filteredCaseStudies = useMemo(() => {
    if (showAll) {
      return caseStudiesData; // Pokaż wszystkie
    } else if (actualSelectedIds && actualSelectedIds.length > 0) {
      return caseStudiesData.filter(study => actualSelectedIds.includes(study.id)); // Pokaż tylko wybrane
    } else {
      return caseStudiesData; // Domyślnie pokaż wszystkie, jeśli nie określono filtrów
    }
  }, [showAll, actualSelectedIds]);

  // Decydujemy, ile elementów pokazać
  const casesToShow = showAllCases 
    ? filteredCaseStudies 
    : filteredCaseStudies.slice(0, initialItemsToShow);

  // Funkcja do obsługi przycisku "Pokaż więcej/mniej"
  const toggleShowMore = () => {
    setShowAllCases(prev => !prev);
  };

  return (
    <section id={sectionId} className="py-20 bg-white">
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        {/* Header sekcji */}
        <div className="text-center mb-12">
          <h2 className="text-3xl md:text-4xl font-bold mb-4">{title}</h2>
          <p className="text-lg text-gray-600 max-w-3xl mx-auto">
            {subtitle}
          </p>
        </div>

        {/* Siatka projektów */}
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8 mb-12">
          {casesToShow.map((caseStudy) => (
            <CaseStudyCard key={caseStudy.id} caseStudy={caseStudy} />
          ))}
        </div>

        {/* Przycisk Pokaż więcej - pokazuje się tylko jeśli jest więcej elementów do wyświetlenia */}
        {showMoreButton && filteredCaseStudies.length > initialItemsToShow && (
          <div className="mb-12 flex w-full justify-center">
            <PrimaryButton
              variant="secondary"
              icon={<FontAwesomeIcon icon={showAllCases ? faMinus : faPlus} className="h-5 w-5" />}
              onClick={toggleShowMore}
            >
              {showAllCases ? 'Pokaż mniej' : 'Pokaż więcej'}
            </PrimaryButton>
          </div>
        )}

        {/* Płynne przejście do sekcji Technologie z kosmicznym tłem */}
        {actualShowInfoBox && (
          <div className="rounded-lg shadow-lg p-8 text-center relative overflow-hidden">
            {/* Kosmiczne tło */}
            <div className="absolute inset-0 bg-gradient-to-r from-[#312e81] to-[#4c1d95] z-0"></div>
            
            {/* Tło gwiaździste */}
            <StarryBackground />
            
            <div className="relative z-10">
              <h3 className="text-2xl font-bold text-white mb-4">
                Jak dostarczamy te rozwiązania?
              </h3>
              <p className="text-white/90 mb-6 max-w-3xl mx-auto">
                Wykorzystujemy sprawdzone technologie i metodyki pracy, które pozwalają nam szybko i efektywnie realizować projekty
              </p>
              <div className="flex flex-wrap justify-center gap-4 mb-4">
                <Link href="#kontakt" className="btn-primary">
                  <FontAwesomeIcon icon={faCommentDots} className="h-5 w-5 mr-2" />
                  Bezpłatna konsultacja
                </Link>
              </div>
            </div>
          </div>
        )}
      </div>
    </section>
  );
}