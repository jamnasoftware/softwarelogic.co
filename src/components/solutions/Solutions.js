import React from 'react';
import Link from 'next/link';
import FontAwesomeIcon from "@/components/common/FontAwesomeIcon";
import StarryBackground from '@/components/common/StarryBackground';
import {
  faChartLine,
  faRocket,
  faUsersCog,
  faExpandArrowsAlt,
  faPaintBrush,
  faDigitalTachograph,
  faCalendarAlt,
  faClock,
  faSync,
  faShieldAlt,
  faDatabase,
  faArrowUp,
  faArrowDown,
  faLink,
  faChartPie,
  faCheck,
  faCommentDots
} from '@fortawesome/free-solid-svg-icons';

/**
 * Dane rozwiązań biznesowych
 */
const solutionsData = [
  {
    icon: faChartLine,
    title: "Nieefektywne procesy",
    description: "Transformujemy ręczne, czasochłonne procesy w zautomatyzowane systemy, które działają szybciej, taniej i z mniejszą liczbą błędów.",
    benefit: "Redukcja czasu o 70%",
    benefitIcon: "clock"
  },
  {
    icon: faRocket,
    title: "Potrzeba szybkiego startu",
    description: "Dostarczamy funkcjonalne MVP w 4-6 tygodni, umożliwiając szybką weryfikację pomysłu i pozyskanie pierwszych użytkowników.",
    benefit: "Od pomysłu do produktu w 6 tygodni",
    benefitIcon: "rocket"
  },
  {
    icon: faExpandArrowsAlt,
    title: "Problemy ze skalowaniem",
    description: "Tworzymy systemy, które płynnie obsługują rosnącą liczbę użytkowników i danych, eliminując wąskie gardła wydajności.",
    benefit: "Gotowość na 10x wzrost",
    benefitIcon: "arrows-up-down"
  },
  {
    icon: faPaintBrush,
    title: "Niska konwersja",
    description: "Projektujemy intuicyjne interfejsy, które zwiększają zaangażowanie użytkowników i przekształcają odwiedzających w płacących klientów.",
    benefit: "Wzrost konwersji o 80%",
    benefitIcon: "arrow-up"
  },
  {
    icon: faDigitalTachograph,
    title: "Przestarzałe systemy",
    description: "Modernizujemy legacy systemy, eliminując dług technologiczny i zapewniając zgodność z nowoczesnymi standardami.",
    benefit: "O 40% niższe koszty utrzymania",
    benefitIcon: "arrow-down"
  },
  {
    icon: faSync,
    title: "Trudności integracyjne",
    description: "Łączymy rozproszone systemy w spójny ekosystem, zapewniając płynny przepływ danych między aplikacjami.",
    benefit: "Koniec z silosami informacyjnymi",
    benefitIcon: "link"
  },
  {
    icon: faShieldAlt,
    title: "Zagrożenia bezpieczeństwa",
    description: "Zabezpieczamy krytyczne dane biznesowe i systemy przed wyciekami, włamaniami i innymi zagrożeniami cyberbezpieczeństwa.",
    benefit: "Kompleksowa ochrona danych",
    benefitIcon: "shield"
  },
  {
    icon: faDatabase,
    title: "Nadmiar danych, brak insightów",
    description: "Przekształcamy surowe dane w actionable insights, które wspierają podejmowanie strategicznych decyzji biznesowych.",
    benefit: "Decyzje oparte na danych",
    benefitIcon: "chart-pie"
  },
  {
    icon: faUsersCog,
    title: "Brak zespołu IT",
    description: "Zapewniamy pełen zespół ekspertów technologicznych dostępnych od zaraz, bez kosztownej i długotrwałej rekrutacji.",
    benefit: "Kompletny zespół na żądanie",
    benefitIcon: "users"
  }
];

/**
 * Mapa konwersji nazw ikon na obiekty ikon
 */
const benefitIconMap = {
  'clock': faClock,
  'rocket': faRocket,
  'users': faUsersCog,
  'arrows-up-down': faExpandArrowsAlt,
  'arrow-up': faArrowUp,
  'arrow-down': faArrowDown,
  'link': faLink,
  'shield': faShieldAlt,
  'check': faCheck,
  'chart-pie': faChartPie
};

/**
 * Komponent sekcji rozwiązań biznesowych
 * 
 * @returns {JSX.Element} Komponent rozwiązań
 */
export default function Solutions() {
  return (
    <section id="jak-pomagamy" className="py-20 bg-gray-50">
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        {/* Nagłówek sekcji */}
        <div className="text-center mb-14">
          <h2 className="text-3xl md:text-4xl font-bold mt-2 mb-4">Rozwiązujemy realne problemy biznesowe</h2>
          <p className="text-lg text-gray-600 max-w-3xl mx-auto">
            Tworzymy dedykowane oprogramowanie, które zamienia Twoje wyzwania w przewagę konkurencyjną
          </p>
        </div>
        
        {/* Karty rozwiązań */}
        <div className="grid grid-cols-1 md:grid-cols-3 gap-6 mb-16">
          {solutionsData.map((solution, index) => (
            <SolutionCard key={index} solution={solution} />
          ))}
        </div>
        
        {/* CTA z gwiaździstym tłem */}
        <div className="rounded-lg shadow-lg p-10 text-center relative overflow-hidden">
          {/* Kosmiczne tło */}
          <div className="absolute inset-0 bg-gradient-to-r from-[#312e81] to-[#4c1d95] z-0"></div>
          
          {/* Tło gwiaździste */}
          <StarryBackground />
          
          <div className="relative z-10">
            <h3 className="text-2xl md:text-3xl font-bold mb-4 text-white">Z jakimi wyzwaniami mierzy się Twój biznes?</h3>
            <p className="text-lg text-white/90 mb-8 max-w-2xl mx-auto">
              Porozmawiajmy o tym, jak możemy pomóc Twojej firmie wykorzystać technologię jako przewagę konkurencyjną
            </p>
            <div className="flex flex-col sm:flex-row justify-center gap-4">
              <Link href="#kontakt" className="btn-primary">
                <FontAwesomeIcon icon={faCommentDots} className="h-5 w-5 mr-2" />
                Bezpłatna konsultacja
              </Link>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

/**
 * Styl karty rozwiązania podobny do oferty
 * 
 * @param {Object} props
 * @param {Object} props.solution - Dane rozwiązania
 * @returns {JSX.Element} Karta rozwiązania
 */
function SolutionCard({ solution }) {
  const { icon, title, description, benefit, benefitIcon } = solution;
  
  return (
    <div className="bg-white rounded-lg border border-gray-100 shadow-md hover:shadow-lg transition-all duration-300 flex flex-col h-full overflow-hidden">
      <div className="p-6 flex-1">
        <div className="w-12 h-12 rounded-full bg-purple-600/10 flex items-center justify-center text-purple-600 mb-4">
          <FontAwesomeIcon icon={icon} className="text-xl" />
        </div>
        <h3 className="text-xl font-bold mb-3">{title}</h3>
        <p className="text-gray-600">
          {description}
        </p>
      </div>
      
      <div className="mt-auto border-t border-gray-100 p-4">
        <div className="flex items-center">
          <FontAwesomeIcon 
            icon={benefitIconMap[benefitIcon] || faArrowUp} 
            className="h-4 w-4 text-purple-600 mr-2" 
          />
          <span className="font-medium text-purple-700">{benefit}</span>
        </div>
      </div>
    </div>
  );
}