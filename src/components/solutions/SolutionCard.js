import React from 'react';
import FontAwesomeIcon from "@/components/common/FontAwesomeIcon";
import {
  faArrowUp,
  faArrowDown,
  faClock,
  faCheck,
  faLink,
  faShieldAlt,
  faChartPie,
  faRocket,
  faUsersCog,
  faExpandArrowsAlt
} from '@fortawesome/free-solid-svg-icons';

/**
 * Mapa ikon korzyści do odpowiednich obiektów FontAwesome
 */
const BENEFIT_ICONS = {
  'arrow-up': faArrowUp,
  'arrow-down': faArrowDown,
  'clock': faClock,
  'check': faCheck,
  'link': faLink,
  'shield': faShieldAlt,
  'chart-pie': faChartPie,
  'rocket': faRocket,
  'users': faUsersCog,
  'arrows-up-down': faExpandArrowsAlt
};

/**
 * Komponent karty rozwiązania
 * 
 * @param {Object} props
 * @param {Object} props.solution - Dane rozwiązania
 * @param {import('@fortawesome/fontawesome-svg-core').IconDefinition} props.solution.icon - Ikona główna
 * @param {string} props.solution.title - Tytuł rozwiązania
 * @param {string} props.solution.description - Opis rozwiązania
 * @param {string} props.solution.benefit - Tekst korzyści
 * @param {string} props.solution.benefitIcon - Nazwa ikony korzyści
 * @returns {JSX.Element} Karta rozwiązania
 */
export default function SolutionCard({ solution }) {
  const { icon, title, description, benefit, benefitIcon } = solution;

  /**
   * Zwraca odpowiednią ikonę na podstawie nazwy
   * 
   * @param {string} name - Nazwa ikony
   * @returns {import('@fortawesome/fontawesome-svg-core').IconDefinition} Obiekt ikony
   */
  const getBenefitIcon = (name) => {
    return BENEFIT_ICONS[name] || faArrowUp;
  };

  return (
    <div className="bg-white rounded-lg shadow-md overflow-hidden hover:shadow-lg transition-all duration-300 h-full flex flex-col">
      <div className="p-6 flex-1 flex flex-col">
        <div className="w-12 h-12 rounded-full bg-purple-600/10 flex items-center justify-center text-purple-600 mb-4">
          <FontAwesomeIcon icon={icon} className="text-xl" />
        </div>

        <h3 className="text-xl font-semibold mb-3">{title}</h3>

        <p className="text-gray-600 mb-4">{description}</p>

        <div className="flex items-center text-green-600 font-medium mt-auto">
          <FontAwesomeIcon icon={getBenefitIcon(benefitIcon)} className="mr-2" />
          <span>{benefit}</span>
        </div>
      </div>
    </div>
  );
}