import React from 'react';
import Link from 'next/link';
import FontAwesomeIcon from "@/components/common/FontAwesomeIcon";
import StarryBackground from '@/components/common/StarryBackground';
import {
  faArrowRight,
  faUser,
  faCode,
  faServer,
  faDatabase,
  faChartLine
} from '@fortawesome/free-solid-svg-icons';
import {
  faLinkedin,
  faGithub
} from '@fortawesome/free-brands-svg-icons';

/**
 * Komponent sekcji zespołu
 */
export default function Team() {
  return (
    <section id="zespol" className="py-20 bg-white">
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        {/* Nagłówek sekcji */}
        <div className="text-center mb-12">
          <h2 className="text-3xl md:text-4xl font-bold mb-4">Poznaj SoftwareLogic</h2>
          <p className="text-lg text-gray-600 max-w-3xl mx-auto">
            Za sukcesem SoftwareLogic stoi zespół doświadczonych ekspertów, którzy łączą wiedzę technologiczną z biznesowym podejściem
          </p>
        </div>

        {/* CEO Card */}
        <div className="bg-white rounded-lg shadow-md mb-12 overflow-hidden">
          <div className="grid grid-cols-1 md:grid-cols-3">
            {/* Lewa kolumna z informacjami o CEO */}
            <div className="p-8">
              <div className="flex items-center mb-6">
                <div className="w-16 h-16 rounded-full bg-purple-100 flex items-center justify-center text-purple-600 mr-4">
                  <FontAwesomeIcon icon={faUser} className="text-2xl" />
                </div>
                <div>
                  <h3 className="text-2xl font-bold">Konrad Kur</h3>
                  <p className="text-purple-600">CEO & Główny Architekt</p>
                </div>
              </div>
              
              <p className="text-gray-600 mb-4">
                Doświadczony architekt rozwiązań technologicznych z 14-letnim stażem w branży IT. 
                Specjalizuje się w projektowaniu skalowalnych, wydajnych systemów.
              </p>
              
              <div className="flex space-x-4">
                <a 
                  href="https://www.linkedin.com/in/konrad-kur-66401b64/" 
                  className="text-gray-400 hover:text-purple-600 transition-colors"
                  aria-label="LinkedIn"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <FontAwesomeIcon icon={faLinkedin} className="text-xl" />
                </a>
                <a 
                  href="https://github.com/konradkur-softwarelogic" 
                  className="text-gray-400 hover:text-purple-600 transition-colors"
                  aria-label="GitHub"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <FontAwesomeIcon icon={faGithub} className="text-xl" />
                </a>
              </div>
            </div>
            
            {/* Środkowa kolumna z doświadczeniem */}
            <div className="bg-gray-50 p-8">
              <h4 className="text-lg font-semibold mb-4">Doświadczenie komercyjne</h4>
              <ul className="space-y-3">
                <ExperienceItem 
                  icon={faServer}
                  text="Projekty dla instytucji publicznych (Rządowe Centrum Legislacji, KRUS)"
                />
                <ExperienceItem 
                  icon={faDatabase}
                  text="Rozwiązania bankowości elektronicznej (iPKO.pl)"
                />
                <ExperienceItem 
                  icon={faCode}
                  text="Pasjonat Linuksa i Pythona od 2009 roku"
                />
              </ul>
            </div>
            
            {/* Prawa kolumna z cytatem */}
            <div className="bg-gradient-to-br from-purple-600 to-purple-800 p-8 text-white flex flex-col justify-center">
              <p className="text-lg italic font-medium">
                "Łączymy głęboką wiedzę techniczną z biznesowym podejściem i szerokim doświadczeniem komercyjnym, co pozwala nam lepiej rozumieć potrzeby klientów i dostarczać rozwiązania, które naprawdę działają."
              </p>
              <p className="mt-4 font-semibold">— Konrad Kur, CEO</p>
            </div>
          </div>
        </div>

        {/* Co nas wyróżnia */}
        <div className="mb-12">
          <div className="text-center mb-8">
            <h3 className="text-2xl font-bold">Co wyróżnia nasz zespół</h3>
          </div>
          
          <div className="grid grid-cols-1 md:grid-cols-3 gap-6">
            <FeatureCard
              title="Ekspertyza techniczna"
              description="Nasz zespół posiada głęboką wiedzę techniczną, którą wykorzystujemy do tworzenia efektywnych rozwiązań."
            />
            
            <FeatureCard
              title="Zrozumienie biznesu"
              description="Staramy się zrozumieć Twój biznes, aby dostarczyć rozwiązania, które realnie wspierają Twoje cele."
            />
            
            <FeatureCard
              title="Partnerskie podejście"
              description="Traktujemy naszych klientów jak partnerów, wspólnie pracując nad sukcesem projektu."
            />
          </div>
        </div>

        {/* CTA - dołącz do zespołu z kosmicznym tłem */}
        <div className="rounded-lg p-8 shadow-lg relative overflow-hidden">
          {/* Kosmiczne tło */}
          <div className="absolute inset-0 bg-gradient-to-r from-[#312e81] to-[#4c1d95] z-0"></div>
          
          {/* Tło gwiaździste */}
          <StarryBackground />

          <div className="relative z-10 text-center">
            <h3 className="text-2xl font-bold mb-4 text-white">Dołącz do naszego zespołu</h3>
            <p className="text-white/90 mb-6 max-w-2xl mx-auto">
              Jesteśmy zawsze otwarci na nowe talenty. Jeśli jesteś pasjonatem technologii i chcesz rozwijać się w dynamicznym środowisku, wyślij nam swoje CV.
            </p>

            <Link href="mailto:office@softwarelogic.co" className="btn-primary">
              <FontAwesomeIcon icon={faArrowRight} className="h-5 w-5 mr-2" />
              <span>Aplikuj teraz</span>
            </Link>
          </div>
        </div>
      </div>
    </section>
  );
}

/**
 * Komponent elementu doświadczenia
 */
function ExperienceItem({ icon, text }) {
  return (
    <li className="flex items-start">
      <div className="flex-shrink-0 w-5 h-5 text-purple-600 mt-1">
        <FontAwesomeIcon icon={icon} />
      </div>
      <span className="ml-3 text-gray-700">{text}</span>
    </li>
  );
}

/**
 * Komponent karty funkcji/cechy
 */
function FeatureCard({ title, description }) {
  return (
    <div className="bg-white p-6 rounded-lg shadow-sm border border-gray-100 hover:shadow-md transition-shadow">
      <h4 className="font-semibold text-xl mb-3">{title}</h4>
      <p className="text-gray-600">{description}</p>
    </div>
  );
}