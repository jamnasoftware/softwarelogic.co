import React from 'react';
import Link from 'next/link';
import FontAwesomeIcon from "@/components/common/FontAwesomeIcon";
import PrimaryButton from "@/components/common/PrimaryButton";
import StarryBackground from '@/components/common/StarryBackground';
import {
  faDesktop,
  faMobileAlt,
  faLaptopCode,
  faServer,
  faCloud,
  faBolt,
  faArrowRight,
  faCommentDots
} from '@fortawesome/free-solid-svg-icons';

/**
 * Dane oferty
 */
const offerData = [
  {
    icon: faLaptopCode,
    title: "Aplikacje webowe",
    description: "Tworzymy wydajne i responsywne aplikacje internetowe, które doskonale działają na wszystkich urządzeniach.",
    url: "/oferta/aplikacje-webowe"
  },
  {
    icon: faDesktop,
    title: "Aplikacje desktop",
    description: "Tworzymy wieloplatformowe aplikacje desktopowe dla Windows, macOS i Linux, łączące wydajność z intuicyjnością.",
    url: "/oferta/aplikacje-desktop"
  },
  {
    icon: faServer,
    title: "Mikroserwisy i integratory",
    description: "Projektujemy skalowalne architektury oparte na mikroserwisach, tworzymy wydajne i bezpieczne API oraz integratory umożliwiające sprawną komunikację między systemami.",
    url: "/oferta/mikroserwisy-i-integratory"
  },
  {
    icon: faCloud,
    title: "DevOps & Cloud",
    description: "Wdrażamy automatyzację procesów CI/CD, zarządzanie infrastrukturą i skalowalne rozwiązania chmurowe.",
    url: "/oferta/devops-i-cloud"
  },
  {
    icon: faMobileAlt,
    title: "Aplikacje mobilne",
    description: "Budujemy natywne i cross-platformowe aplikacje mobilne z intuicyjnym interfejsem i wydajną synchronizacją danych.",
    url: "/oferta/aplikacje-mobilne"
  },
  {
    icon: faBolt,
    title: "Aplikacje No-Code / Low-Code",
    description: "Uzupełniająco oferujemy szybkie rozwiązania bez kodu, idealne do prostszych zastosowań i prototypowania.",
    url: "/oferta/aplikacje-nocode-lowcode"
  }
];

/**
 * Komponent sekcji oferty
 * 
 * @returns {JSX.Element} Komponent oferty
 */
export default function Offer() {
  return (
    <section className="py-20 bg-white">
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        {/* Nagłówek sekcji */}
        <div className="text-center mb-12">
          <h2 className="text-3xl md:text-4xl font-bold mb-4">Nasza oferta</h2>
          <p className="text-lg text-gray-600 max-w-3xl mx-auto">
            Oferujemy kompleksowe rozwiązania dostosowane do indywidualnych potrzeb Twojego biznesu
          </p>
        </div>

        {/* Karty oferty */}
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
          {offerData.map((item, index) => (
            <OfferCard key={index} item={item} />
          ))}
        </div>
        
        {/* CTA z kosmicznym tłem */}
        <div className="rounded-lg p-8 md:p-10 shadow-lg relative overflow-hidden mt-12">
          {/* Kosmiczne tło */}
          <div className="absolute inset-0 bg-gradient-to-r from-[#312e81] to-[#4c1d95] z-0"></div>
          
          {/* Tło gwiaździste */}
          <StarryBackground />
          
          <div className="max-w-3xl mx-auto relative z-10 text-center">
            <h3 className="text-2xl font-bold mb-4 text-white">Potrzebujesz wsparcia w doborze rozwiązania?</h3>
            <p className="text-lg text-white/90 mb-8 md:px-8">
              Skontaktuj się z nami, aby omówić jak możemy pomóc w realizacji Twojego projektu
            </p>
            <div className="flex flex-wrap justify-center">
              <Link href="#kontakt" className="btn-primary">
                <FontAwesomeIcon icon={faCommentDots} className="h-5 w-5 mr-2" />
                Bezpłatna konsultacja
              </Link>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

/**
 * Komponent karty oferty (bez korzyści)
 * 
 * @param {Object} props
 * @param {Object} props.item - Dane elementu oferty
 * @param {import('@fortawesome/fontawesome-svg-core').IconDefinition} props.item.icon - Ikona
 * @param {string} props.item.title - Tytuł
 * @param {string} props.item.description - Opis
 * @param {string} props.item.url - Link do strony
 * @returns {JSX.Element} Karta oferty
 */
function OfferCard({ item }) {
  const { icon, title, description, url } = item;
  
  return (
    <div className="bg-white rounded-lg border border-gray-100 shadow-md hover:shadow-lg transition-all duration-300 flex flex-col h-full overflow-hidden">
      <div className="p-6 flex-1">
        <div className="w-12 h-12 rounded-full bg-purple-600/10 flex items-center justify-center text-purple-600 mb-4">
          <FontAwesomeIcon icon={icon} className="text-xl" />
        </div>
        <h3 className="text-xl font-bold mb-3">{title}</h3>
        <p className="text-gray-600">
          {description}
        </p>
      </div>
      
      {/* Link do szczegółów w stopce */}
      {url && (
        <div className="mt-auto border-t border-gray-100 p-4">
          <Link 
            href={url} 
            className="inline-flex items-center text-purple-600 hover:text-purple-700 font-medium text-sm"
          >
            Zobacz pełną ofertę 
            <FontAwesomeIcon icon={faArrowRight} className="h-4 w-4 ml-2" />
          </Link>
        </div>
      )}
    </div>
  );
}