"use client";
import React from 'react';
import { FontAwesomeIcon as FAI } from '@fortawesome/react-fontawesome';
import dynamic from 'next/dynamic';

/**
 * Wrapper dla komponentu FontAwesomeIcon, który zapobiega błędom hydracji
 * i umożliwia używanie ikon FontAwesome w aplikacji Next.js
 * 
 * Komponent jest ładowany dynamicznie z wyłączonym SSR, aby uniknąć
 * niezgodności między renderowaniem po stronie serwera i klienta
 *
 * @param {Object} props - Wszystkie właściwości przekazywane do oryginalnego komponentu FontAwesomeIcon
 * @returns {JSX.Element} Komponent ikony lub placeholder podczas SSR
 */
const FontAwesomeIconComponent = (props) => {
  // Podczas renderowania po stronie serwera wyświetl placeholder
  if (typeof window === 'undefined') {
    return <span className="fa-placeholder" aria-hidden="true"></span>;
  }
  
  // Po stronie klienta renderuj oryginalny komponent FontAwesomeIcon
  return <FAI {...props} />;
};

// Eksportujemy komponent z wyłączonym SSR
export default dynamic(() => Promise.resolve(FontAwesomeIconComponent), {
  ssr: false
});