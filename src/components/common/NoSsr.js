"use client";
import dynamic from 'next/dynamic';
import React from 'react';

/**
 * Komponent NoSsr (No Server-Side Rendering)
 * 
 * Zapobiega renderowaniu zawartości po stronie serwera,
 * co jest przydatne dla komponentów, które powinny działać
 * wyłącznie w środowisku przeglądarki.
 * 
 * @param {Object} props - Właściwości komponentu
 * @param {React.ReactNode} props.children - Zawartość do wyrenderowania tylko po stronie klienta
 * @returns {JSX.Element} Opakowanie dla treści renderowanej tylko po stronie klienta
 */
const NoSsr = props => (
  <React.Fragment>{props.children}</React.Fragment>
);

/**
 * Eksportujemy komponent z wyłączonym SSR za pomocą dynamic import
 * 
 * Dzięki temu zawartość zostanie wyrenderowana tylko po stronie klienta,
 * co pozwala uniknąć błędów hydracji i problemów z komponentami,
 * które wymagają dostępu do API przeglądarki (window, document, itp.)
 */
export default dynamic(() => Promise.resolve(NoSsr), {
  ssr: false
});