import React from 'react';

/**
 * Komponent tła gwiaździstego do wielokrotnego użycia na stronie
 * 
 * @returns {JSX.Element} Komponent tła gwiaździstego
 */
export default function StarryBackground() {
  return (
    <div className="absolute inset-0 overflow-hidden">
      <div className="stars-container">
        <div className="stars"></div>
        <div className="stars2"></div>
        <div className="stars3"></div>
        <div className="nebula1"></div>
        <div className="nebula2"></div>
        <div className="shooting-star"></div>
        
        {/* Dodatkowe warstwy zapobiegające zanikaniu animacji */}
        <div className="stars-fix">
          <div className="stars"></div>
          <div className="stars2"></div>
          <div className="stars3"></div>
        </div>
      </div>
    </div>
  );
}
