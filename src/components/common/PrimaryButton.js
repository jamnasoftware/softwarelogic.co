import React, { useEffect, useState } from 'react';
import Link from "next/link";

/**
 * Komponent uniwersalnego przycisku używanego w całej aplikacji
 * Zapewnia spójny wygląd wszystkich przycisków i linkow CTA
 */
const PrimaryButton = ({
  children, 
  animateIn = false, 
  variant = 'primary', 
  size = 'md',
  icon = null,
  iconRight = true,
  className = "",
  href,
  ...props
}) => {
  const [showAnimation, setShowAnimation] = useState(false);
  const [isMounted, setIsMounted] = useState(false);

  useEffect(() => {
    setIsMounted(true);
    
    if (!animateIn) return;
    // Aktywuj animację tylko po renderowaniu na kliencie
    setShowAnimation(true);

    // Usuń animację po 3 sekundach
    const timer = setTimeout(() => {
      setShowAnimation(false);
    }, 3000);

    return () => clearTimeout(timer);
  }, [animateIn]);

  // Mapowanie wariantów na klasy
  const variantClasses = {
    'primary': 'bg-gradient-to-r from-purple-600 to-purple-800 hover:from-purple-700 hover:to-purple-900 text-white shadow-md hover:shadow-lg',
    'secondary': 'bg-white text-purple-700 border border-purple-600 hover:bg-purple-50 shadow-sm hover:shadow',
    'outline': 'bg-transparent text-purple-700 border border-purple-600 hover:bg-purple-50',
    'white': 'bg-white text-purple-700 hover:bg-gray-50 shadow-md hover:shadow-lg'
  };

  // Mapowanie rozmiarów na klasy
  const sizeClasses = {
    'sm': 'px-4 py-2 text-sm',
    'md': 'px-6 py-3 text-base',
    'lg': 'px-8 py-4 text-lg'
  };

  // Bazowa klasa dla wszystkich przycisków
  const baseClass = 'inline-flex items-center justify-center font-medium rounded-lg transition-all duration-300 focus:outline-none focus:ring-2 focus:ring-purple-500 focus:ring-opacity-50';
  
  // Animacja (tylko po stronie klienta)
  const animationClass = isMounted && showAnimation ? 'animate-bounce' : '';
  
  // Połącz wszystkie klasy
  const buttonClass = `${baseClass} ${variantClasses[variant] || variantClasses.primary} ${sizeClasses[size] || sizeClasses.md} ${animationClass} ${className}`;

  // Ikona do wyświetlenia
  const iconElement = icon ? (
    <span className={`${iconRight ? 'ml-2' : 'mr-2'} flex items-center`}>
      {icon}
    </span>
  ) : null;

  // Zawartość przycisku z ikoną na odpowiedniej pozycji
  const content = (
    <>
      {!iconRight && iconElement}
      <span>{children}</span>
      {iconRight && iconElement}
    </>
  );

  // Renderowanie po stronie serwera (uproszczona wersja bez animacji)
  if (!isMounted) {
    const staticClass = `${baseClass} ${variantClasses[variant] || variantClasses.primary} ${sizeClasses[size] || sizeClasses.md} ${className}`;
    
    return !href ? (
      <button {...props} className={staticClass}>{content}</button>
    ) : (
      <Link href={href} {...props} className={staticClass}>{content}</Link>
    );
  }

  // Renderowanie po stronie klienta
  return !href ? (
    <button {...props} className={buttonClass}>{content}</button>
  ) : (
    <Link href={href} {...props} className={buttonClass}>{content}</Link>
  );
};

export default PrimaryButton;