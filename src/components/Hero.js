import Link from 'next/link';
import Image from 'next/image';
import FontAwesomeIcon from "../components/common/FontAwesomeIcon";
import Marquee from "react-fast-marquee";
import PrimaryButton from "@/components/common/PrimaryButton";

export default function Hero() {
  // Konfiguracja logotypów ze zrównoważoną liczbą w każdym rzędzie
  const rows = [
    // Pierwszy rząd - 6 dużych logotypów
    [
      { src: '/dropui.png', alt: 'Logo DropUI' },
      { src: '/timecamp.png', alt: 'Logo TimeCamp' },
      { src: '/btc.png', alt: 'Logo BTC' },
      { src: '/idosell.svg', alt: 'Logo IdoSell' },
      { src: '/asseco.png', alt: 'Logo Asseco' },
      { src: '/imker.png', alt: 'Logo Imker' }
    ],
    // Drugi rząd - również 6 logotypów
    [
      { src: '/ministerstwo.png', alt: 'Logo Ministerstwo' },
      { src: '/skinwallet.png', alt: 'Logo Skinwallet' },
      { src: '/isotrade.png', alt: 'Logo ISO Trade' },
      { src: '/fffrree.png', alt: 'Logo Fffrree' },
      { src: '/dreamlink.svg', alt: 'Logo Dreamlink' },
      { src: '/diaval.png', alt: 'Logo Diaval' }
    ],
    // Trzeci rząd - 8 mniejszych logotypów (marquee)
    [
      { src: '/cateromarket.png', alt: 'Logo Cateromarket' },
      { src: '/eon.png', alt: 'Logo EON' },
      { src: '/redsky.png', alt: 'Logo RedSky' },
      { src: '/paudio.png', alt: 'Logo Paudio' },
      { src: '/easyprotect.png', alt: 'Logo EasyProtect' },
      { src: '/us.png', alt: 'Logo US' },
      { src: '/pomerangels.png', alt: 'Logo PomerAngels' },
      { src: '/shortest.svg', alt: 'Logo Shortest' }
    ]
  ];

  return (
    <section className="pt-32 pb-16 md:pt-40 md:pb-24 bg-linear-to-br from-white to-purple-50">
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div className="flex flex-col lg:flex-row items-center">
          <div className="lg:w-1/2 lg:pr-12 mb-10 lg:mb-0">
            <h1 className="text-4xl md:text-5xl lg:text-6xl font-bold tracking-tight text-gray-900 mb-6">
              Zwiększamy <span className="gradient-text">zyski biznesu</span> przez technologię
            </h1>
            <p className="text-xl text-gray-600 mb-8 leading-relaxed">
              Tworzymy dedykowane oprogramowanie, które <strong>zwiększa efektywność o 40%</strong>, <strong>obniża koszty operacyjne</strong> i daje Twojej firmie <strong>przewagę konkurencyjną</strong>.
            </p>
            
            <div className="mb-6">
              <div className="flex flex-col sm:flex-row gap-4 mb-6">
                <PrimaryButton
                  href="#kontakt"
                  icon={<svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="h-5 w-5 transform group-hover:translate-x-1 transition-transform duration-300"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fillRule="evenodd"
                      d="M10.293 5.293a1 1 0 011.414 0l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414-1.414L12.586 11H5a1 1 0 110-2h7.586l-2.293-2.293a1 1 0 010-1.414z"
                      clipRule="evenodd"
                    />
                  </svg>}
                >
                  Bezpłatna konsultacja
                </PrimaryButton>
                
                <Link href="#case-studies" legacyBehavior>
                  <a className="inline-flex justify-center items-center px-8 py-4 border border-primary text-primary rounded-lg font-semibold hover:bg-primary/5 transition-all">
                    <span>Zobacz realizacje</span>
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 ml-2" viewBox="0 0 20 20" fill="currentColor">
                      <path d="M11 3a1 1 0 100 2h2.586l-6.293 6.293a1 1 0 101.414 1.414L15 6.414V9a1 1 0 102 0V4a1 1 0 00-1-1h-5z" />
                      <path d="M5 5a2 2 0 00-2 2v8a2 2 0 002 2h8a2 2 0 002-2v-3a1 1 0 10-2 0v3H5V7h3a1 1 0 000-2H5z" />
                    </svg>
                  </a>
                </Link>
              </div>

              <p className="text-sm text-gray-500 mt-3">
                <span className="inline-flex items-center">
                  <FontAwesomeIcon icon="star" className="w-4 h-4 text-yellow-400 mr-1" />
                  <span>30+ projektów, 100% zadowolonych klientów</span>
                </span>
              </p>
            </div>
          </div>
          
          <div className="lg:w-1/2 relative">
            <div className="absolute -top-6 -left-6 w-full h-full bg-primary/10 rounded-2xl"></div>
            <div className="rounded-2xl overflow-hidden shadow-2xl relative bg-white">
              <div className={"h-full w-full absolute bg-primary/40"}></div>
              <video
                autoPlay
                muted
                loop
                playsInline
                className="w-full h-auto"
                >
                <source src="programming.mp4" type="video/mp4" />
              </video>
            </div>
          </div>
        </div>
      </div>
      
      {/* Sekcja z logo klientów - BEZ ZMIAN w stosunku do oryginału */}
      <div className="w-full py-14 bg-gray-50 mt-16 relative">
        {/* Subtelna siatka w tle */}
        <div className="absolute inset-0 opacity-5">
          <div className="h-full w-full" style={{
            backgroundImage: 'radial-gradient(rgba(100, 100, 100, 0.4) 1px, transparent 1px)',
            backgroundSize: '20px 20px'
          }}></div>
        </div>
        
        <div className="max-w-7xl mx-auto relative z-10">
          <div className="px-4 sm:px-6 lg:px-8">
            <div className="flex flex-col sm:flex-row justify-between items-center mb-10">
              <p className="text-base text-gray-500 uppercase tracking-wider mb-3 sm:mb-0">Zaufali nam:</p>
            </div>
          
            {/* Kontener dla wszystkich logo - ZMODYFIKOWANE zachowanie hover */}
            <div className="logo-container w-full">
              {/* Pierwszy rząd - wszystkie loga widoczne również w RWD */}
              <div className="mb-14">
                {/* Widok mobilny - grid z 3 kolumnami i 2 rzędami */}
                <div className="block md:hidden">
                  <div className="grid grid-cols-3 gap-4 mb-4">
                    {/* Pierwszy wiersz mobilny: pierwsze 3 loga */}
                    <div className="flex justify-start">
                      <div className="bg-white rounded-lg shadow-sm p-3 flex items-center justify-center h-12 border border-gray-100 hover:shadow-md transition-all">
                        <Image
                          src={rows[0][0].src}
                          alt={rows[0][0].alt}
                          width={110}
                          height={35}
                          className="max-h-6 w-auto object-contain grayscale hover:grayscale-0 opacity-70 hover:opacity-100 transition-all duration-300"
                        />
                      </div>
                    </div>
                    <div className="flex justify-center">
                      <div className="bg-white rounded-lg shadow-sm p-3 flex items-center justify-center h-12 border border-gray-100 hover:shadow-md transition-all">
                        <Image
                          src={rows[0][1].src}
                          alt={rows[0][1].alt}
                          width={110}
                          height={35}
                          className="max-h-6 w-auto object-contain grayscale hover:grayscale-0 opacity-70 hover:opacity-100 transition-all duration-300"
                        />
                      </div>
                    </div>
                    <div className="flex justify-end">
                      <div className="bg-white rounded-lg shadow-sm p-3 flex items-center justify-center h-12 border border-gray-100 hover:shadow-md transition-all">
                        <Image
                          src={rows[0][2].src}
                          alt={rows[0][2].alt}
                          width={110}
                          height={35}
                          className="max-h-6 w-auto object-contain grayscale hover:grayscale-0 opacity-70 hover:opacity-100 transition-all duration-300"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="grid grid-cols-3 gap-4">
                    {/* Drugi wiersz mobilny: pozostałe 3 loga */}
                    <div className="flex justify-start">
                      <div className="bg-white rounded-lg shadow-sm p-3 flex items-center justify-center h-12 border border-gray-100 hover:shadow-md transition-all">
                        <Image
                          src={rows[0][3].src}
                          alt={rows[0][3].alt}
                          width={110}
                          height={35}
                          className="max-h-6 w-auto object-contain grayscale hover:grayscale-0 opacity-70 hover:opacity-100 transition-all duration-300"
                        />
                      </div>
                    </div>
                    <div className="flex justify-center">
                      <div className="bg-white rounded-lg shadow-sm p-3 flex items-center justify-center h-12 border border-gray-100 hover:shadow-md transition-all">
                        <Image
                          src={rows[0][4].src}
                          alt={rows[0][4].alt}
                          width={110}
                          height={35}
                          className="max-h-6 w-auto object-contain grayscale hover:grayscale-0 opacity-70 hover:opacity-100 transition-all duration-300"
                        />
                      </div>
                    </div>
                    <div className="flex justify-end">
                      <div className="bg-white rounded-lg shadow-sm p-3 flex items-center justify-center h-12 border border-gray-100 hover:shadow-md transition-all">
                        <Image
                          src={rows[0][5].src}
                          alt={rows[0][5].alt}
                          width={110}
                          height={35}
                          className="max-h-6 w-auto object-contain grayscale hover:grayscale-0 opacity-70 hover:opacity-100 transition-all duration-300"
                        />
                      </div>
                    </div>
                  </div>
                </div>
                
                {/* Widok tabletów i desktopów - flex z równomiernym rozłożeniem */}
                <div className="hidden md:flex justify-between items-center">
                  {rows[0].map((logo, index) => (
                    <div 
                      key={`row1-${index}`} 
                      className={`flex ${index === 0 ? 'justify-start' : index === rows[0].length - 1 ? 'justify-end' : 'justify-center'}`}
                    >
                      <div className="bg-white rounded-lg shadow-sm p-4 flex items-center justify-center h-16 border border-gray-100 hover:shadow-md transition-all w-40">
                        <Image
                          src={logo.src}
                          alt={logo.alt}
                          width={120}
                          height={40}
                          className="max-h-8 w-auto object-contain grayscale hover:grayscale-0 opacity-70 hover:opacity-100 transition-all duration-300"
                        />
                      </div>
                    </div>
                  ))}
                </div>
              </div>
              
              {/* Drugi rząd - wszystkie loga widoczne również w RWD */}
              <div className="mb-14">
                {/* Widok mobilny - grid z 3 kolumnami i 2 rzędami */}
                <div className="block md:hidden">
                  <div className="grid grid-cols-3 gap-4 mb-4">
                    {/* Pierwszy wiersz mobilny: pierwsze 3 loga */}
                    <div className="flex justify-start">
                      <div className="bg-white rounded-lg shadow-sm p-3 flex items-center justify-center h-12 border border-gray-100 hover:shadow-md transition-all">
                        <Image
                          src={rows[1][0].src}
                          alt={rows[1][0].alt}
                          width={110}
                          height={35}
                          className="max-h-6 w-auto object-contain grayscale hover:grayscale-0 opacity-70 hover:opacity-100 transition-all duration-300"
                        />
                      </div>
                    </div>
                    <div className="flex justify-center">
                      <div className="bg-white rounded-lg shadow-sm p-3 flex items-center justify-center h-12 border border-gray-100 hover:shadow-md transition-all">
                        <Image
                          src={rows[1][1].src}
                          alt={rows[1][1].alt}
                          width={110}
                          height={35}
                          className="max-h-6 w-auto object-contain grayscale hover:grayscale-0 opacity-70 hover:opacity-100 transition-all duration-300"
                        />
                      </div>
                    </div>
                    <div className="flex justify-end">
                      <div className="bg-white rounded-lg shadow-sm p-3 flex items-center justify-center h-12 border border-gray-100 hover:shadow-md transition-all">
                        <Image
                          src={rows[1][2].src}
                          alt={rows[1][2].alt}
                          width={110}
                          height={35}
                          className="max-h-6 w-auto object-contain grayscale hover:grayscale-0 opacity-70 hover:opacity-100 transition-all duration-300"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="grid grid-cols-3 gap-4">
                    {/* Drugi wiersz mobilny: pozostałe 3 loga */}
                    <div className="flex justify-start">
                      <div className="bg-white rounded-lg shadow-sm p-3 flex items-center justify-center h-12 border border-gray-100 hover:shadow-md transition-all">
                        <Image
                          src={rows[1][3].src}
                          alt={rows[1][3].alt}
                          width={110}
                          height={35}
                          className="max-h-6 w-auto object-contain grayscale hover:grayscale-0 opacity-70 hover:opacity-100 transition-all duration-300"
                        />
                      </div>
                    </div>
                    <div className="flex justify-center">
                      <div className="bg-white rounded-lg shadow-sm p-3 flex items-center justify-center h-12 border border-gray-100 hover:shadow-md transition-all">
                        <Image
                          src={rows[1][4].src}
                          alt={rows[1][4].alt}
                          width={110}
                          height={35}
                          className="max-h-6 w-auto object-contain grayscale hover:grayscale-0 opacity-70 hover:opacity-100 transition-all duration-300"
                        />
                      </div>
                    </div>
                    <div className="flex justify-end">
                      <div className="bg-white rounded-lg shadow-sm p-3 flex items-center justify-center h-12 border border-gray-100 hover:shadow-md transition-all">
                        <Image
                          src={rows[1][5].src}
                          alt={rows[1][5].alt}
                          width={110}
                          height={35}
                          className="max-h-6 w-auto object-contain grayscale hover:grayscale-0 opacity-70 hover:opacity-100 transition-all duration-300"
                        />
                      </div>
                    </div>
                  </div>
                </div>
                
                {/* Widok tabletów i desktopów - flex z równomiernym rozłożeniem */}
                <div className="hidden md:flex justify-between items-center">
                  {rows[1].map((logo, index) => (
                    <div 
                      key={`row2-${index}`} 
                      className={`flex ${index === 0 ? 'justify-start' : index === rows[1].length - 1 ? 'justify-end' : 'justify-center'}`}
                    >
                      <div className="bg-white rounded-lg shadow-sm p-4 flex items-center justify-center h-16 border border-gray-100 hover:shadow-md transition-all w-40">
                        <Image
                          src={logo.src}
                          alt={logo.alt}
                          width={120}
                          height={40}
                          className="max-h-8 w-auto object-contain grayscale hover:grayscale-0 opacity-70 hover:opacity-100 transition-all duration-300"
                        />
                      </div>
                    </div>
                  ))}
                </div>
              </div>
              
              {/* Trzeci rząd - jako Marquee (już działający dobrze) */}
              <div className="mb-3">
                <p className="text-sm text-gray-500 mb-5">Więcej partnerów:</p>
                
                <Marquee
                  autoFill 
                  className="overflow-visible"
                  speed={40}
                  gradientWidth={50}
                >
                  {rows[2].map((logo, index) => (
                    <div 
                      key={`marquee-${index}`}
                      className="bg-white rounded-lg shadow-sm p-3 mx-4 flex items-center justify-center h-14 border border-gray-100 hover:shadow-md transition-all"
                    >
                      <Image
                        src={logo.src}
                        alt={logo.alt}
                        width={110}
                        height={35}
                        className="h-8 w-auto object-contain grayscale hover:grayscale-0 opacity-60 hover:opacity-100 transition-all duration-300"
                      />
                    </div>
                  ))}
                </Marquee>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}