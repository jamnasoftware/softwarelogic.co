import React, { useState, useEffect } from "react";
import Head from "next/head";
import dynamic from "next/dynamic";

// Importy komponentów statycznych (renderowanych po stronie serwera)
import Header from "../components/layout/Header";
import Hero from "../components/Hero";
import Stats from "../components/stats/Stats";
import Solutions from "../components/solutions/Solutions";
import CaseStudies from "../components/case-studies/CaseStudies";
import Technologies from "../components/technologies/Technologies";
import Team from "../components/team/Team";
import FAQ from "../components/faq/FAQ";
import Offer from "../components/offer/Offer";
import Contact from "../components/contact/Contact";
import Footer from "../components/layout/Footer";
import Process from "@/components/process/Process";

// Dynamiczne importy (tylko po stronie klienta)
const DynamicFloatingCTA = dynamic(() => import("../components/FloatingCTA"), { 
  ssr: false 
});

/**
 * Komponent strony głównej
 * 
 * @returns {JSX.Element} Strona główna
 */
export default function Home() {
  const [isMounted, setIsMounted] = useState(false);
  const [showFloatingCTA, setShowFloatingCTA] = useState(false);

  // Efekt dla inicjalizacji komponentu i obsługi przewijania
  useEffect(() => {
    // Zaznacz, że komponent został zamontowany po stronie klienta
    setIsMounted(true);

    /**
     * Obsługa zdarzenia przewijania - pokazuje lub ukrywa pływające CTA
     */
    const handleScroll = () => {
      if (typeof window !== 'undefined') {
        const heroSection = document.querySelector("section");

        if (heroSection) {
          const heroBottom = heroSection.offsetTop + heroSection.offsetHeight;
          setShowFloatingCTA(window.scrollY > heroBottom);
        }
      }
    };

    // Dodaj nasłuchiwanie zdarzenia po stronie klienta
    if (typeof window !== 'undefined') {
      window.addEventListener("scroll", handleScroll);
      // Wykonaj początkowe sprawdzenie
      handleScroll();
      
      // Odłącz nasłuchiwanie przy demontażu komponentu
      return () => window.removeEventListener("scroll", handleScroll);
    }
  }, []);

  return (
    <>
      <Head>
        <title>
          SoftwareLogic | Rozwiązania technologiczne zwiększające zyski biznesu
        </title>
        <meta
          name="description"
          content="Tworzymy dedykowane systemy informatyczne, które zwiększają efektywność, obniżają koszty i dają przewagę konkurencyjną"
        />
      </Head>

      <Header />
      
      <main>
        <Hero />
        <Stats />
        <Solutions />
        <Process />
        <Offer />
        <CaseStudies />
        <Technologies />
        <Team />
        <FAQ />
        <Contact />
      </main>
      
      <Footer />
      
      {/* Renderuj FloatingCTA tylko po stronie klienta i gdy ma być widoczne */}
      {isMounted && showFloatingCTA && <DynamicFloatingCTA />}
    </>
  );
}