import "../styles/globals.css";
import { config } from "@fortawesome/fontawesome-svg-core";
import "@fortawesome/fontawesome-svg-core/styles.css";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fab } from "@fortawesome/free-brands-svg-icons";
import { fas } from "@fortawesome/free-solid-svg-icons";
import Head from "next/head";
import Script from "next/script";
import { useEffect } from "react";
// Ważne: zapobiega konfliktom z SSR
config.autoAddCss = false;
library.add(fab, fas);

export default function App({ Component, pageProps }) {
  useEffect(() => {
    if (typeof window !== "undefined") {
      const handleError = (event) => {
        // Logujemy błąd dla celów diagnostycznych
        console.error("Application error:", event.error || event.message);

        // Jeśli błąd dotyczy manipulacji aplikacją, zapobiegamy domyślnemu alertowi
        if (
          (event.error &&
            event.error.message &&
            event.error.message.includes("manipulation")) ||
          (event.message && event.message.includes("manipulation"))
        ) {
          event.preventDefault();
          event.stopPropagation();
          console.warn("Wykryto manipulację, ale bez wyświetlania komunikatu.");
          return true; // Oznacza, że błąd został obsłużony
        }
      };

      window.addEventListener("error", handleError);
      return () => window.removeEventListener("error", handleError);
    }
  }, []);

  return (
    <>
      <Head>
        <title>
          SoftwareLogic | Rozwiązania technologiczne zwiększające zyski biznesu
        </title>
        <meta
          name="description"
          content="Tworzymy dedykowane systemy informatyczne, które zwiększają efektywność, obniżają koszty i dają przewagę konkurencyjną"
        />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link
          rel="preconnect"
          href="https://fonts.gstatic.com"
          crossOrigin="anonymous"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&family=Space+Grotesk:wght@400;500;600;700&display=swap"
          rel="stylesheet"
        />
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css"
          integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA=="
          crossOrigin="anonymous"
          referrerPolicy="no-referrer"
        />
      </Head>
      <Component {...pageProps} />

      <Script id="google-analytics" strategy="afterInteractive">
        {`
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'G-2DFPN0N5W0');
        `}
      </Script>

      <Script
        src="https://app.dropui.com/api/campaign/request/dbc467fc-ab23-4e2c-b063-37cfa6e30a0c/"
        async
      ></Script>

    </>
  );
}
