import React, { useState, useEffect } from "react";
import Head from "next/head";
import { useRouter } from "next/router";

// Komponenty strony
import Header from "../../components/layout/Header";
import Footer from "../../components/layout/Footer";
import Hero from "../../components/services/Hero";
import WhyService from "../../components/services/WhyService";
import ProcessSteps from "../../components/services/ProcessSteps";
import ServiceTypes from "../../components/services/ServiceTypes";
import Technologies from "../../components/services/Technologies";
import CaseStudies from "../../components/case-studies/CaseStudies";
import FAQ from "../../components/services/FAQ";
import ContactForm from "../../components/services/ContactForm";

// Importuj dane
import serviceData from "../../data/Services";

/**
 * Komponent dynamicznej podstrony usługi
 * 
 * @returns {JSX.Element} Komponent podstrony usługi
 */
export default function ServicePage() {
  const router = useRouter();
  const { id } = router.query;
  const [serviceInfo, setServiceInfo] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  // Pobierz dane usługi na podstawie ID
  useEffect(() => {
    if (router.isReady && id) {
      const data = serviceData[id];
      setServiceInfo(data);
      setIsLoading(false);
    }
  }, [router.isReady, id]);

  // Render stanu ładowania
  if (!router.isReady || isLoading) {
    return (
      <>
        <Head>
          <title>Ładowanie... | SoftwareLogic</title>
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        </Head>
        <Header />
        <main className="py-20 flex items-center justify-center min-h-[50vh]">
          <div className="bg-white rounded-lg shadow-md p-8 text-center">
            <div className="w-16 h-16 mx-auto mb-4 rounded-full bg-purple-600/10 flex items-center justify-center">
              <svg className="animate-spin h-8 w-8 text-purple-600" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                <circle className="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" strokeWidth="4"></circle>
                <path className="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
              </svg>
            </div>
            <p className="text-lg text-gray-700">Ładowanie danych usługi...</p>
          </div>
        </main>
        <Footer />
      </>
    );
  }

  // Render stanu braku znalezionych danych
  if (!serviceInfo) {
    return (
      <>
        <Head>
          <title>Nie znaleziono usługi | SoftwareLogic</title>
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        </Head>
        <Header />
        <main className="py-20 flex items-center justify-center min-h-[50vh]">
          <div className="bg-white rounded-lg shadow-md p-8 text-center max-w-md mx-auto">
            <div className="w-16 h-16 mx-auto mb-4 rounded-full bg-red-100 flex items-center justify-center">
              <svg className="h-8 w-8 text-red-600" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z" />
              </svg>
            </div>
            <h2 className="text-2xl font-bold mb-4 text-gray-800">Nie znaleziono usługi</h2>
            <p className="text-gray-600 mb-6">Przepraszamy, ale szukana usługa nie istnieje lub została przeniesiona.</p>
            <a href="/" className="inline-flex items-center px-6 py-3 bg-purple-600 text-white rounded-lg font-medium shadow-md hover:bg-purple-700 transition-colors duration-300">
              <svg className="w-5 h-5 mr-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M10 19l-7-7m0 0l7-7m-7 7h18"></path>
              </svg>
              Wróć do strony głównej
            </a>
          </div>
        </main>
        <Footer />
      </>
    );
  }

  // Główny render strony
  return (
    <>
      <Head>
        <title>{serviceInfo.meta?.title || "Usługa"} | SoftwareLogic</title>
        <meta
          name="description"
          content={serviceInfo.meta?.description || "Opis usługi"}
        />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        {/* Dodatkowe meta tagi specificzne dla SEO */}
        {serviceInfo.meta?.canonical && (
          <link rel="canonical" href={serviceInfo.meta.canonical} />
        )}
        {serviceInfo.meta?.keywords && (
          <meta name="keywords" content={serviceInfo.meta.keywords} />
        )}
        {/* Open Graph */}
        <meta property="og:title" content={`${serviceInfo.meta?.title || "Usługa"} | SoftwareLogic`} />
        <meta property="og:description" content={serviceInfo.meta?.description || "Opis usługi"} />
        <meta property="og:type" content="website" />
        {serviceInfo.meta?.ogImage && (
          <meta property="og:image" content={serviceInfo.meta.ogImage} />
        )}
      </Head>

      <Header />

      <main>
        {/* Sekcje podstrony usługi */}
        <Hero data={serviceInfo.hero} />
        <WhyService data={serviceInfo.whyService} />
        <ProcessSteps data={serviceInfo.process} />
        <ServiceTypes data={serviceInfo.types} />
        <Technologies data={serviceInfo.technologies} />
        
        {/* Przekazanie danych do CaseStudies */}
        <CaseStudies
          caseStudies={{
            selectedIds: serviceInfo.caseStudies.selectedIds,
            showInfoBox: serviceInfo.caseStudies.showInfoBox,
            hidden: serviceInfo.caseStudies.hidden
          }}
          // Alternatywne wartości props dla większej elastyczności
          title={serviceInfo.caseStudies.title}
          subtitle={serviceInfo.caseStudies.subtitle}
        />
        
        <FAQ data={serviceInfo.faq} />
        <ContactForm data={serviceInfo.contact} />
      </main>

      <Footer />
    </>
  );
}