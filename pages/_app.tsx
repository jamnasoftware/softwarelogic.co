import type { AppProps } from "next/app";
import { ParallaxProvider } from "react-scroll-parallax";
import Head from "next/head";
import Script from "next/script";
import { AppContextProvider } from "../store/app-context";
import Layout from "layout";
import { desc, title } from "./_document";
import { useRouter } from "next/router";
import "./bootstrap.scss";
import "./style.scss";

function MyApp({ Component, pageProps }: AppProps) {
  const router = useRouter();
  return (
    <ParallaxProvider>
      <AppContextProvider>

        <Head>
          <title>{title}</title>
          <link
            rel="canonical"
            href={`https://softwarelogic.co${router.asPath.split('?')[0]}`}
            key="canonical"
          />
          <meta property="og:title" content={title} key="title"/>
          <meta property="og:description" content={desc} key="description"/>
          <meta property="og:url" content="https://softwarelogic.co" key="url"/>
          <meta property="og:type" content="website" key="type"/>
          <meta key="ogimage" property="og:image" content="https://softwarelogic.co/og/home.png"/>
        </Head>
        <Layout>
          <Component {...pageProps} />
        </Layout>

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "https://schema.org",
              "@type": "LocalBusiness",
              url: "https://softwarelogic.co/",
              logo: "https://softwarelogic.co/icon/software-logic-logo_purple.svg",
              description: "full-stack agile software experts ready to support your business",
              name: "softwarelogic.com",
              address: {
                "@type": "PostalAddress",
                addressLocality: "Stargard",
                addressRegion: "ZP",
                postalCode: "73-110",
                addressCountry: "PL",
              },
              image: ["https://softwarelogic.co/og/home.png"],
            }),
          }}
        />
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "WebSite",
              url: "https://softwarelogic.co",
            }),
          }}
        />
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "Software Logic",
              mainEntityOfPage: "https://softwarelogic.co",
              url: "https://softwarelogic.co",
              logo: "https://softwarelogic.co/icon/software-logic-logo_purple.svg",
              sameAs: [
                "https://www.facebook.com/softwarelogic.co",
                "https://www.linkedin.com/company/softwarelogic-co/",
                "https://gitlab.com/users/jamnasoftware/projects",
              ],
              email: "office@softwarelogic.co",
              foundingLocation: "Stargard, Poland",
              vatId: "PL8542432361",
            }),
          }}
        />

        <Script id="google-analytics" strategy="lazyOnload">
          {`
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'G-2DFPN0N5W0');
          `}
        </Script>

        <Script
          src="//www.termsfeed.com/public/cookie-consent/4.1.0/cookie-consent.js"
          strategy="lazyOnload"
          onLoad={() => {
            // @ts-expect-error cookieconsent
            cookieconsent.run({
              "notice_banner_type": "simple",
              "consent_type": "express",
              "palette": "light",
              "language": "en",
              "page_load_consent_levels": ["strictly-necessary"],
              "notice_banner_reject_button_hide": false,
              "preferences_center_close_button_hide": false,
              "page_refresh_confirmation_buttons": false,
              "website_name": ""
            });
          }}
        />

        {/* eslint-ignore-next-line */}
        <script type="text/plain" data-cookie-consent="tracking" src="https://widget.clutch.co/static/js/widget.js"
                async></script>
        <script type="text/plain" data-cookie-consent="tracking"
                dangerouslySetInnerHTML={{
                  __html: 'setTimeout(() => {window.CLUTCHCO.Init()}, 1000)'
                }}
        />
        <script type="text/plain" data-cookie-consent="tracking"
                src="https://www.googletagmanager.com/gtag/js?id=G-2DFPN0N5W0" async></script>

        <Script
          src="https://app.dropui.com/api/campaign/request/6af7fd59-d4ef-4a72-ac97-16ba6cb86afa"
          async
        ></Script>
        <noscript>Free cookie consent management tool by <a href="https://www.termsfeed.com/">TermsFeed</a></noscript>

      </AppContextProvider>
    </ParallaxProvider>
  );
}

export default MyApp;
