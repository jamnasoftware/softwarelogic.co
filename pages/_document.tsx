import { Html, Head, Main, NextScript } from "next/document";

export const title = "Accelerated software development & product design company - softwarelogic.co";
export const desc = "softwarelogic.com is a full-stack agile team that designs and develops flawless web applications & digital products. Let's work together on your custom software solution, including MVP development."

export default function Document() {
  return (
    <Html lang="en">
      <Head>
        <meta httpEquiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta httpEquiv="X-UA-Compatible" content="ie=edge" />

        <meta name="robots" content="index, follow" />
        <meta name="theme-color" content="#5B01AC" />
        <meta name="description" content={desc} />

        <link rel="icon" type="image/png" href="/images/favicon.png" />
        <link
          href="https://fonts.googleapis.com/css2?family=Bentham&family=Nunito+Sans:wght@200&family=Old+Standard+TT&family=Poppins:wght@100;400;500&display=swap"
          rel="preconnect"
        />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
      </Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
