import { NextApiResponse } from "next";
import { getPaths as getBlogPaths } from "pages/blog/[id]";
import { getPaths as getCaseStudiesPaths } from "pages/case-studies/[id]";
import { getPaths as getServicesPaths } from "pages/services/[id]";
import { getPaths as getTechnologiesPaths } from "pages/technology/[id]";

function generateSiteMap(paths: string[]) {
  return `<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
  ${paths
      .map(path => {
        return `<url>
    <loc>https://softwarelogic.co${path}</loc>
  </url>
  `;
      })
      .join("")}
</urlset>
`;
}

function SiteMap() {
  // getServerSideProps will do the heavy lifting
}

export async function getServerSideProps({ res }: { res: NextApiResponse }) {
  const paths = [
    '/',
    ...await getBlogPaths(),
    ...await getCaseStudiesPaths(),
    ...await getServicesPaths(),
    ...await getTechnologiesPaths(),
  ];
  // We generate the XML sitemap with the posts data
  const sitemap = generateSiteMap(paths);

  res.setHeader("Content-Type", "text/xml");
  // we send the XML to the browser
  res.write(sitemap);
  res.end();

  return {
    props: {},
  };
}

export default SiteMap;
