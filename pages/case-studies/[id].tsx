import { textToPath } from "utils/helpers/main";
import CaseStudy from "views/CaseStudy";
import casestudies from "data/casestudies";

const getSlugs = () => casestudies.map(item => textToPath(item.title));

export const getPath = (slug?: string) => `/case-studies/${slug}${slug ? '/': ''}`;
export const getPaths = () => {
  const slugs = getSlugs();
  return [
    ...slugs.map(slug => getPath(slug))
  ]
}

export async function getStaticPaths() {
  return {
    paths: casestudies.map((r) => ({
      params: {
        id: textToPath(r.title),
      },
    })),
    fallback: false,
  };
}
export async function getStaticProps() {
  return { props: {} };
}

export default CaseStudy;
