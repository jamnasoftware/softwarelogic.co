import technologies from "data/technologies";
import Technology from "views/Technology";
import { GetStaticProps } from "next";

const getSlugs = () => Object.keys(technologies);

export const getPath = (slug?: string) => `/technology/${slug}${slug ? '/' : ''}`;
export const getPaths = () => {
  const slugs = getSlugs();
  return [
    ...slugs.map(slug => getPath(slug))
  ]
}

export function getStaticPaths() {
  return {
    paths: getSlugs().map(key=> ({
      params: {
        id: key,
      },
    })),
    fallback: false,
  };
}

export const getStaticProps: GetStaticProps = async (context) => {
  try {
    if (!context.params?.id) {
      throw Error;
    }
    const id = context.params.id;
    if (typeof id !== 'string') {
      throw Error;
    }
    const technology = technologies[id as keyof typeof technologies];
    if (!technology) {
      throw Error;
    }
    return {
      props: {
        ...technology
      },
      revalidate: 10 * 60,
    };
  } catch {
    return {
      notFound: true,
    }
  }
}

export default Technology;
