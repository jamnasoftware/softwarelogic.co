import React, { Fragment } from "react";
import { Col, Container, Row } from "reactstrap";
import caseStudies from "data/casestudies";
import technologies from "data/technologies";
import Link from "next/link";
import { WP_REST_API_Tags } from "wp-types";
import services from "data/services";
import styles from "./styles.module.scss";
import { ParallaxBanner } from "react-scroll-parallax";
import Head from "next/head";

const getTags = async (page: number) => await fetch(`https://serwer2272706.home.pl/autoinstalator/wordpressplugins2/wp-json/wp/v2/tags?per_page=100&page=${page + 1}`).then(r => r.json()).then((items: WP_REST_API_Tags) => items.filter(item => item.count > 1)).then((items: WP_REST_API_Tags) => items.map(item => ([item.name, `/blog/tag/${item.slug}`])))
const getAllTags = async () => {
  const total = await fetch(`https://serwer2272706.home.pl/autoinstalator/wordpressplugins2/wp-json/wp/v2/tags?per_page=1&page=1`).then(r => {
    return +(r.headers.get('X-WP-Total') || 0);
  });
  return (await Promise.all(new Array(Math.ceil(total / 100)).fill(true).map((v, index) => {
    return getTags(index);
  }))).flat();
}
const list = Object.entries(technologies).map(([key, value]) => ({
  title: value.title,
  url: `/technology/${key}`,
}));
export const getStaticProps = async () => {
  const sitemap = {
    'Services': services.map(item => ([item.title, item.url])),
    'Case Studies': caseStudies.map(item => ([item.title, item.url])),
    'Technologies': list.map(item => ([item.title, item.url])),
    'Tags': await getAllTags(),
  }
  return {
    props: {
      sitemap,
    },
    revalidate: 24 * 60 * 60 // day
  };
}

type SitemapProps = {
  sitemap: Record<string, [string, string][]>,
}

const Sitemap = ({ sitemap }: SitemapProps) => {
  return (
    <>
      <Head>
        <meta key="ogimage" property="og:image" content="https://softwarelogic.co/og/sitemap.png" />
      </Head>
      <ParallaxBanner
        layers={[{ image: "/hero.webp", speed: -15 }]}
        className={styles.hero}
      >
        <header className="container">
          <h1 className={styles.title}>
            Sitemap
          </h1>
        </header>
      </ParallaxBanner>
      <Container className={styles.container}>
        {Object.entries(sitemap).map(([name, items]) => (
          <Fragment key={name}>
            <h2>{name}</h2>
            <Row>
              {items.map(([title, url]) => (
                <Col key={title} md={4}>
                  <Link className="color-primary" href={url}>{title}</Link>
                </Col>
              ))}
            </Row>
          </Fragment>
        ))}
      </Container>
    </>
  );
};

export default Sitemap;
