import services from "data/services";
import { GetStaticProps } from "next";
import { textToPath } from "utils/helpers/main";
import Services from "views/Services";

export const getPath = (slug?: string) => `/services/${slug}${slug ? '/' : ''}`;
export const getPaths = () => {
  return [
    ...services.map(service => service.url)
  ]
}

export function getStaticPaths() {
  return {
    paths: services.map((r) => ({
      params: {
        id: textToPath(r.title),
      },
    })),
    fallback: false,
  };
}

export const getStaticProps: GetStaticProps = async (context) => {
  try {
    if (!context.params?.id) {
      throw Error;
    }
    const offer = services.find(data => textToPath(data.title) === context.params!.id);
    if (!offer) {
      throw Error;
    }
    return {
      props: {
        ...offer,
      },
      revalidate: 24 * 60 * 60 // day
    };
  } catch  {
    return {
      notFound: true,
    }
  }
}
export default Services;
