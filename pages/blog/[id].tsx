import { GetStaticProps } from "next";
import News from "views/News/News";
import { WP_REST_API_Post, WP_REST_API_Posts } from "wp-types";
import Blog, { getStaticProps as getStaticPropsNewsList } from "./index"; 

const getPosts = async () => {
  const posts: WP_REST_API_Posts = await fetch(
    "https://serwer2272706.home.pl/autoinstalator/wordpressplugins2/wp-json/wp/v2/posts?per_page=100&_fields=slug"
  ).then((r) => r.json());
  return posts.map(post => post.slug);
}

export const getPath = (slug: string = '') => `/blog/${slug}${slug ? '/': ''}`;
export const getPaths = async () => {
  const posts = await getPosts();
  return [
    getPath(),
    ...posts.map(slug => getPath(slug))
  ]
}

export async function getStaticPaths() {
  const posts = await getPosts();
  return {
    paths: posts.map(slug => ({
      params: {
        id: slug,
      },
    })),
    fallback: true,
  };
}

export const getStaticProps: GetStaticProps = async (context) => {
  try {
    if (!context.params?.id) {
      throw Error;
    }
    const { id } = context.params;
    if(Number.isSafeInteger(+id)) {
      return getStaticPropsNewsList(context);
    }
    const data = await fetch(`https://serwer2272706.home.pl/autoinstalator/wordpressplugins2/wp-json/wp/v2/posts?slug=${id}&_fields=date,modified,slug,title,content,excerpt`).then(r => r.json())
    if (data.length === 0) {
      throw Error;
    }
    const newsData = data[0];
    return {
      props: {
        newsData,
      },
      revalidate: 10 * 60,
    };
  } catch  {
    return {
      notFound: true,
      revalidate: 60,
    }
  }
}

type Props = {
  posts?: WP_REST_API_Posts
  newsData?: WP_REST_API_Post
}

const NewsRouter = (props: Props) => {
  if(!!props.posts) {
    return (
      // @ts-expect-error stupid
      <Blog {...props} />
    )
  }
  return (
    // @ts-expect-error stupid
    <News {...props} />
  )
}

export default NewsRouter;
