import { GetStaticProps } from "next";
import Tag from "views/Tag";
import { WP_REST_API_Post, WP_REST_API_Tags } from "wp-types";

const getTags = async (page: number) => await fetch(`https://serwer2272706.home.pl/autoinstalator/wordpressplugins2/wp-json/wp/v2/tags?per_page=100&page=${page + 1}`).then(r => r.json()).then((items: WP_REST_API_Tags) => items.filter(item => item.count > 1)).then((items: WP_REST_API_Tags) => items.map(item => item.slug))
const getAllTags = async () => {
  const total = await fetch(`https://serwer2272706.home.pl/autoinstalator/wordpressplugins2/wp-json/wp/v2/tags?per_page=1&page=1`).then(r => {
    return+(r.headers.get('X-WP-Total') || 0);
  });
  return (await Promise.all(new Array(Math.ceil(total/100)).fill(true).map((v, index) => {
    return getTags(index);
  }))).flat();
}
export async function getStaticPaths() {
  const tags = await getAllTags();
  return {
    paths: tags.map((slug) => ({
      params: {
        id: slug,
      },
    })),
    fallback: true,
  };
}

const fetchApi = async (endpoint: string) => await fetch(`https://serwer2272706.home.pl/autoinstalator/wordpressplugins2/wp-json/wp/v2/${endpoint}`).then(r => r.json());

export const getStaticProps: GetStaticProps = async (context) => {
  try {
    const slug = context.params?.id;
    const tag = await fetchApi(`tags?slug=${slug}`).then(tags => tags[0]);
    const id = tag.id;
    const posts = await fetchApi(`posts?per_page=100&tags=${id}`);
    return {
      props: {
        tag,
        posts: posts.map((post: WP_REST_API_Post) => ({
          title: post.title.rendered,
          content: post.excerpt.rendered,
          date: post.date,
          slug: post.slug,
          minutesToRead: Math.round(post.content.rendered.split(' ').length / 200),
        })),
        total: posts.length,
        page: 1,
      },
      revalidate: 20 * 60 // 20min
    };
  } catch {
    return {
      notFound: true,
      revalidate: 60,
    }
  }
}

export default Tag;
