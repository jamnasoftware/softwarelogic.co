import { GetStaticProps } from "next";
import Blog, { Props } from "views/Blog";
import { WP_REST_API_Post } from "wp-types";

export const getStaticProps = (async (context) => {
  const page = +(context.params?.id || 1);
  if(page < 1){
    return {
      notFound: true,
      revalidate: 60,
    }
  }
  let total = 0;
  const posts = await fetch(
    `https://serwer2272706.home.pl/autoinstalator/wordpressplugins2/wp-json/wp/v2/posts?per_page=9&page=${page}`
  ).then((r) => {
    total = +(r.headers.get('X-WP-Total') || 0);
    return r.json();
  });
  if (Math.floor(total / 9) - page + 1 < 0) {
    return {
      notFound: true,
      revalidate: 60,
    }
  }
  return {
    props: {
      posts: posts.map((post: WP_REST_API_Post) => ({
        title: post.title,
        date: post.date,
        excerpt: post.excerpt,
        modified: post.modified,
        slug: post.slug,
        image: post.content.rendered.match(/<img.*src="(\S*)"/)?.[1],
        minutesToRead: Math.round(post.content.rendered.split(' ').length / 200),
      })),
      total,
      page,
    },
    revalidate: 20 * 60 // 20min
  };
}) satisfies GetStaticProps<Props>

export default Blog;
